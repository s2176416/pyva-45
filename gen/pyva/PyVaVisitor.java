// Generated from C:/Users/adila/IdeaProjects/pyva-45/src/pyva\PyVa.g4 by ANTLR 4.8
package pyva;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link PyVaParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface PyVaVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link PyVaParser#goal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGoal(PyVaParser.GoalContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(PyVaParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncdecl(PyVaParser.FuncdeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code blockdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlockdecl(PyVaParser.BlockdeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code vardecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVardecl(PyVaParser.VardeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code ifdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfdecl(PyVaParser.IfdeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code whiledecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhiledecl(PyVaParser.WhiledeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code fordecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFordecl(PyVaParser.FordeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcalldecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncalldecl(PyVaParser.FuncalldeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code returndecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturndecl(PyVaParser.ReturndeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code threaddecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThreaddecl(PyVaParser.ThreaddeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code joindecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJoindecl(PyVaParser.JoindeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code syncdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSyncdecl(PyVaParser.SyncdeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code recorddecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecorddecl(PyVaParser.RecorddeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code printdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrintdecl(PyVaParser.PrintdeclContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#recordvartpye}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecordvartpye(PyVaParser.RecordvartpyeContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#block}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBlock(PyVaParser.BlockContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typelessvardecl}
	 * labeled alternative in {@link PyVaParser#vardeclare}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypelessvardecl(PyVaParser.TypelessvardeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typevardecl}
	 * labeled alternative in {@link PyVaParser#vardeclare}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypevardecl(PyVaParser.TypevardeclContext ctx);
	/**
	 * Visit a parse tree produced by the {@code valuedArrayInit}
	 * labeled alternative in {@link PyVaParser#arrayInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValuedArrayInit(PyVaParser.ValuedArrayInitContext ctx);
	/**
	 * Visit a parse tree produced by the {@code sizedArrayInit}
	 * labeled alternative in {@link PyVaParser#arrayInit}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSizedArrayInit(PyVaParser.SizedArrayInitContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#arrayValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayValue(PyVaParser.ArrayValueContext ctx);
	/**
	 * Visit a parse tree produced by the {@code charExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharExpr(PyVaParser.CharExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code recordExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRecordExpr(PyVaParser.RecordExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullExpr(PyVaParser.NullExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code trueExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTrueExpr(PyVaParser.TrueExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayInitExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayInitExpr(PyVaParser.ArrayInitExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultExpr(PyVaParser.MultExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code numExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumExpr(PyVaParser.NumExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code plusExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlusExpr(PyVaParser.PlusExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringExpr(PyVaParser.StringExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParExpr(PyVaParser.ParExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code funcallExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncallExpr(PyVaParser.FuncallExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code compExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompExpr(PyVaParser.CompExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code prfExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrfExpr(PyVaParser.PrfExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code falseExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFalseExpr(PyVaParser.FalseExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolExpr(PyVaParser.BoolExprContext ctx);
	/**
	 * Visit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdExpr(PyVaParser.IdExprContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#idCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdCall(PyVaParser.IdCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#accesser}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAccesser(PyVaParser.AccesserContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#params}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParams(PyVaParser.ParamsContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#primitiveType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimitiveType(PyVaParser.PrimitiveTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(PyVaParser.IdentifierContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#arrayType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayType(PyVaParser.ArrayTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#compoundType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompoundType(PyVaParser.CompoundTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code typeFuncType}
	 * labeled alternative in {@link PyVaParser#funcType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeFuncType(PyVaParser.TypeFuncTypeContext ctx);
	/**
	 * Visit a parse tree produced by the {@code voidFuncType}
	 * labeled alternative in {@link PyVaParser#funcType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVoidFuncType(PyVaParser.VoidFuncTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#funcall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFuncall(PyVaParser.FuncallContext ctx);
	/**
	 * Visit a parse tree produced by the {@code minusPrfOp}
	 * labeled alternative in {@link PyVaParser#prfOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinusPrfOp(PyVaParser.MinusPrfOpContext ctx);
	/**
	 * Visit a parse tree produced by the {@code notPrfOp}
	 * labeled alternative in {@link PyVaParser#prfOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotPrfOp(PyVaParser.NotPrfOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#multOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultOp(PyVaParser.MultOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#plusOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlusOp(PyVaParser.PlusOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#boolOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBoolOp(PyVaParser.BoolOpContext ctx);
	/**
	 * Visit a parse tree produced by {@link PyVaParser#compOp}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompOp(PyVaParser.CompOpContext ctx);
}