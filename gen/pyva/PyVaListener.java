// Generated from C:/Users/adila/IdeaProjects/pyva-45/src/pyva\PyVa.g4 by ANTLR 4.8
package pyva;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link PyVaParser}.
 */
public interface PyVaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link PyVaParser#goal}.
	 * @param ctx the parse tree
	 */
	void enterGoal(PyVaParser.GoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#goal}.
	 * @param ctx the parse tree
	 */
	void exitGoal(PyVaParser.GoalContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(PyVaParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(PyVaParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funcdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterFuncdecl(PyVaParser.FuncdeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funcdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitFuncdecl(PyVaParser.FuncdeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code blockdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterBlockdecl(PyVaParser.BlockdeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code blockdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitBlockdecl(PyVaParser.BlockdeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code vardecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterVardecl(PyVaParser.VardeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code vardecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitVardecl(PyVaParser.VardeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterIfdecl(PyVaParser.IfdeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitIfdecl(PyVaParser.IfdeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whiledecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterWhiledecl(PyVaParser.WhiledeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whiledecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitWhiledecl(PyVaParser.WhiledeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code fordecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterFordecl(PyVaParser.FordeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code fordecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitFordecl(PyVaParser.FordeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funcalldecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterFuncalldecl(PyVaParser.FuncalldeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funcalldecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitFuncalldecl(PyVaParser.FuncalldeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code returndecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterReturndecl(PyVaParser.ReturndeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code returndecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitReturndecl(PyVaParser.ReturndeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code threaddecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterThreaddecl(PyVaParser.ThreaddeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code threaddecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitThreaddecl(PyVaParser.ThreaddeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code joindecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterJoindecl(PyVaParser.JoindeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code joindecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitJoindecl(PyVaParser.JoindeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code syncdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterSyncdecl(PyVaParser.SyncdeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code syncdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitSyncdecl(PyVaParser.SyncdeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code recorddecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterRecorddecl(PyVaParser.RecorddeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code recorddecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitRecorddecl(PyVaParser.RecorddeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code printdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void enterPrintdecl(PyVaParser.PrintdeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code printdecl}
	 * labeled alternative in {@link PyVaParser#decl}.
	 * @param ctx the parse tree
	 */
	void exitPrintdecl(PyVaParser.PrintdeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#recordvartpye}.
	 * @param ctx the parse tree
	 */
	void enterRecordvartpye(PyVaParser.RecordvartpyeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#recordvartpye}.
	 * @param ctx the parse tree
	 */
	void exitRecordvartpye(PyVaParser.RecordvartpyeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(PyVaParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(PyVaParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typelessvardecl}
	 * labeled alternative in {@link PyVaParser#vardeclare}.
	 * @param ctx the parse tree
	 */
	void enterTypelessvardecl(PyVaParser.TypelessvardeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typelessvardecl}
	 * labeled alternative in {@link PyVaParser#vardeclare}.
	 * @param ctx the parse tree
	 */
	void exitTypelessvardecl(PyVaParser.TypelessvardeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typevardecl}
	 * labeled alternative in {@link PyVaParser#vardeclare}.
	 * @param ctx the parse tree
	 */
	void enterTypevardecl(PyVaParser.TypevardeclContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typevardecl}
	 * labeled alternative in {@link PyVaParser#vardeclare}.
	 * @param ctx the parse tree
	 */
	void exitTypevardecl(PyVaParser.TypevardeclContext ctx);
	/**
	 * Enter a parse tree produced by the {@code valuedArrayInit}
	 * labeled alternative in {@link PyVaParser#arrayInit}.
	 * @param ctx the parse tree
	 */
	void enterValuedArrayInit(PyVaParser.ValuedArrayInitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code valuedArrayInit}
	 * labeled alternative in {@link PyVaParser#arrayInit}.
	 * @param ctx the parse tree
	 */
	void exitValuedArrayInit(PyVaParser.ValuedArrayInitContext ctx);
	/**
	 * Enter a parse tree produced by the {@code sizedArrayInit}
	 * labeled alternative in {@link PyVaParser#arrayInit}.
	 * @param ctx the parse tree
	 */
	void enterSizedArrayInit(PyVaParser.SizedArrayInitContext ctx);
	/**
	 * Exit a parse tree produced by the {@code sizedArrayInit}
	 * labeled alternative in {@link PyVaParser#arrayInit}.
	 * @param ctx the parse tree
	 */
	void exitSizedArrayInit(PyVaParser.SizedArrayInitContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#arrayValue}.
	 * @param ctx the parse tree
	 */
	void enterArrayValue(PyVaParser.ArrayValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#arrayValue}.
	 * @param ctx the parse tree
	 */
	void exitArrayValue(PyVaParser.ArrayValueContext ctx);
	/**
	 * Enter a parse tree produced by the {@code charExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCharExpr(PyVaParser.CharExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code charExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCharExpr(PyVaParser.CharExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code recordExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterRecordExpr(PyVaParser.RecordExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code recordExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitRecordExpr(PyVaParser.RecordExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNullExpr(PyVaParser.NullExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNullExpr(PyVaParser.NullExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code trueExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterTrueExpr(PyVaParser.TrueExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code trueExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitTrueExpr(PyVaParser.TrueExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayInitExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterArrayInitExpr(PyVaParser.ArrayInitExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayInitExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitArrayInitExpr(PyVaParser.ArrayInitExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterMultExpr(PyVaParser.MultExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitMultExpr(PyVaParser.MultExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code numExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterNumExpr(PyVaParser.NumExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code numExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitNumExpr(PyVaParser.NumExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code plusExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPlusExpr(PyVaParser.PlusExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code plusExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPlusExpr(PyVaParser.PlusExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterStringExpr(PyVaParser.StringExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitStringExpr(PyVaParser.StringExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterParExpr(PyVaParser.ParExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitParExpr(PyVaParser.ParExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code funcallExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterFuncallExpr(PyVaParser.FuncallExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code funcallExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitFuncallExpr(PyVaParser.FuncallExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code compExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterCompExpr(PyVaParser.CompExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code compExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitCompExpr(PyVaParser.CompExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code prfExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPrfExpr(PyVaParser.PrfExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code prfExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPrfExpr(PyVaParser.PrfExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code falseExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterFalseExpr(PyVaParser.FalseExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code falseExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitFalseExpr(PyVaParser.FalseExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterBoolExpr(PyVaParser.BoolExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code boolExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitBoolExpr(PyVaParser.BoolExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterIdExpr(PyVaParser.IdExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code idExpr}
	 * labeled alternative in {@link PyVaParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitIdExpr(PyVaParser.IdExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#idCall}.
	 * @param ctx the parse tree
	 */
	void enterIdCall(PyVaParser.IdCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#idCall}.
	 * @param ctx the parse tree
	 */
	void exitIdCall(PyVaParser.IdCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#accesser}.
	 * @param ctx the parse tree
	 */
	void enterAccesser(PyVaParser.AccesserContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#accesser}.
	 * @param ctx the parse tree
	 */
	void exitAccesser(PyVaParser.AccesserContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#params}.
	 * @param ctx the parse tree
	 */
	void enterParams(PyVaParser.ParamsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#params}.
	 * @param ctx the parse tree
	 */
	void exitParams(PyVaParser.ParamsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void enterPrimitiveType(PyVaParser.PrimitiveTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void exitPrimitiveType(PyVaParser.PrimitiveTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(PyVaParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(PyVaParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#arrayType}.
	 * @param ctx the parse tree
	 */
	void enterArrayType(PyVaParser.ArrayTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#arrayType}.
	 * @param ctx the parse tree
	 */
	void exitArrayType(PyVaParser.ArrayTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#compoundType}.
	 * @param ctx the parse tree
	 */
	void enterCompoundType(PyVaParser.CompoundTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#compoundType}.
	 * @param ctx the parse tree
	 */
	void exitCompoundType(PyVaParser.CompoundTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code typeFuncType}
	 * labeled alternative in {@link PyVaParser#funcType}.
	 * @param ctx the parse tree
	 */
	void enterTypeFuncType(PyVaParser.TypeFuncTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code typeFuncType}
	 * labeled alternative in {@link PyVaParser#funcType}.
	 * @param ctx the parse tree
	 */
	void exitTypeFuncType(PyVaParser.TypeFuncTypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code voidFuncType}
	 * labeled alternative in {@link PyVaParser#funcType}.
	 * @param ctx the parse tree
	 */
	void enterVoidFuncType(PyVaParser.VoidFuncTypeContext ctx);
	/**
	 * Exit a parse tree produced by the {@code voidFuncType}
	 * labeled alternative in {@link PyVaParser#funcType}.
	 * @param ctx the parse tree
	 */
	void exitVoidFuncType(PyVaParser.VoidFuncTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#funcall}.
	 * @param ctx the parse tree
	 */
	void enterFuncall(PyVaParser.FuncallContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#funcall}.
	 * @param ctx the parse tree
	 */
	void exitFuncall(PyVaParser.FuncallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code minusPrfOp}
	 * labeled alternative in {@link PyVaParser#prfOp}.
	 * @param ctx the parse tree
	 */
	void enterMinusPrfOp(PyVaParser.MinusPrfOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code minusPrfOp}
	 * labeled alternative in {@link PyVaParser#prfOp}.
	 * @param ctx the parse tree
	 */
	void exitMinusPrfOp(PyVaParser.MinusPrfOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notPrfOp}
	 * labeled alternative in {@link PyVaParser#prfOp}.
	 * @param ctx the parse tree
	 */
	void enterNotPrfOp(PyVaParser.NotPrfOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notPrfOp}
	 * labeled alternative in {@link PyVaParser#prfOp}.
	 * @param ctx the parse tree
	 */
	void exitNotPrfOp(PyVaParser.NotPrfOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#multOp}.
	 * @param ctx the parse tree
	 */
	void enterMultOp(PyVaParser.MultOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#multOp}.
	 * @param ctx the parse tree
	 */
	void exitMultOp(PyVaParser.MultOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#plusOp}.
	 * @param ctx the parse tree
	 */
	void enterPlusOp(PyVaParser.PlusOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#plusOp}.
	 * @param ctx the parse tree
	 */
	void exitPlusOp(PyVaParser.PlusOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#boolOp}.
	 * @param ctx the parse tree
	 */
	void enterBoolOp(PyVaParser.BoolOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#boolOp}.
	 * @param ctx the parse tree
	 */
	void exitBoolOp(PyVaParser.BoolOpContext ctx);
	/**
	 * Enter a parse tree produced by {@link PyVaParser#compOp}.
	 * @param ctx the parse tree
	 */
	void enterCompOp(PyVaParser.CompOpContext ctx);
	/**
	 * Exit a parse tree produced by {@link PyVaParser#compOp}.
	 * @param ctx the parse tree
	 */
	void exitCompOp(PyVaParser.CompOpContext ctx);
}