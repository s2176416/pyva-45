// Generated from C:/Users/adila/IdeaProjects/pyva-45/src/pyva\PyVa.g4 by ANTLR 4.8
package pyva;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class PyVaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		PRINT=1, CLASS=2, NEW=3, IF=4, ELSE=5, FOR=6, WHILE=7, RETURN=8, NOT=9, 
		BOOLEAN=10, VOID=11, CHAR=12, NULL=13, INT=14, TRUE=15, FALSE=16, OR=17, 
		AND=18, DEF=19, ASS=20, COLON=21, COMMA=22, DOT=23, DQUOTE=24, SQUOTE=25, 
		EQ=26, GE=27, GT=28, LE=29, LBRACE=30, LPAR=31, LT=32, MINUS=33, NE=34, 
		PLUS=35, RBRACE=36, RPAR=37, SEMI=38, SLASH=39, STAR=40, OBRACK=41, CBRACK=42, 
		THREAD=43, JOIN=44, SYNC=45, OCOM=46, CCOM=47, DOUBLESLASH=48, RECORD=49, 
		ID=50, NUM=51, STRING=52, CHR=53, BOOL=54, WS=55, COMMENT=56, LINECOMMENT=57;
	public static final int
		RULE_goal = 0, RULE_program = 1, RULE_decl = 2, RULE_recordvartpye = 3, 
		RULE_block = 4, RULE_vardeclare = 5, RULE_arrayInit = 6, RULE_arrayValue = 7, 
		RULE_expr = 8, RULE_idCall = 9, RULE_accesser = 10, RULE_params = 11, 
		RULE_primitiveType = 12, RULE_identifier = 13, RULE_arrayType = 14, RULE_compoundType = 15, 
		RULE_funcType = 16, RULE_funcall = 17, RULE_prfOp = 18, RULE_multOp = 19, 
		RULE_plusOp = 20, RULE_boolOp = 21, RULE_compOp = 22;
	private static String[] makeRuleNames() {
		return new String[] {
			"goal", "program", "decl", "recordvartpye", "block", "vardeclare", "arrayInit", 
			"arrayValue", "expr", "idCall", "accesser", "params", "primitiveType", 
			"identifier", "arrayType", "compoundType", "funcType", "funcall", "prfOp", 
			"multOp", "plusOp", "boolOp", "compOp"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'print'", "'class'", "'new'", "'if'", "'else'", "'for'", "'while'", 
			"'return'", "'!'", "'boolean'", "'void'", "'char'", "'null'", "'int'", 
			"'true'", "'false'", "'||'", "'&&'", "'def'", "'='", "':'", "','", "'.'", 
			"'\"'", null, "'=='", "'>='", "'>'", "'<='", "'{'", "'('", "'<'", "'-'", 
			"'!='", "'+'", "'}'", "')'", "';'", "'/'", "'*'", "'['", "']'", "'thread'", 
			"'join'", "'sync'", "'/*'", "'*/'", "'//'", "'record'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "PRINT", "CLASS", "NEW", "IF", "ELSE", "FOR", "WHILE", "RETURN", 
			"NOT", "BOOLEAN", "VOID", "CHAR", "NULL", "INT", "TRUE", "FALSE", "OR", 
			"AND", "DEF", "ASS", "COLON", "COMMA", "DOT", "DQUOTE", "SQUOTE", "EQ", 
			"GE", "GT", "LE", "LBRACE", "LPAR", "LT", "MINUS", "NE", "PLUS", "RBRACE", 
			"RPAR", "SEMI", "SLASH", "STAR", "OBRACK", "CBRACK", "THREAD", "JOIN", 
			"SYNC", "OCOM", "CCOM", "DOUBLESLASH", "RECORD", "ID", "NUM", "STRING", 
			"CHR", "BOOL", "WS", "COMMENT", "LINECOMMENT"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "PyVa.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public PyVaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class GoalContext extends ParserRuleContext {
		public List<DeclContext> decl() {
			return getRuleContexts(DeclContext.class);
		}
		public DeclContext decl(int i) {
			return getRuleContext(DeclContext.class,i);
		}
		public GoalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_goal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterGoal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitGoal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitGoal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GoalContext goal() throws RecognitionException {
		GoalContext _localctx = new GoalContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_goal);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(47); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(46);
				decl();
				}
				}
				setState(49); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PRINT) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RETURN) | (1L << BOOLEAN) | (1L << VOID) | (1L << CHAR) | (1L << NULL) | (1L << INT) | (1L << DEF) | (1L << LBRACE) | (1L << THREAD) | (1L << JOIN) | (1L << SYNC) | (1L << ID))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProgramContext extends ParserRuleContext {
		public List<DeclContext> decl() {
			return getRuleContexts(DeclContext.class);
		}
		public DeclContext decl(int i) {
			return getRuleContext(DeclContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(54);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PRINT) | (1L << IF) | (1L << FOR) | (1L << WHILE) | (1L << RETURN) | (1L << BOOLEAN) | (1L << VOID) | (1L << CHAR) | (1L << NULL) | (1L << INT) | (1L << DEF) | (1L << LBRACE) | (1L << THREAD) | (1L << JOIN) | (1L << SYNC) | (1L << ID))) != 0)) {
				{
				{
				setState(51);
				decl();
				}
				}
				setState(56);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclContext extends ParserRuleContext {
		public DeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decl; }
	 
		public DeclContext() { }
		public void copyFrom(DeclContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ReturndeclContext extends DeclContext {
		public TerminalNode RETURN() { return getToken(PyVaParser.RETURN, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(PyVaParser.SEMI, 0); }
		public ReturndeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterReturndecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitReturndecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitReturndecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FuncdeclContext extends DeclContext {
		public TerminalNode DEF() { return getToken(PyVaParser.DEF, 0); }
		public FuncTypeContext funcType() {
			return getRuleContext(FuncTypeContext.class,0);
		}
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public TerminalNode LPAR() { return getToken(PyVaParser.LPAR, 0); }
		public ParamsContext params() {
			return getRuleContext(ParamsContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(PyVaParser.RPAR, 0); }
		public TerminalNode LBRACE() { return getToken(PyVaParser.LBRACE, 0); }
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(PyVaParser.RBRACE, 0); }
		public FuncdeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterFuncdecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitFuncdecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitFuncdecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FordeclContext extends DeclContext {
		public TerminalNode FOR() { return getToken(PyVaParser.FOR, 0); }
		public TerminalNode LPAR() { return getToken(PyVaParser.LPAR, 0); }
		public List<VardeclareContext> vardeclare() {
			return getRuleContexts(VardeclareContext.class);
		}
		public VardeclareContext vardeclare(int i) {
			return getRuleContext(VardeclareContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(PyVaParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(PyVaParser.SEMI, i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(PyVaParser.RPAR, 0); }
		public TerminalNode LBRACE() { return getToken(PyVaParser.LBRACE, 0); }
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(PyVaParser.RBRACE, 0); }
		public FordeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterFordecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitFordecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitFordecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class VardeclContext extends DeclContext {
		public VardeclareContext vardeclare() {
			return getRuleContext(VardeclareContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(PyVaParser.SEMI, 0); }
		public VardeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterVardecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitVardecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitVardecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class JoindeclContext extends DeclContext {
		public TerminalNode JOIN() { return getToken(PyVaParser.JOIN, 0); }
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public TerminalNode SEMI() { return getToken(PyVaParser.SEMI, 0); }
		public JoindeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterJoindecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitJoindecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitJoindecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RecorddeclContext extends DeclContext {
		public TerminalNode DEF() { return getToken(PyVaParser.DEF, 0); }
		public TerminalNode RECORD() { return getToken(PyVaParser.RECORD, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode LBRACE() { return getToken(PyVaParser.LBRACE, 0); }
		public TerminalNode RBRACE() { return getToken(PyVaParser.RBRACE, 0); }
		public List<RecordvartpyeContext> recordvartpye() {
			return getRuleContexts(RecordvartpyeContext.class);
		}
		public RecordvartpyeContext recordvartpye(int i) {
			return getRuleContext(RecordvartpyeContext.class,i);
		}
		public RecorddeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterRecorddecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitRecorddecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitRecorddecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BlockdeclContext extends DeclContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public BlockdeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterBlockdecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitBlockdecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitBlockdecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ThreaddeclContext extends DeclContext {
		public TerminalNode THREAD() { return getToken(PyVaParser.THREAD, 0); }
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ThreaddeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterThreaddecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitThreaddecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitThreaddecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FuncalldeclContext extends DeclContext {
		public FuncallContext funcall() {
			return getRuleContext(FuncallContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(PyVaParser.SEMI, 0); }
		public FuncalldeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterFuncalldecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitFuncalldecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitFuncalldecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class SyncdeclContext extends DeclContext {
		public TerminalNode SYNC() { return getToken(PyVaParser.SYNC, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public SyncdeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterSyncdecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitSyncdecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitSyncdecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhiledeclContext extends DeclContext {
		public TerminalNode WHILE() { return getToken(PyVaParser.WHILE, 0); }
		public TerminalNode LPAR() { return getToken(PyVaParser.LPAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(PyVaParser.RPAR, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public WhiledeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterWhiledecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitWhiledecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitWhiledecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrintdeclContext extends DeclContext {
		public TerminalNode PRINT() { return getToken(PyVaParser.PRINT, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(PyVaParser.SEMI, 0); }
		public PrintdeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterPrintdecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitPrintdecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitPrintdecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfdeclContext extends DeclContext {
		public TerminalNode IF() { return getToken(PyVaParser.IF, 0); }
		public TerminalNode LPAR() { return getToken(PyVaParser.LPAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(PyVaParser.RPAR, 0); }
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(PyVaParser.ELSE, 0); }
		public IfdeclContext(DeclContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterIfdecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitIfdecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitIfdecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclContext decl() throws RecognitionException {
		DeclContext _localctx = new DeclContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_decl);
		int _la;
		try {
			setState(129);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				_localctx = new FuncdeclContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(57);
				match(DEF);
				setState(58);
				funcType();
				setState(59);
				match(ID);
				setState(60);
				match(LPAR);
				setState(61);
				params();
				setState(62);
				match(RPAR);
				setState(63);
				match(LBRACE);
				setState(64);
				program();
				setState(65);
				match(RBRACE);
				}
				break;
			case 2:
				_localctx = new BlockdeclContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(67);
				block();
				}
				break;
			case 3:
				_localctx = new VardeclContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(68);
				vardeclare();
				setState(69);
				match(SEMI);
				}
				break;
			case 4:
				_localctx = new IfdeclContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(71);
				match(IF);
				setState(72);
				match(LPAR);
				setState(73);
				expr(0);
				setState(74);
				match(RPAR);
				setState(75);
				block();
				setState(78);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==ELSE) {
					{
					setState(76);
					match(ELSE);
					setState(77);
					block();
					}
				}

				}
				break;
			case 5:
				_localctx = new WhiledeclContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(80);
				match(WHILE);
				setState(81);
				match(LPAR);
				setState(82);
				expr(0);
				setState(83);
				match(RPAR);
				setState(84);
				block();
				}
				break;
			case 6:
				_localctx = new FordeclContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(86);
				match(FOR);
				setState(87);
				match(LPAR);
				setState(88);
				vardeclare();
				setState(89);
				match(SEMI);
				setState(90);
				expr(0);
				setState(91);
				match(SEMI);
				setState(92);
				vardeclare();
				setState(93);
				match(RPAR);
				setState(94);
				match(LBRACE);
				setState(95);
				program();
				setState(96);
				match(RBRACE);
				}
				break;
			case 7:
				_localctx = new FuncalldeclContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(98);
				funcall();
				setState(99);
				match(SEMI);
				}
				break;
			case 8:
				_localctx = new ReturndeclContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(101);
				match(RETURN);
				setState(102);
				expr(0);
				setState(103);
				match(SEMI);
				}
				break;
			case 9:
				_localctx = new ThreaddeclContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(105);
				match(THREAD);
				setState(106);
				match(ID);
				setState(107);
				block();
				}
				break;
			case 10:
				_localctx = new JoindeclContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(108);
				match(JOIN);
				setState(109);
				match(ID);
				setState(110);
				match(SEMI);
				}
				break;
			case 11:
				_localctx = new SyncdeclContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(111);
				match(SYNC);
				setState(112);
				block();
				}
				break;
			case 12:
				_localctx = new RecorddeclContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(113);
				match(DEF);
				setState(114);
				match(RECORD);
				setState(115);
				identifier();
				setState(116);
				match(LBRACE);
				setState(120);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << VOID) | (1L << CHAR) | (1L << NULL) | (1L << INT) | (1L << THREAD) | (1L << ID))) != 0)) {
					{
					{
					setState(117);
					recordvartpye();
					}
					}
					setState(122);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(123);
				match(RBRACE);
				}
				break;
			case 13:
				_localctx = new PrintdeclContext(_localctx);
				enterOuterAlt(_localctx, 13);
				{
				setState(125);
				match(PRINT);
				setState(126);
				expr(0);
				setState(127);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RecordvartpyeContext extends ParserRuleContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public TerminalNode SEMI() { return getToken(PyVaParser.SEMI, 0); }
		public RecordvartpyeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_recordvartpye; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterRecordvartpye(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitRecordvartpye(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitRecordvartpye(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RecordvartpyeContext recordvartpye() throws RecognitionException {
		RecordvartpyeContext _localctx = new RecordvartpyeContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_recordvartpye);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(131);
			identifier();
			setState(132);
			match(ID);
			setState(133);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode LBRACE() { return getToken(PyVaParser.LBRACE, 0); }
		public ProgramContext program() {
			return getRuleContext(ProgramContext.class,0);
		}
		public TerminalNode RBRACE() { return getToken(PyVaParser.RBRACE, 0); }
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitBlock(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_block);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(135);
			match(LBRACE);
			setState(136);
			program();
			setState(137);
			match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VardeclareContext extends ParserRuleContext {
		public VardeclareContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_vardeclare; }
	 
		public VardeclareContext() { }
		public void copyFrom(VardeclareContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class TypelessvardeclContext extends VardeclareContext {
		public List<IdCallContext> idCall() {
			return getRuleContexts(IdCallContext.class);
		}
		public IdCallContext idCall(int i) {
			return getRuleContext(IdCallContext.class,i);
		}
		public List<TerminalNode> ASS() { return getTokens(PyVaParser.ASS); }
		public TerminalNode ASS(int i) {
			return getToken(PyVaParser.ASS, i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TypelessvardeclContext(VardeclareContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterTypelessvardecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitTypelessvardecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitTypelessvardecl(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypevardeclContext extends VardeclareContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public List<TerminalNode> ID() { return getTokens(PyVaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(PyVaParser.ID, i);
		}
		public List<TerminalNode> ASS() { return getTokens(PyVaParser.ASS); }
		public TerminalNode ASS(int i) {
			return getToken(PyVaParser.ASS, i);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TypevardeclContext(VardeclareContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterTypevardecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitTypevardecl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitTypevardecl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final VardeclareContext vardeclare() throws RecognitionException {
		VardeclareContext _localctx = new VardeclareContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_vardeclare);
		try {
			int _alt;
			setState(162);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				_localctx = new TypelessvardeclContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(139);
				idCall();
				setState(144);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(140);
						match(ASS);
						setState(141);
						idCall();
						}
						} 
					}
					setState(146);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
				}
				setState(147);
				match(ASS);
				setState(148);
				expr(0);
				}
				break;
			case 2:
				_localctx = new TypevardeclContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(150);
				identifier();
				setState(151);
				match(ID);
				setState(156);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(152);
						match(ASS);
						setState(153);
						match(ID);
						}
						} 
					}
					setState(158);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
				}
				setState(159);
				match(ASS);
				setState(160);
				expr(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayInitContext extends ParserRuleContext {
		public ArrayInitContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayInit; }
	 
		public ArrayInitContext() { }
		public void copyFrom(ArrayInitContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class SizedArrayInitContext extends ArrayInitContext {
		public TerminalNode NEW() { return getToken(PyVaParser.NEW, 0); }
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public List<TerminalNode> OBRACK() { return getTokens(PyVaParser.OBRACK); }
		public TerminalNode OBRACK(int i) {
			return getToken(PyVaParser.OBRACK, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> CBRACK() { return getTokens(PyVaParser.CBRACK); }
		public TerminalNode CBRACK(int i) {
			return getToken(PyVaParser.CBRACK, i);
		}
		public SizedArrayInitContext(ArrayInitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterSizedArrayInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitSizedArrayInit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitSizedArrayInit(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ValuedArrayInitContext extends ArrayInitContext {
		public TerminalNode OBRACK() { return getToken(PyVaParser.OBRACK, 0); }
		public TerminalNode CBRACK() { return getToken(PyVaParser.CBRACK, 0); }
		public ArrayValueContext arrayValue() {
			return getRuleContext(ArrayValueContext.class,0);
		}
		public ValuedArrayInitContext(ArrayInitContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterValuedArrayInit(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitValuedArrayInit(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitValuedArrayInit(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrayInitContext arrayInit() throws RecognitionException {
		ArrayInitContext _localctx = new ArrayInitContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_arrayInit);
		int _la;
		try {
			int _alt;
			setState(186);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case OBRACK:
				_localctx = new ValuedArrayInitContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(164);
				match(OBRACK);
				setState(166);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NEW) | (1L << NOT) | (1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << LPAR) | (1L << MINUS) | (1L << OBRACK) | (1L << ID) | (1L << NUM) | (1L << STRING) | (1L << CHR))) != 0)) {
					{
					setState(165);
					arrayValue();
					}
				}

				setState(168);
				match(CBRACK);
				}
				break;
			case NEW:
				_localctx = new SizedArrayInitContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(169);
				match(NEW);
				setState(170);
				identifier();
				setState(175); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(171);
						match(OBRACK);
						setState(172);
						expr(0);
						setState(173);
						match(CBRACK);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(177); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				setState(183);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(179);
						match(OBRACK);
						setState(180);
						match(CBRACK);
						}
						} 
					}
					setState(185);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayValueContext extends ParserRuleContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(PyVaParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(PyVaParser.COMMA, i);
		}
		public ArrayValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterArrayValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitArrayValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitArrayValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrayValueContext arrayValue() throws RecognitionException {
		ArrayValueContext _localctx = new ArrayValueContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_arrayValue);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(188);
			expr(0);
			setState(193);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(189);
				match(COMMA);
				setState(190);
				expr(0);
				}
				}
				setState(195);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CharExprContext extends ExprContext {
		public TerminalNode CHR() { return getToken(PyVaParser.CHR, 0); }
		public CharExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterCharExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitCharExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitCharExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class RecordExprContext extends ExprContext {
		public TerminalNode NEW() { return getToken(PyVaParser.NEW, 0); }
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public RecordExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterRecordExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitRecordExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitRecordExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NullExprContext extends ExprContext {
		public TerminalNode NULL() { return getToken(PyVaParser.NULL, 0); }
		public NullExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterNullExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitNullExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitNullExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TrueExprContext extends ExprContext {
		public TerminalNode TRUE() { return getToken(PyVaParser.TRUE, 0); }
		public TrueExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterTrueExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitTrueExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitTrueExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayInitExprContext extends ExprContext {
		public ArrayInitContext arrayInit() {
			return getRuleContext(ArrayInitContext.class,0);
		}
		public ArrayInitExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterArrayInitExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitArrayInitExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitArrayInitExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MultExprContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public MultOpContext multOp() {
			return getRuleContext(MultOpContext.class,0);
		}
		public MultExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterMultExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitMultExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitMultExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NumExprContext extends ExprContext {
		public TerminalNode NUM() { return getToken(PyVaParser.NUM, 0); }
		public NumExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterNumExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitNumExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitNumExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PlusExprContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public PlusOpContext plusOp() {
			return getRuleContext(PlusOpContext.class,0);
		}
		public PlusExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterPlusExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitPlusExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitPlusExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringExprContext extends ExprContext {
		public TerminalNode STRING() { return getToken(PyVaParser.STRING, 0); }
		public StringExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterStringExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitStringExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitStringExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParExprContext extends ExprContext {
		public TerminalNode LPAR() { return getToken(PyVaParser.LPAR, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode RPAR() { return getToken(PyVaParser.RPAR, 0); }
		public ParExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterParExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitParExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitParExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FuncallExprContext extends ExprContext {
		public FuncallContext funcall() {
			return getRuleContext(FuncallContext.class,0);
		}
		public FuncallExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterFuncallExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitFuncallExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitFuncallExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class CompExprContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public CompOpContext compOp() {
			return getRuleContext(CompOpContext.class,0);
		}
		public CompExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterCompExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitCompExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitCompExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrfExprContext extends ExprContext {
		public PrfOpContext prfOp() {
			return getRuleContext(PrfOpContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public PrfExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterPrfExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitPrfExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitPrfExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class FalseExprContext extends ExprContext {
		public TerminalNode FALSE() { return getToken(PyVaParser.FALSE, 0); }
		public FalseExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterFalseExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitFalseExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitFalseExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolExprContext extends ExprContext {
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public BoolOpContext boolOp() {
			return getRuleContext(BoolOpContext.class,0);
		}
		public BoolExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterBoolExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitBoolExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitBoolExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdExprContext extends ExprContext {
		public IdCallContext idCall() {
			return getRuleContext(IdCallContext.class,0);
		}
		public IdExprContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterIdExpr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitIdExpr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitIdExpr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 16;
		enterRecursionRule(_localctx, 16, RULE_expr, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(215);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				_localctx = new PrfExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(197);
				prfOp();
				setState(198);
				expr(16);
				}
				break;
			case 2:
				{
				_localctx = new ParExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(200);
				match(LPAR);
				setState(201);
				expr(0);
				setState(202);
				match(RPAR);
				}
				break;
			case 3:
				{
				_localctx = new FuncallExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(204);
				funcall();
				}
				break;
			case 4:
				{
				_localctx = new IdExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(205);
				idCall();
				}
				break;
			case 5:
				{
				_localctx = new ArrayInitExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(206);
				arrayInit();
				}
				break;
			case 6:
				{
				_localctx = new CharExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(207);
				match(CHR);
				}
				break;
			case 7:
				{
				_localctx = new NumExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(208);
				match(NUM);
				}
				break;
			case 8:
				{
				_localctx = new TrueExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(209);
				match(TRUE);
				}
				break;
			case 9:
				{
				_localctx = new FalseExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(210);
				match(FALSE);
				}
				break;
			case 10:
				{
				_localctx = new StringExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(211);
				match(STRING);
				}
				break;
			case 11:
				{
				_localctx = new RecordExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(212);
				match(NEW);
				setState(213);
				match(ID);
				}
				break;
			case 12:
				{
				_localctx = new NullExprContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(214);
				match(NULL);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(235);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(233);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
					case 1:
						{
						_localctx = new MultExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(217);
						if (!(precpred(_ctx, 15))) throw new FailedPredicateException(this, "precpred(_ctx, 15)");
						setState(218);
						multOp();
						setState(219);
						expr(16);
						}
						break;
					case 2:
						{
						_localctx = new PlusExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(221);
						if (!(precpred(_ctx, 14))) throw new FailedPredicateException(this, "precpred(_ctx, 14)");
						setState(222);
						plusOp();
						setState(223);
						expr(15);
						}
						break;
					case 3:
						{
						_localctx = new CompExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(225);
						if (!(precpred(_ctx, 13))) throw new FailedPredicateException(this, "precpred(_ctx, 13)");
						setState(226);
						compOp();
						setState(227);
						expr(14);
						}
						break;
					case 4:
						{
						_localctx = new BoolExprContext(new ExprContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(229);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(230);
						boolOp();
						setState(231);
						expr(13);
						}
						break;
					}
					} 
				}
				setState(237);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,15,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class IdCallContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public List<AccesserContext> accesser() {
			return getRuleContexts(AccesserContext.class);
		}
		public AccesserContext accesser(int i) {
			return getRuleContext(AccesserContext.class,i);
		}
		public IdCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_idCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterIdCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitIdCall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitIdCall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdCallContext idCall() throws RecognitionException {
		IdCallContext _localctx = new IdCallContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_idCall);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(238);
			match(ID);
			setState(242);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(239);
					accesser();
					}
					} 
				}
				setState(244);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AccesserContext extends ParserRuleContext {
		public TerminalNode DOT() { return getToken(PyVaParser.DOT, 0); }
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public List<TerminalNode> OBRACK() { return getTokens(PyVaParser.OBRACK); }
		public TerminalNode OBRACK(int i) {
			return getToken(PyVaParser.OBRACK, i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> CBRACK() { return getTokens(PyVaParser.CBRACK); }
		public TerminalNode CBRACK(int i) {
			return getToken(PyVaParser.CBRACK, i);
		}
		public AccesserContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_accesser; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterAccesser(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitAccesser(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitAccesser(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AccesserContext accesser() throws RecognitionException {
		AccesserContext _localctx = new AccesserContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_accesser);
		try {
			int _alt;
			setState(255);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case DOT:
				enterOuterAlt(_localctx, 1);
				{
				setState(245);
				match(DOT);
				setState(246);
				match(ID);
				}
				break;
			case OBRACK:
				enterOuterAlt(_localctx, 2);
				{
				setState(251); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(247);
						match(OBRACK);
						setState(248);
						expr(0);
						setState(249);
						match(CBRACK);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(253); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,17,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParamsContext extends ParserRuleContext {
		public List<IdentifierContext> identifier() {
			return getRuleContexts(IdentifierContext.class);
		}
		public IdentifierContext identifier(int i) {
			return getRuleContext(IdentifierContext.class,i);
		}
		public List<TerminalNode> ID() { return getTokens(PyVaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(PyVaParser.ID, i);
		}
		public List<TerminalNode> COMMA() { return getTokens(PyVaParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(PyVaParser.COMMA, i);
		}
		public ParamsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_params; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterParams(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitParams(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitParams(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParamsContext params() throws RecognitionException {
		ParamsContext _localctx = new ParamsContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_params);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(268);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << VOID) | (1L << CHAR) | (1L << NULL) | (1L << INT) | (1L << THREAD) | (1L << ID))) != 0)) {
				{
				setState(257);
				identifier();
				setState(258);
				match(ID);
				setState(265);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(259);
					match(COMMA);
					setState(260);
					identifier();
					setState(261);
					match(ID);
					}
					}
					setState(267);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimitiveTypeContext extends ParserRuleContext {
		public TerminalNode BOOLEAN() { return getToken(PyVaParser.BOOLEAN, 0); }
		public TerminalNode INT() { return getToken(PyVaParser.INT, 0); }
		public TerminalNode CHAR() { return getToken(PyVaParser.CHAR, 0); }
		public TerminalNode NULL() { return getToken(PyVaParser.NULL, 0); }
		public TerminalNode VOID() { return getToken(PyVaParser.VOID, 0); }
		public PrimitiveTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primitiveType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterPrimitiveType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitPrimitiveType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitPrimitiveType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrimitiveTypeContext primitiveType() throws RecognitionException {
		PrimitiveTypeContext _localctx = new PrimitiveTypeContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_primitiveType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(270);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << BOOLEAN) | (1L << VOID) | (1L << CHAR) | (1L << NULL) | (1L << INT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentifierContext extends ParserRuleContext {
		public PrimitiveTypeContext primitiveType() {
			return getRuleContext(PrimitiveTypeContext.class,0);
		}
		public ArrayTypeContext arrayType() {
			return getRuleContext(ArrayTypeContext.class,0);
		}
		public CompoundTypeContext compoundType() {
			return getRuleContext(CompoundTypeContext.class,0);
		}
		public IdentifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterIdentifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitIdentifier(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitIdentifier(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IdentifierContext identifier() throws RecognitionException {
		IdentifierContext _localctx = new IdentifierContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_identifier);
		try {
			setState(275);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(272);
				primitiveType();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(273);
				arrayType();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(274);
				compoundType();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayTypeContext extends ParserRuleContext {
		public TerminalNode BOOLEAN() { return getToken(PyVaParser.BOOLEAN, 0); }
		public List<TerminalNode> OBRACK() { return getTokens(PyVaParser.OBRACK); }
		public TerminalNode OBRACK(int i) {
			return getToken(PyVaParser.OBRACK, i);
		}
		public List<TerminalNode> CBRACK() { return getTokens(PyVaParser.CBRACK); }
		public TerminalNode CBRACK(int i) {
			return getToken(PyVaParser.CBRACK, i);
		}
		public TerminalNode INT() { return getToken(PyVaParser.INT, 0); }
		public TerminalNode CHAR() { return getToken(PyVaParser.CHAR, 0); }
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public ArrayTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterArrayType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitArrayType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitArrayType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArrayTypeContext arrayType() throws RecognitionException {
		ArrayTypeContext _localctx = new ArrayTypeContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_arrayType);
		try {
			int _alt;
			setState(305);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case BOOLEAN:
				enterOuterAlt(_localctx, 1);
				{
				setState(277);
				match(BOOLEAN);
				setState(280); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(278);
						match(OBRACK);
						setState(279);
						match(CBRACK);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(282); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case INT:
				enterOuterAlt(_localctx, 2);
				{
				setState(284);
				match(INT);
				setState(287); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(285);
						match(OBRACK);
						setState(286);
						match(CBRACK);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(289); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case CHAR:
				enterOuterAlt(_localctx, 3);
				{
				setState(291);
				match(CHAR);
				setState(294); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(292);
						match(OBRACK);
						setState(293);
						match(CBRACK);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(296); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			case ID:
				enterOuterAlt(_localctx, 4);
				{
				setState(298);
				match(ID);
				setState(301); 
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(299);
						match(OBRACK);
						setState(300);
						match(CBRACK);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(303); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,25,_ctx);
				} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompoundTypeContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public TerminalNode THREAD() { return getToken(PyVaParser.THREAD, 0); }
		public CompoundTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compoundType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterCompoundType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitCompoundType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitCompoundType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompoundTypeContext compoundType() throws RecognitionException {
		CompoundTypeContext _localctx = new CompoundTypeContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_compoundType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(307);
			_la = _input.LA(1);
			if ( !(_la==THREAD || _la==ID) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncTypeContext extends ParserRuleContext {
		public FuncTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcType; }
	 
		public FuncTypeContext() { }
		public void copyFrom(FuncTypeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class VoidFuncTypeContext extends FuncTypeContext {
		public TerminalNode VOID() { return getToken(PyVaParser.VOID, 0); }
		public VoidFuncTypeContext(FuncTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterVoidFuncType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitVoidFuncType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitVoidFuncType(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class TypeFuncTypeContext extends FuncTypeContext {
		public IdentifierContext identifier() {
			return getRuleContext(IdentifierContext.class,0);
		}
		public TypeFuncTypeContext(FuncTypeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterTypeFuncType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitTypeFuncType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitTypeFuncType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncTypeContext funcType() throws RecognitionException {
		FuncTypeContext _localctx = new FuncTypeContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_funcType);
		try {
			setState(311);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				_localctx = new TypeFuncTypeContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(309);
				identifier();
				}
				break;
			case 2:
				_localctx = new VoidFuncTypeContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(310);
				match(VOID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FuncallContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(PyVaParser.ID, 0); }
		public TerminalNode LPAR() { return getToken(PyVaParser.LPAR, 0); }
		public TerminalNode RPAR() { return getToken(PyVaParser.RPAR, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(PyVaParser.COMMA); }
		public TerminalNode COMMA(int i) {
			return getToken(PyVaParser.COMMA, i);
		}
		public FuncallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_funcall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterFuncall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitFuncall(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitFuncall(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FuncallContext funcall() throws RecognitionException {
		FuncallContext _localctx = new FuncallContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_funcall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(313);
			match(ID);
			setState(314);
			match(LPAR);
			setState(323);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NEW) | (1L << NOT) | (1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << LPAR) | (1L << MINUS) | (1L << OBRACK) | (1L << ID) | (1L << NUM) | (1L << STRING) | (1L << CHR))) != 0)) {
				{
				setState(315);
				expr(0);
				setState(320);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(316);
					match(COMMA);
					setState(317);
					expr(0);
					}
					}
					setState(322);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(325);
			match(RPAR);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrfOpContext extends ParserRuleContext {
		public PrfOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prfOp; }
	 
		public PrfOpContext() { }
		public void copyFrom(PrfOpContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class NotPrfOpContext extends PrfOpContext {
		public TerminalNode NOT() { return getToken(PyVaParser.NOT, 0); }
		public NotPrfOpContext(PrfOpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterNotPrfOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitNotPrfOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitNotPrfOp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MinusPrfOpContext extends PrfOpContext {
		public TerminalNode MINUS() { return getToken(PyVaParser.MINUS, 0); }
		public MinusPrfOpContext(PrfOpContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterMinusPrfOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitMinusPrfOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitMinusPrfOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PrfOpContext prfOp() throws RecognitionException {
		PrfOpContext _localctx = new PrfOpContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_prfOp);
		try {
			setState(329);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case MINUS:
				_localctx = new MinusPrfOpContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(327);
				match(MINUS);
				}
				break;
			case NOT:
				_localctx = new NotPrfOpContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(328);
				match(NOT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultOpContext extends ParserRuleContext {
		public TerminalNode STAR() { return getToken(PyVaParser.STAR, 0); }
		public TerminalNode SLASH() { return getToken(PyVaParser.SLASH, 0); }
		public MultOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterMultOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitMultOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitMultOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultOpContext multOp() throws RecognitionException {
		MultOpContext _localctx = new MultOpContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_multOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(331);
			_la = _input.LA(1);
			if ( !(_la==SLASH || _la==STAR) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlusOpContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(PyVaParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(PyVaParser.MINUS, 0); }
		public PlusOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plusOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterPlusOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitPlusOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitPlusOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PlusOpContext plusOp() throws RecognitionException {
		PlusOpContext _localctx = new PlusOpContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_plusOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(333);
			_la = _input.LA(1);
			if ( !(_la==MINUS || _la==PLUS) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolOpContext extends ParserRuleContext {
		public TerminalNode AND() { return getToken(PyVaParser.AND, 0); }
		public TerminalNode OR() { return getToken(PyVaParser.OR, 0); }
		public BoolOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterBoolOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitBoolOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitBoolOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BoolOpContext boolOp() throws RecognitionException {
		BoolOpContext _localctx = new BoolOpContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_boolOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(335);
			_la = _input.LA(1);
			if ( !(_la==OR || _la==AND) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompOpContext extends ParserRuleContext {
		public TerminalNode LE() { return getToken(PyVaParser.LE, 0); }
		public TerminalNode LT() { return getToken(PyVaParser.LT, 0); }
		public TerminalNode GE() { return getToken(PyVaParser.GE, 0); }
		public TerminalNode GT() { return getToken(PyVaParser.GT, 0); }
		public TerminalNode EQ() { return getToken(PyVaParser.EQ, 0); }
		public TerminalNode NE() { return getToken(PyVaParser.NE, 0); }
		public CompOpContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compOp; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).enterCompOp(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PyVaListener ) ((PyVaListener)listener).exitCompOp(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PyVaVisitor ) return ((PyVaVisitor<? extends T>)visitor).visitCompOp(this);
			else return visitor.visitChildren(this);
		}
	}

	public final CompOpContext compOp() throws RecognitionException {
		CompOpContext _localctx = new CompOpContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_compOp);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(337);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << EQ) | (1L << GE) | (1L << GT) | (1L << LE) | (1L << LT) | (1L << NE))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 8:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 15);
		case 1:
			return precpred(_ctx, 14);
		case 2:
			return precpred(_ctx, 13);
		case 3:
			return precpred(_ctx, 12);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3;\u0156\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\3\2\6\2\62"+
		"\n\2\r\2\16\2\63\3\3\7\3\67\n\3\f\3\16\3:\13\3\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4Q\n\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\7\4y\n\4\f\4\16\4|\13\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4\u0084"+
		"\n\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\7\7\u0091\n\7\f\7\16"+
		"\7\u0094\13\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\7\7\u009d\n\7\f\7\16\7\u00a0"+
		"\13\7\3\7\3\7\3\7\5\7\u00a5\n\7\3\b\3\b\5\b\u00a9\n\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\6\b\u00b2\n\b\r\b\16\b\u00b3\3\b\3\b\7\b\u00b8\n\b\f\b\16"+
		"\b\u00bb\13\b\5\b\u00bd\n\b\3\t\3\t\3\t\7\t\u00c2\n\t\f\t\16\t\u00c5\13"+
		"\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\5\n\u00da\n\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\7\n\u00ec\n\n\f\n\16\n\u00ef\13\n\3\13\3\13\7\13\u00f3"+
		"\n\13\f\13\16\13\u00f6\13\13\3\f\3\f\3\f\3\f\3\f\3\f\6\f\u00fe\n\f\r\f"+
		"\16\f\u00ff\5\f\u0102\n\f\3\r\3\r\3\r\3\r\3\r\3\r\7\r\u010a\n\r\f\r\16"+
		"\r\u010d\13\r\5\r\u010f\n\r\3\16\3\16\3\17\3\17\3\17\5\17\u0116\n\17\3"+
		"\20\3\20\3\20\6\20\u011b\n\20\r\20\16\20\u011c\3\20\3\20\3\20\6\20\u0122"+
		"\n\20\r\20\16\20\u0123\3\20\3\20\3\20\6\20\u0129\n\20\r\20\16\20\u012a"+
		"\3\20\3\20\3\20\6\20\u0130\n\20\r\20\16\20\u0131\5\20\u0134\n\20\3\21"+
		"\3\21\3\22\3\22\5\22\u013a\n\22\3\23\3\23\3\23\3\23\3\23\7\23\u0141\n"+
		"\23\f\23\16\23\u0144\13\23\5\23\u0146\n\23\3\23\3\23\3\24\3\24\5\24\u014c"+
		"\n\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\30\2\3\22\31\2\4\6\b\n"+
		"\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\2\b\3\2\f\20\4\2--\64\64\3\2)*"+
		"\4\2##%%\3\2\23\24\5\2\34\37\"\"$$\2\u0177\2\61\3\2\2\2\48\3\2\2\2\6\u0083"+
		"\3\2\2\2\b\u0085\3\2\2\2\n\u0089\3\2\2\2\f\u00a4\3\2\2\2\16\u00bc\3\2"+
		"\2\2\20\u00be\3\2\2\2\22\u00d9\3\2\2\2\24\u00f0\3\2\2\2\26\u0101\3\2\2"+
		"\2\30\u010e\3\2\2\2\32\u0110\3\2\2\2\34\u0115\3\2\2\2\36\u0133\3\2\2\2"+
		" \u0135\3\2\2\2\"\u0139\3\2\2\2$\u013b\3\2\2\2&\u014b\3\2\2\2(\u014d\3"+
		"\2\2\2*\u014f\3\2\2\2,\u0151\3\2\2\2.\u0153\3\2\2\2\60\62\5\6\4\2\61\60"+
		"\3\2\2\2\62\63\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2\2\64\3\3\2\2\2\65\67"+
		"\5\6\4\2\66\65\3\2\2\2\67:\3\2\2\28\66\3\2\2\289\3\2\2\29\5\3\2\2\2:8"+
		"\3\2\2\2;<\7\25\2\2<=\5\"\22\2=>\7\64\2\2>?\7!\2\2?@\5\30\r\2@A\7\'\2"+
		"\2AB\7 \2\2BC\5\4\3\2CD\7&\2\2D\u0084\3\2\2\2E\u0084\5\n\6\2FG\5\f\7\2"+
		"GH\7(\2\2H\u0084\3\2\2\2IJ\7\6\2\2JK\7!\2\2KL\5\22\n\2LM\7\'\2\2MP\5\n"+
		"\6\2NO\7\7\2\2OQ\5\n\6\2PN\3\2\2\2PQ\3\2\2\2Q\u0084\3\2\2\2RS\7\t\2\2"+
		"ST\7!\2\2TU\5\22\n\2UV\7\'\2\2VW\5\n\6\2W\u0084\3\2\2\2XY\7\b\2\2YZ\7"+
		"!\2\2Z[\5\f\7\2[\\\7(\2\2\\]\5\22\n\2]^\7(\2\2^_\5\f\7\2_`\7\'\2\2`a\7"+
		" \2\2ab\5\4\3\2bc\7&\2\2c\u0084\3\2\2\2de\5$\23\2ef\7(\2\2f\u0084\3\2"+
		"\2\2gh\7\n\2\2hi\5\22\n\2ij\7(\2\2j\u0084\3\2\2\2kl\7-\2\2lm\7\64\2\2"+
		"m\u0084\5\n\6\2no\7.\2\2op\7\64\2\2p\u0084\7(\2\2qr\7/\2\2r\u0084\5\n"+
		"\6\2st\7\25\2\2tu\7\63\2\2uv\5\34\17\2vz\7 \2\2wy\5\b\5\2xw\3\2\2\2y|"+
		"\3\2\2\2zx\3\2\2\2z{\3\2\2\2{}\3\2\2\2|z\3\2\2\2}~\7&\2\2~\u0084\3\2\2"+
		"\2\177\u0080\7\3\2\2\u0080\u0081\5\22\n\2\u0081\u0082\7(\2\2\u0082\u0084"+
		"\3\2\2\2\u0083;\3\2\2\2\u0083E\3\2\2\2\u0083F\3\2\2\2\u0083I\3\2\2\2\u0083"+
		"R\3\2\2\2\u0083X\3\2\2\2\u0083d\3\2\2\2\u0083g\3\2\2\2\u0083k\3\2\2\2"+
		"\u0083n\3\2\2\2\u0083q\3\2\2\2\u0083s\3\2\2\2\u0083\177\3\2\2\2\u0084"+
		"\7\3\2\2\2\u0085\u0086\5\34\17\2\u0086\u0087\7\64\2\2\u0087\u0088\7(\2"+
		"\2\u0088\t\3\2\2\2\u0089\u008a\7 \2\2\u008a\u008b\5\4\3\2\u008b\u008c"+
		"\7&\2\2\u008c\13\3\2\2\2\u008d\u0092\5\24\13\2\u008e\u008f\7\26\2\2\u008f"+
		"\u0091\5\24\13\2\u0090\u008e\3\2\2\2\u0091\u0094\3\2\2\2\u0092\u0090\3"+
		"\2\2\2\u0092\u0093\3\2\2\2\u0093\u0095\3\2\2\2\u0094\u0092\3\2\2\2\u0095"+
		"\u0096\7\26\2\2\u0096\u0097\5\22\n\2\u0097\u00a5\3\2\2\2\u0098\u0099\5"+
		"\34\17\2\u0099\u009e\7\64\2\2\u009a\u009b\7\26\2\2\u009b\u009d\7\64\2"+
		"\2\u009c\u009a\3\2\2\2\u009d\u00a0\3\2\2\2\u009e\u009c\3\2\2\2\u009e\u009f"+
		"\3\2\2\2\u009f\u00a1\3\2\2\2\u00a0\u009e\3\2\2\2\u00a1\u00a2\7\26\2\2"+
		"\u00a2\u00a3\5\22\n\2\u00a3\u00a5\3\2\2\2\u00a4\u008d\3\2\2\2\u00a4\u0098"+
		"\3\2\2\2\u00a5\r\3\2\2\2\u00a6\u00a8\7+\2\2\u00a7\u00a9\5\20\t\2\u00a8"+
		"\u00a7\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00bd\7,"+
		"\2\2\u00ab\u00ac\7\5\2\2\u00ac\u00b1\5\34\17\2\u00ad\u00ae\7+\2\2\u00ae"+
		"\u00af\5\22\n\2\u00af\u00b0\7,\2\2\u00b0\u00b2\3\2\2\2\u00b1\u00ad\3\2"+
		"\2\2\u00b2\u00b3\3\2\2\2\u00b3\u00b1\3\2\2\2\u00b3\u00b4\3\2\2\2\u00b4"+
		"\u00b9\3\2\2\2\u00b5\u00b6\7+\2\2\u00b6\u00b8\7,\2\2\u00b7\u00b5\3\2\2"+
		"\2\u00b8\u00bb\3\2\2\2\u00b9\u00b7\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba\u00bd"+
		"\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bc\u00a6\3\2\2\2\u00bc\u00ab\3\2\2\2\u00bd"+
		"\17\3\2\2\2\u00be\u00c3\5\22\n\2\u00bf\u00c0\7\30\2\2\u00c0\u00c2\5\22"+
		"\n\2\u00c1\u00bf\3\2\2\2\u00c2\u00c5\3\2\2\2\u00c3\u00c1\3\2\2\2\u00c3"+
		"\u00c4\3\2\2\2\u00c4\21\3\2\2\2\u00c5\u00c3\3\2\2\2\u00c6\u00c7\b\n\1"+
		"\2\u00c7\u00c8\5&\24\2\u00c8\u00c9\5\22\n\22\u00c9\u00da\3\2\2\2\u00ca"+
		"\u00cb\7!\2\2\u00cb\u00cc\5\22\n\2\u00cc\u00cd\7\'\2\2\u00cd\u00da\3\2"+
		"\2\2\u00ce\u00da\5$\23\2\u00cf\u00da\5\24\13\2\u00d0\u00da\5\16\b\2\u00d1"+
		"\u00da\7\67\2\2\u00d2\u00da\7\65\2\2\u00d3\u00da\7\21\2\2\u00d4\u00da"+
		"\7\22\2\2\u00d5\u00da\7\66\2\2\u00d6\u00d7\7\5\2\2\u00d7\u00da\7\64\2"+
		"\2\u00d8\u00da\7\17\2\2\u00d9\u00c6\3\2\2\2\u00d9\u00ca\3\2\2\2\u00d9"+
		"\u00ce\3\2\2\2\u00d9\u00cf\3\2\2\2\u00d9\u00d0\3\2\2\2\u00d9\u00d1\3\2"+
		"\2\2\u00d9\u00d2\3\2\2\2\u00d9\u00d3\3\2\2\2\u00d9\u00d4\3\2\2\2\u00d9"+
		"\u00d5\3\2\2\2\u00d9\u00d6\3\2\2\2\u00d9\u00d8\3\2\2\2\u00da\u00ed\3\2"+
		"\2\2\u00db\u00dc\f\21\2\2\u00dc\u00dd\5(\25\2\u00dd\u00de\5\22\n\22\u00de"+
		"\u00ec\3\2\2\2\u00df\u00e0\f\20\2\2\u00e0\u00e1\5*\26\2\u00e1\u00e2\5"+
		"\22\n\21\u00e2\u00ec\3\2\2\2\u00e3\u00e4\f\17\2\2\u00e4\u00e5\5.\30\2"+
		"\u00e5\u00e6\5\22\n\20\u00e6\u00ec\3\2\2\2\u00e7\u00e8\f\16\2\2\u00e8"+
		"\u00e9\5,\27\2\u00e9\u00ea\5\22\n\17\u00ea\u00ec\3\2\2\2\u00eb\u00db\3"+
		"\2\2\2\u00eb\u00df\3\2\2\2\u00eb\u00e3\3\2\2\2\u00eb\u00e7\3\2\2\2\u00ec"+
		"\u00ef\3\2\2\2\u00ed\u00eb\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\23\3\2\2"+
		"\2\u00ef\u00ed\3\2\2\2\u00f0\u00f4\7\64\2\2\u00f1\u00f3\5\26\f\2\u00f2"+
		"\u00f1\3\2\2\2\u00f3\u00f6\3\2\2\2\u00f4\u00f2\3\2\2\2\u00f4\u00f5\3\2"+
		"\2\2\u00f5\25\3\2\2\2\u00f6\u00f4\3\2\2\2\u00f7\u00f8\7\31\2\2\u00f8\u0102"+
		"\7\64\2\2\u00f9\u00fa\7+\2\2\u00fa\u00fb\5\22\n\2\u00fb\u00fc\7,\2\2\u00fc"+
		"\u00fe\3\2\2\2\u00fd\u00f9\3\2\2\2\u00fe\u00ff\3\2\2\2\u00ff\u00fd\3\2"+
		"\2\2\u00ff\u0100\3\2\2\2\u0100\u0102\3\2\2\2\u0101\u00f7\3\2\2\2\u0101"+
		"\u00fd\3\2\2\2\u0102\27\3\2\2\2\u0103\u0104\5\34\17\2\u0104\u010b\7\64"+
		"\2\2\u0105\u0106\7\30\2\2\u0106\u0107\5\34\17\2\u0107\u0108\7\64\2\2\u0108"+
		"\u010a\3\2\2\2\u0109\u0105\3\2\2\2\u010a\u010d\3\2\2\2\u010b\u0109\3\2"+
		"\2\2\u010b\u010c\3\2\2\2\u010c\u010f\3\2\2\2\u010d\u010b\3\2\2\2\u010e"+
		"\u0103\3\2\2\2\u010e\u010f\3\2\2\2\u010f\31\3\2\2\2\u0110\u0111\t\2\2"+
		"\2\u0111\33\3\2\2\2\u0112\u0116\5\32\16\2\u0113\u0116\5\36\20\2\u0114"+
		"\u0116\5 \21\2\u0115\u0112\3\2\2\2\u0115\u0113\3\2\2\2\u0115\u0114\3\2"+
		"\2\2\u0116\35\3\2\2\2\u0117\u011a\7\f\2\2\u0118\u0119\7+\2\2\u0119\u011b"+
		"\7,\2\2\u011a\u0118\3\2\2\2\u011b\u011c\3\2\2\2\u011c\u011a\3\2\2\2\u011c"+
		"\u011d\3\2\2\2\u011d\u0134\3\2\2\2\u011e\u0121\7\20\2\2\u011f\u0120\7"+
		"+\2\2\u0120\u0122\7,\2\2\u0121\u011f\3\2\2\2\u0122\u0123\3\2\2\2\u0123"+
		"\u0121\3\2\2\2\u0123\u0124\3\2\2\2\u0124\u0134\3\2\2\2\u0125\u0128\7\16"+
		"\2\2\u0126\u0127\7+\2\2\u0127\u0129\7,\2\2\u0128\u0126\3\2\2\2\u0129\u012a"+
		"\3\2\2\2\u012a\u0128\3\2\2\2\u012a\u012b\3\2\2\2\u012b\u0134\3\2\2\2\u012c"+
		"\u012f\7\64\2\2\u012d\u012e\7+\2\2\u012e\u0130\7,\2\2\u012f\u012d\3\2"+
		"\2\2\u0130\u0131\3\2\2\2\u0131\u012f\3\2\2\2\u0131\u0132\3\2\2\2\u0132"+
		"\u0134\3\2\2\2\u0133\u0117\3\2\2\2\u0133\u011e\3\2\2\2\u0133\u0125\3\2"+
		"\2\2\u0133\u012c\3\2\2\2\u0134\37\3\2\2\2\u0135\u0136\t\3\2\2\u0136!\3"+
		"\2\2\2\u0137\u013a\5\34\17\2\u0138\u013a\7\r\2\2\u0139\u0137\3\2\2\2\u0139"+
		"\u0138\3\2\2\2\u013a#\3\2\2\2\u013b\u013c\7\64\2\2\u013c\u0145\7!\2\2"+
		"\u013d\u0142\5\22\n\2\u013e\u013f\7\30\2\2\u013f\u0141\5\22\n\2\u0140"+
		"\u013e\3\2\2\2\u0141\u0144\3\2\2\2\u0142\u0140\3\2\2\2\u0142\u0143\3\2"+
		"\2\2\u0143\u0146\3\2\2\2\u0144\u0142\3\2\2\2\u0145\u013d\3\2\2\2\u0145"+
		"\u0146\3\2\2\2\u0146\u0147\3\2\2\2\u0147\u0148\7\'\2\2\u0148%\3\2\2\2"+
		"\u0149\u014c\7#\2\2\u014a\u014c\7\13\2\2\u014b\u0149\3\2\2\2\u014b\u014a"+
		"\3\2\2\2\u014c\'\3\2\2\2\u014d\u014e\t\4\2\2\u014e)\3\2\2\2\u014f\u0150"+
		"\t\5\2\2\u0150+\3\2\2\2\u0151\u0152\t\6\2\2\u0152-\3\2\2\2\u0153\u0154"+
		"\t\7\2\2\u0154/\3\2\2\2!\638Pz\u0083\u0092\u009e\u00a4\u00a8\u00b3\u00b9"+
		"\u00bc\u00c3\u00d9\u00eb\u00ed\u00f4\u00ff\u0101\u010b\u010e\u0115\u011c"+
		"\u0123\u012a\u0131\u0133\u0139\u0142\u0145\u014b";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}