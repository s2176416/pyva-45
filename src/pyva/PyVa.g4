grammar PyVa;

/* The entirity of the program must have at least one declaration */
goal: decl+;

/* Program consists many declarations */
program: decl*;

/* We call definitions, variable assignments, if while for statements declarations */
decl: DEF funcType ID LPAR params RPAR LBRACE program RBRACE                    #funcdecl // def void printInt(int i) {...}
    | block                                                                     #blockdecl // {...}
    | vardeclare SEMI                                                           #vardecl // int i = a = 0; i = a = 1;
    | IF LPAR expr RPAR block (ELSE block)?                                     #ifdecl // if (i > 1) {...}
    | WHILE LPAR expr RPAR block                                                #whiledecl // while (i < 10) {...}
    | FOR LPAR vardeclare SEMI expr SEMI vardeclare RPAR LBRACE program RBRACE  #fordecl // for (int i = 0; i < 10; i = i + 1) {...}
    | funcall SEMI                                                              #funcalldecl // printInt(1);
    | RETURN expr SEMI                                                          #returndecl // return 1;
    | THREAD ID block                                                           #threaddecl // thread pp {...}
    | JOIN ID SEMI                                                              #joindecl // join pp;
    | SYNC block                                                                #syncdecl // sync {...}
    | DEF RECORD identifier LBRACE recordvartpye* RBRACE                        #recorddecl // def record Coor {int x; int y;}
    | PRINT expr SEMI                                                           #printdecl // print true;
    ;


/* Record declaration */
recordvartpye
    : identifier ID SEMI
    ;

/* A scope block is just many declarations surrounded by curly braces */
block
    : LBRACE program RBRACE
    ;

/* A variable declaration. the first one is for reassignments and the second one is for declarations of new variables */
vardeclare
    : idCall (ASS idCall)* ASS expr         #typelessvardecl // i = a = 2;
    | identifier ID (ASS ID)* ASS expr      #typevardecl // int i = a = 2;
    ;

arrayInit: OBRACK arrayValue? CBRACK                        #valuedArrayInit // [[1], [2], [3]] == int[3][1] regarding size
    | NEW identifier (OBRACK expr CBRACK)+ (OBRACK CBRACK)* #sizedArrayInit // int[4][3][2][], cannot do int[4][][2]
    ;

/* Array initialization with direct values */
arrayValue: (expr (COMMA expr)*); // [1, 2, 3]

/* Accessing an ID. It can look like normal variables (i) or accessings of variables */
idCall: ID accesser*; // e.g. i[2], i.x, i.x[0][2].y

/* The accessers of idCall */
accesser: DOT ID // .x
    | (OBRACK expr CBRACK)+ // [1][2][6/2]
    ;

/* Parameter instances of function definition */
params
    : (identifier ID (COMMA identifier ID)*)? // int i, char c
    ;

/* Primitive types */
primitiveType: BOOLEAN | INT | CHAR | NULL | VOID;

/* The list of possible types. These are the types declared on the left of variable names when declaring new variables. */
identifier: primitiveType | arrayType | compoundType;

/* Array Types */
arrayType: BOOLEAN (OBRACK CBRACK)+
    | INT (OBRACK CBRACK)+
    | CHAR (OBRACK CBRACK)+
    | ID (OBRACK CBRACK)+ //ID refers to user defined identifier here
    ;

/* Custom Types for threads or records */
compoundType: ID | THREAD;

/* Function types can by any type that identifier can allow, or it can be void */
funcType
    : identifier    #typeFuncType
    | VOID          #voidFuncType
    ;

/* All right hand side of assignments are expressions. Expressions can also be used for conditions, parameters, etc. */
expr: prfOp expr        #prfExpr // !true, -2
    | expr multOp expr  #multExpr // 2 * 2, 4 / 2
    | expr plusOp expr  #plusExpr // 2 + 2, 2 - 2
    | expr compOp expr  #compExpr // 2 < 3, 2 <= 3, 4 == 4, 4 != 4, 5 > 4, 5 >= 5
    | expr boolOp expr  #boolExpr // true || false, true && true
    | LPAR expr RPAR    #parExpr // (2 + 2)
    | funcall           #funcallExpr // mod(11, 7)
    | idCall            #idExpr // i
    | arrayInit         #arrayInitExpr // [1, 2, 3], new int[3]
    | CHR               #charExpr // 'a'
    | NUM               #numExpr // 2
    | TRUE              #trueExpr // true
    | FALSE             #falseExpr // false
    | STRING            #stringExpr // "PyVa"
    | NEW ID            #recordExpr // new Coor
    | NULL              #nullExpr // null
    ;

/* A function call expression */
funcall
    : ID LPAR (expr (COMMA expr)*)? RPAR
    ;

prfOp: MINUS    #minusPrfOp
    | NOT       #notPrfOp
    ;

multOp: STAR | SLASH;

plusOp: PLUS | MINUS;

boolOp: AND | OR;

compOp: LE | LT | GE | GT | EQ | NE;

PRINT:  'print';
CLASS:  'class';
NEW:    'new';
IF:     'if';
ELSE:   'else';
FOR:    'for';
WHILE:  'while';
RETURN: 'return';
NOT:    '!';
BOOLEAN:'boolean';
VOID:   'void';
CHAR:   'char';
NULL: 'null';
INT:    'int';
TRUE :  'true';
FALSE:  'false';
OR:     '||';
AND:    '&&';
DEF:    'def';
ASS:    '=';
COLON:  ':';
COMMA:  ',';
DOT:    '.';
DQUOTE: '"';
SQUOTE: ['];
EQ:     '==';
GE:     '>=';
GT:     '>';
LE:     '<=';
LBRACE: '{';
LPAR:   '(';
LT:     '<';
MINUS:  '-';
NE:     '!=';
PLUS:   '+';
RBRACE: '}';
RPAR:   ')';
SEMI:   ';';
SLASH:  '/';
STAR:   '*';
OBRACK: '[';
CBRACK: ']';
THREAD: 'thread';
JOIN:   'join';
SYNC:   'sync';
OCOM:   '/*';
CCOM:   '*/';
DOUBLESLASH: '//';
RECORD: 'record';

ID: LETTER (LETTER | DIGIT)*;
NUM: DIGIT (DIGIT)*;
STRING: DQUOTE .*? DQUOTE;
CHR: SQUOTE .*? SQUOTE;
BOOL: TRUE | FALSE;

WS: [ \t\r\n]+ -> skip;
COMMENT: (OCOM ~[*]* CCOM) -> skip;
LINECOMMENT: DOUBLESLASH ~[\n]* -> skip;

fragment LETTER: [a-zA-Z];
fragment DIGIT: [0-9];