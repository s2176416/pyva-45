package pyva;

import codegeneration.CodeGenerator;
import exceptions.Exception;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import exceptions.passes.SyntaxErrorListener;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;

@SuppressWarnings("Duplicates")
public class PyVa {
    private static final String PYVA_NOTE = "src/pyva/PyVa";

    private static final String COMPLEX =           "src/pyva/exampleprogs/complex";
    private static final String FUNCTION_SCOPE =    "src/pyva/exampleprogs/function-scope-prog";
    private static final String FOR_WHILE_TEST =    "src/pyva/exampleprogs/for_while-prog";
    private static final String FUNCTION_TEST =     "src/pyva/exampleprogs/function-prog";
    private static final String ARRAY_RECORD =      "src/pyva/exampleprogs/array-record-prog";

    private static final String BANKING =   "src/testing/concurrency/programs/banking";
    private static final String PETERSON =  "src/testing/concurrency/programs/peterson";

    private static final String ARRAYS_AS_RECORDS =         "src/testing/semantic/exampleprogs/arraysAsRecords";
    private static final String BUBBLE_SORT =               "src/testing/semantic/exampleprogs/bubble-sort";
    private static final String DYNAMIC_PROG_REM_COINS =    "src/testing/semantic/exampleprogs/dynamic-prog-rem-coins";
    private static final String FEB_DAYS =                  "src/testing/semantic/exampleprogs/feb-days";
    private static final String INSERTION_SORT =            "src/testing/semantic/exampleprogs/insertion-sort";
    private static final String LINKED_LIST =               "src/testing/semantic/exampleprogs/linkedList";
    private static final String PRIMES =                    "src/testing/semantic/exampleprogs/prime";

    public static void main(String[] args) throws Exception, IOException {
        runFile(PYVA_NOTE);
//        generateFile(PYVA_NOTE);
    }

    public static void runFile(String fileName) throws IOException, Exception {
        String contents = null;
        try {
            contents = new String(Files.readAllBytes(Paths.get(fileName + ".txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        runString(contents);
    }

    public static void runString(String input) throws IOException, Exception {
        generateProgram(input);

        String workDir = System.getProperty("user.dir");
        String path;
        ProcessBuilder builder;
        if (System.getProperty("os.name").contains("Windows")) {
            path = workDir + "\\src\\codegeneration";
            builder = new ProcessBuilder(
                    "cmd.exe",
                    "/c",
                    "cd " + path + " && runhaskell ",
                    "Program.hs");
        } else {
            path = workDir + "/src/codegeneration";
            builder = new ProcessBuilder(
                    "bash",
                    "-c",
                    "cd " + path + " && runhaskell ",
                    "Program.hs");
        }
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;

        while (true) {
            line = r.readLine();
            if (line == null) break;
            System.out.println(line);
        }
    }

    public static void generateFile(String fileName) throws Exception, IOException {
        String contents = null;
        try {
            contents = new String(Files.readAllBytes(Paths.get(fileName + ".txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        ParseTree tree = syntaxCheck(contents);
        if (tree != null) {
            CodeGenerator generator = new CodeGenerator();
            FileWriter myWriter = new FileWriter("src/codegeneration/Program.hs");
            myWriter.write(generator.generate(tree));
            myWriter.close();
        }
    }

    public static void generateProgram(String input) throws Exception, IOException {
        ParseTree tree = syntaxCheck(input);
        if (tree != null) {
            CodeGenerator generator = new CodeGenerator();
            FileWriter myWriter = new FileWriter("src/codegeneration/Program.hs");
            myWriter.write(generator.generate(tree));
            myWriter.close();
        }
    }

    public static ParseTree syntaxCheck(String text) throws Exception {
        try {
            CharStream stream = CharStreams.fromString(text);
            Lexer lexer = null;
            Constructor<? extends Lexer> lexerConstr = (PyVaLexer.class)
                    .getConstructor(CharStream.class);

            lexer = lexerConstr.newInstance(stream);
            lexer.removeErrorListeners();
            SyntaxErrorListener errorListener = new SyntaxErrorListener();
            lexer.addErrorListener(errorListener);

            TokenStream tokens = new CommonTokenStream(lexer);
            PyVaParser parser = new PyVaParser(tokens);

            parser.removeErrorListeners();
            parser.addErrorListener(errorListener);

            ParseTree tree = parser.goal();
            if (errorListener.getErrors().size() > 0) {
                throw new Exception(Exception.getMessage(errorListener.getErrors()));
            }
            return tree;
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
