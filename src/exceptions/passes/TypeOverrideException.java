package exceptions.passes;

public class TypeOverrideException extends RedefinitionException {
    public TypeOverrideException(){super();}
    public TypeOverrideException(String m){super(m);}
}
