package exceptions.passes;

public class VarRedefinitionException extends RedefinitionException {
    public VarRedefinitionException(){super();}
    public VarRedefinitionException(String m){super(m);}
}
