package exceptions.passes;

import exceptions.Exception;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import java.util.ArrayList;
import java.util.List;

public class SyntaxErrorListener extends BaseErrorListener {
    private final ArrayList<Exception> errors;

    public SyntaxErrorListener () {
        this.errors = new ArrayList<>();
    }

    public List<Exception> getErrors() {
        return this.errors;
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        String message = "(" + line + ":" + charPositionInLine + "): " + msg;
        errors.add(new Exception(message));
    }
}
