package exceptions.passes;

import exceptions.DefinitionException;

public class RedefinitionException extends DefinitionException {
    public RedefinitionException(){super();}
    public RedefinitionException(String m){super(m);}
}
