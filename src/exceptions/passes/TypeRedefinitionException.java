package exceptions.passes;

public class TypeRedefinitionException extends RedefinitionException {
    public TypeRedefinitionException(){super();}
    public TypeRedefinitionException(String m){super(m);}
}
