package exceptions.passes;

public class FieldRedefinitionException extends RedefinitionException {
    public FieldRedefinitionException(){super();}
    public FieldRedefinitionException(String m){super(m);}
}
