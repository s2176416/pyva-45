package exceptions;

import java.util.List;

public class Exception extends java.lang.Exception {
    public Exception() {
        super();
    }

    public Exception(String message) {
        super(message);
    }

    /**
     * Creates a nicely formatted string from multiple exceptions
     *
     * @param es The exceptions
     * @return Nicely formatted message
     */
    public static String getMessage(List<Exception> es) {
        StringBuilder string = new StringBuilder().append("\n");
        for (Exception e : es) {
            string.append(e.getMessage()).append("\n");
        }
        return string.toString();
    }
}
