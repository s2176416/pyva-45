package exceptions;

public class OutOfScopeException extends Exception {
    public OutOfScopeException(){super();}
    public OutOfScopeException(String m){super(m);}
}
