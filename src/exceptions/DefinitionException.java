package exceptions;

public class DefinitionException extends Exception {
    public DefinitionException(){super();}
    public DefinitionException(String m){super(m);}
}
