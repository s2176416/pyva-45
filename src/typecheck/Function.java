package typecheck;

import typecheck.typing.Type;

import java.util.ArrayList;

/**
 * Class that represents a function instance. The class represents a function object which includes
 * the name of the function, the return type of the function, and the parameters the function
 * requires. Each parameter has a type and a name.
 * This is to be used by {@link typecheck.scope.Scope} in the
 * PyVaDefListener pass (the first pass of the type checking phase)
 * and PyVaConcreteDefListener (the second pass) which uses the list resulted form
 * the first pass to change its properties (e.g. the return type) if necessary.
 */
public class Function {
    /**
     * The identifier of the function
     */
    public final String identifier;
    private Type returnType;
    private final ArrayList<Type> paramTypes;
    private final ArrayList<String> paramNames;

    /**
     * Creates a new functions instance with supplied identifier, return type, parameter types and their names.
     *
     * @param id         The identifier of the function
     * @param returnType The return type of the function
     * @param paramTypes The types of the supplied parameters
     * @param paramNames The names of the supplied parameters
     */
    public Function(String id, Type returnType, ArrayList<Type> paramTypes, ArrayList<String> paramNames) {
        this.identifier = id;
        this.returnType = returnType;
        if (paramTypes != null) {
            this.paramTypes = paramTypes;
        } else {
            this.paramTypes = new ArrayList<>();
        }
        if (paramNames != null) {
            this.paramNames = paramNames;
        } else {
            this.paramNames = new ArrayList<>();
        }
    }

    /**
     * Return the list of parameter types
     *
     * @return The list of parameter types
     */
    public ArrayList<Type> getParamTypes() {
        return this.paramTypes;
    }

    /**
     * Return the list of parameter names
     *
     * @return The list of parameter names
     */
    public ArrayList<String> getParamNames() {
        return paramNames;
    }

    /**
     * Sets the return type of this function
     *
     * @param type The new return type
     */
    public void setReturnType(Type type) {
        this.returnType = type;
    }

    /**
     * Returns the return type of this function
     *
     * @return The return type of the function
     */
    public Type getReturnType() {
        return returnType;
    }
}
