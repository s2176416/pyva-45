package typecheck.scope;

import java.util.Arrays;

/**
 * Class used by {@link Scope} to distinguish between scopes
 */
public class ScopeLevel {
    private int[] depths;

    /**
     * Creates a new scope instance with supplied depths
     *
     * @param depths The depth of this level
     */
    public ScopeLevel(int... depths) {
        this.depths = depths;
    }

    /**
     * Retrieve the depths of this scope level instance
     *
     * @return The depths
     */
    public int[] getDepths() {
        return depths;
    }

    /**
     * Test equality of two {@link ScopeLevel scope levels}
     *
     * @param other The object to test equality on
     * @return True if the contents of {@link ScopeLevel#depths} are the same
     */
    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof ScopeLevel) {
            ScopeLevel c = (ScopeLevel) other;
            if (c.depths.length != this.depths.length) {
                return false;
            }
            for (int i = 0; i < depths.length; i++) {
                if (depths[i] != c.depths[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Prints the scope levels's depths as a string
     *
     * @return String representation of this scope level
     */
    @Override
    public String toString() {
        return Arrays.toString(this.depths);
    }
}
