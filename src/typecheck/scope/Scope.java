package typecheck.scope;

import typecheck.typing.Type;

import java.util.*;

/**
 * Class to keep track of currently open Scope and closed Scopes. Once a scope is closed, it is permanently
 * closed and cannot be modified anymore. Accessing specific (possibly closed) Scopes and testing for the existence of certain identifiers,
 * requires a {@link ScopeLevel}.
 *
 * @param <T> The data type the scope may contain. For example {@link Type}
 */
public class Scope<T> {
    private int size = 4;
    private final ScopeLevel scopeLevel;
    private final Map<String, Data> data;
    private final List<Scope<T>> closedScopes;
    private Scope<T> currentScope;

    /**
     * Creates a new Scope instance with {@link ScopeLevel scopeLevel} 0
     */
    public Scope() {
        this.scopeLevel = new ScopeLevel(0);
        this.closedScopes = new LinkedList<>();
        this.data = new HashMap<>();
    }

    /**
     * Create a new Scope instance with supplied {@link ScopeLevel}
     *
     * @param c The scope level of this instance
     */
    private Scope(ScopeLevel c) {
        this.scopeLevel = c;
        this.closedScopes = new LinkedList<>();
        this.data = new HashMap<>();
    }

    /**
     * Recursively opens a new Scope in the currently open scope.
     */
    public void openScope() {
        if (currentScope == null) {
            currentScope = new Scope<>(new ScopeLevel(addDepth(closedScopes.size())));
        } else {
            currentScope.openScope();
        }
    }

    /**
     * Creates an array of integers, representing scope level.
     *
     * @param d The integer to append to the end of current scope level.
     * @return The scope level.
     */
    private int[] addDepth(int d) {
        int[] result = new int[scopeLevel.getDepths().length + 1];
        System.arraycopy(scopeLevel.getDepths(), 0, result, 0, scopeLevel.getDepths().length);
        result[result.length - 1] = d;
        return result;
    }

    /**
     * Recursively finds and closes the currently open scope. If all scopes but main scope are closed already
     * the program terminates. Closing a scope will add the scope to a closed scopes list, to make it accessible even
     * though its closed.
     */
    public void closeScope() {
        if (currentScope == null) {
            System.err.println("All scopes were closed already");
            System.exit(4);
        }
        if (currentScope.currentScope != null) {
            currentScope.closeScope();
        } else {
            closedScopes.add(currentScope);
            currentScope = null;
        }
    }

    /**
     * Recursively finds the current scope level
     *
     * @return The {@link ScopeLevel} of the most recently opened scope.
     */
    public ScopeLevel currentScopeLevel() {
        if (currentScope != null) {
            return currentScope.currentScopeLevel();
        } else {
            return this.scopeLevel;
        }
    }

    /**
     * Calls {@link Scope#put(String, Type, Object)} with null as data.
     *
     * @param id   The id to put
     * @param type The Type connected to the id
     * @return True or false, depending on whether the Scope already contained the supplied identifier
     */
    public boolean put(String id, Type type) {
        return put(id, type, null);
    }

    /**
     * Adds the given id to the currently open scope if possible.
     *
     * @param id   The id to add
     * @param type The Type connected to the id
     * @param data The data bound to the identifier
     * @return True or false, depending on whether the Scope already contained the supplied identifier
     */
    public boolean put(String id, Type type, T data) {
        if (currentScope == null) {
            if (!this.data.containsKey(id)) {
                this.data.put(id, new Data(type, scopeLevel, this.size, data));
                size += Type.SIZE;
                return true;
            }
            return false;
        } else {
            return currentScope.put(id, type, data);
        }
    }

    /**
     * Retrieves the data of the currently open scope
     *
     * @param id The id of the data to retrieve
     * @return The data {@link T}
     */
    public T data(String id) {
        Data d = getDataContainer(id);
        if (d != null) {
            return d.data;
        }
        return null;
    }

    /**
     * Retrieves the data of the Scope, depending on the supplied id and {@link ScopeLevel}
     *
     * @param id         The id of the data to retrieve
     * @param scopeLevel The scope level at which to start looking from (enclosing scopes may contain this id as well)
     * @return The data {@link T}
     */
    public T data(String id, ScopeLevel scopeLevel) {
        Data d = getDataContainer(id, scopeLevel);
        if (d != null) {
            return d.data;
        }
        return null;
    }

    /**
     * Tests whether an identifier exists in the current open scope or its enclosing scopes
     *
     * @param id The identifier to test existence of
     * @return True when the identifier exists in the scope
     */
    public boolean contains(String id) {
        boolean res = data.containsKey(id);
        if (currentScope != null) {
            return res || currentScope.contains(id);
        }
        return res;
    }

    /**
     * Tests whether an identifier exists in the given scope level or its enclosing scopes
     *
     * @param id         The identifier to test existence of
     * @param scopeLevel The scope level to start from
     * @return True when the identifier exists in the scope
     */
    public boolean containsWithinScope(String id, ScopeLevel scopeLevel) {
        if (data.containsKey(id)) {
            return true;
        }
        if (this.scopeLevel.getDepths().length > scopeLevel.getDepths().length) {
            return false;
        } else if (this.scopeLevel.getDepths().length == scopeLevel.getDepths().length) {
            if (this.scopeLevel.equals(scopeLevel)) {
                return data.containsKey(id);
            } else {
                return false;
            }
        } else {
            int nextDepth = scopeLevel.getDepths()[this.scopeLevel.getDepths().length];
            if (nextDepth < closedScopes.size()) {
                return closedScopes.get(nextDepth).containsWithinScope(id, scopeLevel);
            } else if (nextDepth == closedScopes.size() && currentScope != null) {
                return currentScope.containsWithinScope(id, scopeLevel);
            }
        }
        return false;
    }

    /**
     * Returns the type bound to an identifier, starting at the most recent opened scope
     *
     * @param id The identifier to retrieve the type from
     * @return The type bound to the identifier
     */
    public Type type(String id) {
        Data d = getDataContainer(id);
        if (d != null) {
            return d.type;
        }
        return null;
    }

    /**
     * Returns the type bound to an identifier, starting at the supplied scope level
     *
     * @param id         The identifier to retrieve the type from
     * @param scopeLevel The scope level to start looking from
     * @return The type bound to the identifier
     */
    public Type type(String id, ScopeLevel scopeLevel) {
        Data d = getDataContainer(id, scopeLevel);
        if (d != null) {
            return d.type;
        }
        return null;
    }

    /**
     * Returns the {@link ScopeLevel} of where an identifier resides in, starting at the most recently opened scope.
     *
     * @param id The identifier to retrieve the scope level from
     * @return The scope level bound to the identifier
     */
    public ScopeLevel scopeLevel(String id) {
        Data d = getDataContainer(id);
        if (d != null) {
            return d.scopeLevel;
        }
        return null;
    }

    /**
     * Returns the {@link ScopeLevel} of where an identifier resides in, starting at the supplied scope level.
     *
     * @param id         The identifier to retrieve the scope level from
     * @param scopeLevel The scope level to start looking from
     * @return The scope level bound to an identifier
     */
    public ScopeLevel scopeLevel(String id, ScopeLevel scopeLevel) {
        Data d = getDataContainer(id, scopeLevel);
        if (d != null) {
            return d.scopeLevel;
        }
        return null;
    }

    /**
     * Returns the offset of an identifier, starting at the most recently opened scope. Note that this offset
     * may also be an offset of the enclosing scope, the offsets do not accumulate.
     *
     * @param id The identifier to retrieve the offset from
     * @return The offset bound to an identifier
     */
    public Integer offset(String id) {
        Data d = getDataContainer(id);
        if (d != null) {
            return d.offset;
        }
        return null;
    }

    /**
     * Returns the offset of an identifier, starting at supplied scope level. Note that this offset
     * may also be an offset of the enclosing scope, these offsets do not accumulate.
     *
     * @param id         The identifier to retrieve the offset from
     * @param scopeLevel The scope level to start looking from
     * @return The offset bound to an identifier
     */
    public Integer offset(String id, ScopeLevel scopeLevel) {
        Data d = getDataContainer(id, scopeLevel);
        if (d != null) {
            return d.offset;
        }
        return null;
    }

    /**
     * Return the current local data area size of the most recently opened scope.
     *
     * @return The current size of the most recently opened scope in bytes ({@link Type#SIZE}).
     */
    public int localDataAreaSize() {
        if (this.currentScope != null) {
            return this.currentScope.localDataAreaSize();
        }
        return this.data.size() * Type.SIZE;
    }

    /**
     * Return the current local data area size of the scope with the supplied scope level
     *
     * @param scopeLevel The scope level of the scope to retrieve the size from
     * @return The current size of the supplied scope in bytes ({@link Type#SIZE}).
     */
    public int localDataAreaSize(ScopeLevel scopeLevel) {
        if (this.scopeLevel.getDepths().length >= scopeLevel.getDepths().length) {
            return data.size() * Type.SIZE;
        } else {
            int nextDepth = scopeLevel.getDepths()[this.scopeLevel.getDepths().length];
            if (nextDepth < closedScopes.size()) {
                return closedScopes.get(nextDepth).localDataAreaSize(scopeLevel);
            } else if (nextDepth == closedScopes.size() && currentScope != null) {
                return currentScope.localDataAreaSize(scopeLevel);
            }
        }
        return data.size() * Type.SIZE;
    }

    /**
     * Retrieves the data container based off id and scope level
     *
     * @param id         The id to retrieve the container from
     * @param scopeLevel The scope level to start looking from
     * @return The data container bound to the identifier
     */
    private Data getDataContainer(String id, ScopeLevel scopeLevel) {
        Data result = getDataContainer(id);
        if (this.scopeLevel.getDepths().length >= scopeLevel.getDepths().length) {
            return result;
        } else {
            int nextDepth = scopeLevel.getDepths()[this.scopeLevel.getDepths().length];
            Data deeperResult;
            if (nextDepth < closedScopes.size()) {
                deeperResult = closedScopes.get(nextDepth).getDataContainer(id, scopeLevel);
                if (deeperResult != null) {
                    return deeperResult;
                }
            } else if (nextDepth == closedScopes.size() && currentScope != null) {
                deeperResult = currentScope.getDataContainer(id, scopeLevel);
                if (deeperResult != null) {
                    return deeperResult;
                }
            }
        }
        return result;
    }

    /**
     * Retrieves the data container based off id and most recently opened scope
     *
     * @param id The id to retrieve the container from
     * @return The data container bound to the identifier
     */
    private Data getDataContainer(String id) {
        Data result = data.get(id);
        if (currentScope != null) {
            Data r2 = currentScope.getDataContainer(id);
            if (r2 != null) {
                return r2;
            }
        }
        return result;
    }

    /**
     * Creates a list of opened and closed scopes in the order in which they are opened.
     *
     * @return The list of {@link ScopeLevel scope levels}
     */
    public List<ScopeLevel> getScopeLevels() {
        List<ScopeLevel> scopeLevels = new LinkedList<>();
        scopeLevels.add(this.scopeLevel);
        for (Scope<T> s : closedScopes) {
            scopeLevels.addAll(s.getScopeLevels());
        }
        if (this.currentScope != null) {
            scopeLevels.addAll(this.currentScope.getScopeLevels());
        }
        return scopeLevels;
    }

    /**
     * Retrieves a {@link Set} of identifiers of the supplied scope level, note that this set does not include enclosing scope's identifiers
     *
     * @param scopeLevel The scope level of which to retrieve the identifiers from
     * @return The set of identifiers
     */
    public Set<String> keySet(ScopeLevel scopeLevel) {
        if (this.scopeLevel.equals(scopeLevel)) {
            return this.data.keySet();
        }
        if (this.scopeLevel.getDepths().length >= scopeLevel.getDepths().length) {
            return this.data.keySet();
        } else {
            int nextDepth = scopeLevel.getDepths()[this.scopeLevel.getDepths().length];
            if (nextDepth < closedScopes.size()) {
                return closedScopes.get(nextDepth).keySet(scopeLevel);
            } else if (nextDepth == closedScopes.size() && currentScope != null) {
                return currentScope.keySet(scopeLevel);
            }
        }
        return this.data.keySet();
    }

    /**
     * Private class that contains {@link Type}, offset, {@link ScopeLevel} and {@link T data}
     */
    private class Data {
        private final Type type;
        private final Integer offset;
        private final ScopeLevel scopeLevel;
        private final T data;

        private Data(Type type, ScopeLevel scopeLevel, Integer offset, T data) {
            this.type = type;
            this.scopeLevel = scopeLevel;
            this.offset = offset;
            this.data = data;
        }
    }
}
