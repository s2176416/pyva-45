package typecheck;

/**
 * Thread class that contains the id of the thread
 */
public class PyVaThread {
    private final String identifier;

    /**
     * Creates a new Thread instance with a given identifier
     *
     * @param identifier the name of the thread.
     */
    public PyVaThread(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Returns the identifier of the thread.
     *
     * @return The identifier of the thread.
     */
    public String getIdentifier() {
        return this.identifier;
    }
}
