package typecheck;

import exceptions.Exception;
import pyva.PyVaBaseListener;
import pyva.PyVaParser;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import typecheck.scope.Scope;
import typecheck.scope.ScopeLevel;
import typecheck.typing.PyVaConcreteDefListener;
import typecheck.typing.Type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import static typecheck.typing.Type.*;

/**
 * Class used to do type checking and scope checking.
 * Type checking ensures that variables only accept expressions which is accepted by the type of the variable.
 * Scope checking ensures that types and functions may only be used or called whenever it exists in the current scope.
 * This class depends heavily on {@link PyVaConcreteDefListener} and may throw exceptions when {@link PyVaConcreteDefListener} does.
 * Whenever a type check or scope check fails, an error message will be added and an {@link Exception} will be thrown.
 *
 * @see PyVaConcreteDefListener
 * @see Type
 * @see Function
 * @see PyVaThread
 * @see Scope
 * @see Exception
 */
public class PyVaTypeScopeCheck extends PyVaBaseListener {
    private Scope varScope;
    private Scope<Function> funScope;
    private Scope<Type> typeScope;
    private ParseTreeProperty<Type> exprTypes;
    private ArrayList<ParseTree> containReturns;
    private Stack<Type> functionTypes;
    private Scope<PyVaThread> threadScope;
    private int inThreads;
    private HashMap<String, HashMap<String, Integer>> sharedVars;
    private HashMap<String, HashMap<String, Integer>> childThreads;
    private int totalChildThreads;

    private ArrayList<Exception> errors;

    public void check(ParseTree tree) throws Exception {
        PyVaConcreteDefListener functionLister = new PyVaConcreteDefListener();
        functionLister.listDefinitions(tree);
        this.varScope = new Scope();
        this.funScope = functionLister.getFunScope();
        this.typeScope = functionLister.getTypeScope();
        this.exprTypes = new ParseTreeProperty<>();
        this.containReturns = new ArrayList<>();
        this.functionTypes = new Stack<>();
        this.threadScope = new Scope<>();
        this.inThreads = 0;
        this.errors = new ArrayList<>();
        this.sharedVars = new HashMap<>();
        this.childThreads = new HashMap<>();
        this.totalChildThreads = 0;
        new ParseTreeWalker().walk(this, tree);
        if (this.errors.size() > 0) {
            throw new Exception(Exception.getMessage(errors));
        }
    }

    /**
     * Simply checks the entirety of the program (list of declarations) whether or not
     * any of them are return declarations or contain return declarations in their scopes.
     * If any of them have, then this program also has a return declaration and ctx
     * is added to the list of contexts that have return declarations.
     *
     * @param ctx The {@link PyVaParser.ProgramContext} that is exited from
     */
    @Override
    public void exitProgram(PyVaParser.ProgramContext ctx) {
        for (ParseTree tree : ctx.decl()) {
            if (containReturns.contains(tree)) {
                containReturns.add(ctx);
            }
        }
    }

    /**
     * Opens the scopes of the Scope fields available in this class. The parameters required
     * for this the function being entered is now considered a new variable in the new scope
     * of the function being entered and is thus added to the scope. The return type of the function
     * is also added to the stack so as to check the equality of types with the return declaration
     * later inside the function body.
     *
     * @param ctx The {@link PyVaParser.FuncdeclContext} being entered to.
     */
    @Override
    public void enterFuncdecl(PyVaParser.FuncdeclContext ctx) {
        Function f = funScope.data(ctx.ID().getText(), varScope.currentScopeLevel());
        openScopes();
        for (int i = 0; i < f.getParamNames().size(); i++) {
            addVarToScope(f.getParamNames().get(i), f.getParamTypes().get(i));
        }
        functionTypes.push(f.getReturnType());
    }

    /**
     * Simply opens a new scope for all scopes in this class from the new record declaration.
     *
     * @param ctx The {@link PyVaParser.RecorddeclContext} being entered to.
     */
    @Override
    public void enterRecorddecl(PyVaParser.RecorddeclContext ctx) {
        openScopes();
    }

    /**
     * Simply closes a new scope for all scopes in this class from the new record declaration.
     *
     * @param ctx The {@link PyVaParser.RecorddeclContext} exited from.
     */
    @Override
    public void exitRecorddecl(PyVaParser.RecorddeclContext ctx) {
        closeScopes();
    }

    /**
     * Pops the functions return type from the functionTypes stack.
     * Also checks the program (list of declarations, a.k.a the body of the program)
     * whether or not it contains a return declaration if the function type is not void.
     * If the function's type is not void and no return declaration exists in the function's body,
     * an error will be added to the list of errors.
     *
     * @param ctx The {@link PyVaParser.FuncdeclContext} exited from.
     */
    public void exitFuncdecl(PyVaParser.FuncdeclContext ctx) {
        closeScopes();
        Type type = functionTypes.pop();
        if (!type.equals(VOID) && !containReturns.contains(ctx.program())) {
            addError(ctx.start, String.format("Return statement missing for some path in the function '%s'", ctx.ID().getText()));
        }
    }

    /**
     * Checks the expression type of the expression in the condition part of the if statement.
     * If it is not a boolean expression, an error is added to the list of errors.
     * Also, if the if statement has an else statement also and both blocks have return declarations,
     * then this if else declaration has a return declaration. If the if statement does not have an else
     * statement, even though a return statement exists in the block, the if statement is not considered to have
     * a return declaration since there is a possibility that the if block isn't entered in runtime.
     *
     * @param ctx The {@link PyVaParser.IfdeclContext} that is exited from
     */
    @Override
    public void exitIfdecl(PyVaParser.IfdeclContext ctx) {
        if (!exprType(ctx.expr()).accepts(BOOL)) {
            addError(ctx.expr().start, "Cannot parse '" + ctx.expr().getText() + "' as a boolean expression.");
        }

        if (ctx.block().size() == 2 && containReturns.contains(ctx.block(0)) && containReturns.contains(ctx.block(1))) {
            containReturns.add(ctx);
        }
    }

    /**
     * Checks the expression type of the given condition. If the expression is not a boolean expression,
     * an error is added to the list of errors. Also, even if a return declaration exists in the while block,
     * there is a possibility that the while block is not visited at all later at runtime. So regardless of whether
     * or not the while block contains a return declaration, the while block will not be considered as a declaration
     * that contains a return declaration.
     *
     * @param ctx The {@link PyVaParser.WhiledeclContext} that is exited from
     */
    @Override
    public void exitWhiledecl(PyVaParser.WhiledeclContext ctx) {
        if (!exprType(ctx.expr()).accepts(BOOL)) {
            addError(ctx.expr().start, "Cannot parse '" + ctx.expr().getText() + "' as a boolean expression.");
        }
    }

    /**
     * Simply opens the scopes that are in this class for the newly created for loop.
     *
     * @param ctx The {@link PyVaParser.FordeclContext} entered to.
     */
    @Override
    public void enterFordecl(PyVaParser.FordeclContext ctx) {
        openScopes();
    }

    /**
     * Checks the middle for block expression whether or not it is a boolean expression.
     * If it isn't a boolean expression, an error will be added to the list of errors.
     * Also, regardles of whether or not a return declaration exists in the for block,
     * the for block will always be considered to not have a return declaration since
     * there is always the possibility that the for block is not visited at all in runtime.
     *
     * @param ctx The {@link PyVaParser.FordeclContext} being exited from.
     */
    @Override
    public void exitFordecl(PyVaParser.FordeclContext ctx) {
        if (!exprType(ctx.expr()).accepts(BOOL)) {
            addError(ctx.expr().start, "Cannot parse '" + ctx.expr().getText() + "' as a boolean expression.");
        }
        closeScopes();
    }

    /**
     * Checks the validity of the return declaration.
     * If the return declaration is done outside of a function, or
     * if function that's closest in enclosing the return declaration is a void function, or
     * if the type of the expression in the return declaration does not match the function's type
     * an error will be added to the list of errors. Otherwise, ctx is considered to contain a return declaration (obviously)
     *
     * @param ctx The {@link PyVaParser.ReturndeclContext} exited from.
     */
    @Override
    public void exitReturndecl(PyVaParser.ReturndeclContext ctx) {
        if (functionTypes.empty()) {
            addError(ctx.start, "Cannot return on a non-function scope!");
        } else if (functionTypes.peek().equals(VOID)) {
            addError(ctx.start, "Void functions should not have return statements!");
        } else if (functionTypes.peek().accepts(exprType(ctx.expr()))) {
            containReturns.add(ctx);
        } else {
            addError(ctx.expr().start, "Return identifier '" + exprType(ctx.expr()) + "' does not match function identifier  '" + functionTypes.peek() + "'");
        }
    }

    /**
     * Simply opens the scopes that exist in this class.
     *
     * @param ctx The {@link PyVaParser.BlockContext} entered to.
     */
    @Override
    public void enterBlock(PyVaParser.BlockContext ctx) {
        openScopes();
    }

    /**
     * Simply closes the scopes that exist in this class.
     * Also if a return declaration exists in the program, it is passed
     * onto this BlockContext
     *
     * @param ctx The {@link PyVaParser.BlockContext} being exited from.
     */
    @Override
    public void exitBlock(PyVaParser.BlockContext ctx) {
        closeScopes();
        if (containReturns.contains(ctx.program())) {
            containReturns.add(ctx);
        }
    }

    /**
     * Checks whether or not all the variables which is to be reassigned have the same type.
     * Also checks whether or not the expression has the same type as the variables it is being assigned to.
     *
     * @param ctx The {@link PyVaParser.TypelessvardeclContext} being exited from.
     */
    @Override
    public void exitTypelessvardecl(PyVaParser.TypelessvardeclContext ctx) {
        Type type = exprType(ctx.idCall(0));
        for (int i = 1; i < ctx.idCall().size(); i++) {
            if (!type.accepts(exprType(ctx.idCall(i)))) {
                addError(ctx.idCall(i).start, String.format("Expected type '%s' but variable '%s' has type '%s'", type, ctx.idCall(i), exprType(ctx.idCall(i))));
            }
        }
        if (!exprType(ctx.expr()).accepts(type)) {
            addError(ctx.expr().start, String.format("Expected type %s but expression '%s' has type '%s'", type, ctx.expr().getText(), exprType(ctx.expr())));
        }
    }

    /**
     * If a variable already exits in the current scope with the given name, or
     * if it is currently inside a thread block and the number of shared variables have been met, or
     * if the type given as the type of the variables does not exist, or
     * if the type of the expression does not match the type given to the variables,
     * an error with the corresponding error messages will be added to the list of errors.
     * Otherwise, the variables are added to the scope along with their types
     *
     * @param ctx The {@link PyVaParser.TypevardeclContext} being exited from.
     */
    @Override
    public void exitTypevardecl(PyVaParser.TypevardeclContext ctx) {
        Type expected = Type.tryType(ctx.identifier().getText(), typeScope, varScope.currentScopeLevel());
        if (expected == NULL) {
            addError(ctx.identifier().start, "Type '" + ctx.identifier().getText() + "' does not exist");
        } else {
            for (int i = 0; i < ctx.ID().size(); i++) {
                if (!addVarToScope(ctx.ID(i).getText(), expected)) {
                    addError(ctx.ID(i).getSymbol(), "Cannot redeclare variable '" + ctx.ID(i).getText() + "' within the same scope");
                } else if (inThreads > 0 && !addSharedVar(ctx.ID(i).getText())) {
                    addError(ctx.ID(i).getSymbol(), "More than the maximum number of shared variables (4) have been created!");
                }
            }
            Type actual = exprType(ctx.expr());
            if (!expected.accepts(actual)) {
                addError(ctx.expr().start, String.format("Expected type %s but expression '%s' has type '%s'", expected, ctx.expr().getText(), actual));
            }
        }
    }

    /**
     * Sets the expression type of ctx to the ParseTreeProperty exprTypes.
     *
     * @param ctx The {@link PyVaParser.ArrayInitExprContext} being exited from.
     */
    @Override
    public void exitArrayInitExpr(PyVaParser.ArrayInitExprContext ctx) {
        setExprType(ctx, exprType(ctx.arrayInit()));
    }

    /**
     * Sets the expression type of ctx to the ParseTreeProperty exprTypes
     * if the arrayValue exists. Sets it to ANY otherwise.
     *
     * @param ctx The {@link PyVaParser.ValuedArrayInitContext} being exited from.
     */
    @Override
    public void exitValuedArrayInit(PyVaParser.ValuedArrayInitContext ctx) {
        if (ctx.arrayValue() != null) {
            setExprType(ctx, exprType(ctx.arrayValue()));
        } else {
            setExprType(ctx, ANY.toArray(1));
        }
    }

    /**
     * If the base type of the array cannot be identified, or
     * if the sized array initialization is given expressions that are not integer expressions,
     * then errors with the corresponding messages will be added to the list of errors.
     *
     * @param ctx The {@link PyVaParser.SizedArrayInitContext} being exited from.
     */
    @Override
    public void exitSizedArrayInit(PyVaParser.SizedArrayInitContext ctx) {
        Type baseType = Type.tryType(ctx.identifier().getText(), typeScope, varScope.currentScopeLevel());
        if (baseType == NULL) {
            addError(ctx.identifier().start, "Type '" + ctx.identifier().getText() + "' does not exist");
            setExprType(ctx, ANY);
        } else {
            setExprType(ctx, baseType.toArray(ctx.OBRACK().size()));
            for (PyVaParser.ExprContext expr : ctx.expr()) {
                Type actual = exprType(expr);
                if (!INT.accepts(actual)) {
                    addError(expr.start, String.format("Array sizes must be of type '%s' but was '%s' in the expression '%s'", INT, actual, expr.getText()));
                }
            }
        }
    }

    /**
     * If a value in the array initialization is different from other values,
     * then an error is added to the list of errors. Will set the expression type to
     * the ParseTreeProperty exprTypes as well.
     *
     * @param ctx The {@link PyVaParser.ArrayValueContext} being exited from.
     */
    @Override
    public void exitArrayValue(PyVaParser.ArrayValueContext ctx) {
        Type expect = ANY;
        String expression = ANY.getIdentifier();
        boolean error = false;
        for (PyVaParser.ExprContext expr : ctx.expr()) {
            Type actual = exprType(expr);
            if (expect == ANY && !actual.getIdentifier().equals(expect.getIdentifier())) {
                expect = actual;
                expression = expr.getText();
            }
            if (!expect.accepts(actual)) {
                error = true;
                addError(expr.start, String.format("Array values should all share the same types but Type '%s' in the expression '%s' differs from type '%s' in the expression '%s'", actual, expr.getText(), expect, expression));
            }
        }
        if (error) {
            setExprType(ctx, ANY.toArray(expect.getDepth() + 1));
        } else {
            setExprType(ctx, expect.toArray(expect.getDepth() + 1));
        }
    }

    /**
     * If the type of the expression does not match the type of the operator,
     * then an error message will be added to the list of errors.
     * The ! operator expects a boolean expression following it.
     * The - operator expects an integer expression following it.
     *
     * @param ctx The {@link PyVaParser.PrfExprContext} being exited from.
     */
    @Override
    public void exitPrfExpr(PyVaParser.PrfExprContext ctx) {
        Type actual = exprType(ctx.expr());
        Type expected = exprType(ctx.prfOp());
        setExprType(ctx, expected);
        if (actual != expected) {
            addError(ctx.expr().start, String.format("Expected '%1$s %2$s' but was '%1$s %3$s'", ctx.prfOp(), expected, actual));
        }
    }

    /**
     * Sets the type of the operator to integer.
     *
     * @param ctx The {@link PyVaParser.MinusPrfOpContext} being exited from.
     */
    @Override
    public void exitMinusPrfOp(PyVaParser.MinusPrfOpContext ctx) {
        setExprType(ctx, INT);
    }

    /**
     * Sets the type of the operator to boolean.
     *
     * @param ctx The {@link PyVaParser.NotPrfOpContext} being exited from.
     */
    @Override
    public void exitNotPrfOp(PyVaParser.NotPrfOpContext ctx) {
        setExprType(ctx, BOOL);
    }

    /**
     * Checks whether or not both expressions in this expression are integers or not.
     * If at least one of them is not an integer, an error message will be added to the list of errors.
     * Sets the type of the expression to integer as well to the ParseTreeProperty exprTypes.
     *
     * @param ctx The {@link PyVaParser.MultExprContext} being exited from.
     */
    @Override
    public void exitMultExpr(PyVaParser.MultExprContext ctx) {
        setExprType(ctx, INT);
        Type t1 = exprType(ctx.expr(0));
        Type t2 = exprType(ctx.expr(1));
        if (!t1.accepts(INT) || !t2.accepts(INT)) {
            addError(ctx.expr(0).start, String.format("Expected '%s * %s' but was '%s * %s'", INT, INT, t1, t2));
        }
    }

    /**
     * Checks whether or not both expressions in this expression are integers or not.
     * If at least one of them is not an integer, an error message will be added to the list of errors.
     * Sets the type of the expression to integer as well to the ParseTreeProperty exprTypes.
     *
     * @param ctx The {@link PyVaParser.PlusExprContext} being exited from.
     */
    @Override
    public void exitPlusExpr(PyVaParser.PlusExprContext ctx) {
        setExprType(ctx, INT);
        Type t1 = exprType(ctx.expr(0));
        Type t2 = exprType(ctx.expr(1));
        if (!INT.accepts(t1) || !INT.accepts(t2)) {
            addError(ctx.expr(0).start, String.format("Expected '%s + %s' but was '%s + %s'", INT, INT, t1, t2));
        }
    }

    /**
     * Checks whether or not both the given expressions are expressions of the same type
     * and are expressions of one of the primitive types. If one of the conditions are not met,
     * an error will be added to the list of errors.
     * Also sets the expression type of this expression to boolean.
     *
     * @param ctx The {@link PyVaParser.CompExprContext} being exited from.
     */
    @Override
    public void exitCompExpr(PyVaParser.CompExprContext ctx) {
        setExprType(ctx, BOOL);
        Type t1 = exprType(ctx.expr(0));
        Type t2 = exprType(ctx.expr(1));
        if (!t1.accepts(t2)) {
            addError(ctx.expr(0).start, String.format("Equality between the type '%s' and '%s' is always false", t1, t2));
        }
    }

    /**
     * If at least one of the two expressions are not boolean,
     * an error message will be added to the list of errors.
     * Also sets the expression type of this expression to boolean.
     *
     * @param ctx The {@link PyVaParser.BoolExprContext} being exited from.
     */
    @Override
    public void exitBoolExpr(PyVaParser.BoolExprContext ctx) {
        setExprType(ctx, BOOL);
        Type t1 = exprType(ctx.expr(0));
        Type t2 = exprType(ctx.expr(1));
        if (!BOOL.accepts(t1) || !BOOL.accepts(t2)) {
            addError(ctx.expr(0).start, String.format("Expected '%1$s %3$s %2$s' but was '%4$s %3$s %5$s'", BOOL, BOOL, ctx.boolOp().getText(), t1, t2));
        }
    }

    /**
     * Sets the type of this expression to the expression inside between the parentheses.
     *
     * @param ctx The {@link PyVaParser.ParExprContext} being exited from.
     */
    @Override
    public void exitParExpr(PyVaParser.ParExprContext ctx) {
        setExprType(ctx, exprType(ctx.expr()));
    }

    /**
     * Sets the type of this expression to the expression of the function calls expression type.
     *
     * @param ctx The {@link PyVaParser.FuncallExprContext} being exited from.
     */
    @Override
    public void exitFuncallExpr(PyVaParser.FuncallExprContext ctx) {
        setExprType(ctx, exprType(ctx.funcall()));
    }

    /**
     * Sets the type of this expression to character.
     *
     * @param ctx The {@link PyVaParser.CharExprContext} being exited from.
     */
    @Override
    public void exitCharExpr(PyVaParser.CharExprContext ctx) {
        setExprType(ctx, CHAR);
    }

    /**
     * Sets the type of this expression to null.
     *
     * @param ctx The {@link PyVaParser.NullExprContext} being exited from.
     */
    @Override
    public void exitNullExpr(PyVaParser.NullExprContext ctx) {
        setExprType(ctx, NULL);
    }

    /**
     * Sets the type of this expresstion to the expression of the id.
     *
     * @param ctx The {@link PyVaParser.IdExprContext} being exited from.
     */
    @Override
    public void exitIdExpr(PyVaParser.IdExprContext ctx) {
        setExprType(ctx, exprType(ctx.idCall()));
    }

    /**
     * Sets the type of this expression to integer.
     *
     * @param ctx The {@link PyVaParser.NumExprContext} being exited from.
     */
    @Override
    public void exitNumExpr(PyVaParser.NumExprContext ctx) {
        setExprType(ctx, INT);
    }

    /**
     * Sets the type of this expression to a character array (String)/
     *
     * @param ctx The {@link PyVaParser.StringExprContext} being exited from.
     */
    @Override
    public void exitStringExpr(PyVaParser.StringExprContext ctx) {
        setExprType(ctx, CHAR.toArray(1)); // a string equals the type 'char[]'
    }

    /**
     * If the new record instance that is being initialized does not correspond to
     * a defined record definition, an error is added to the list of errors.
     * Also, sets the type of this expression to the type of the record.
     *
     * @param ctx The {@link PyVaParser.RecordExprContext} being entered to.
     */
    @Override
    public void enterRecordExpr(PyVaParser.RecordExprContext ctx) {
        Type type = Type.tryType(ctx.ID().getText(), typeScope, varScope.currentScopeLevel());
        if (type == NULL) {
            addError(ctx.ID().getSymbol(), "Type '" + ctx.ID().getText() + "' does not exist in this scope!");
        }
        setExprType(ctx, type);
    }

    /**
     * For every accessser instance in the idCall (see ANTLR grammar PyVa.g4),
     * A new type is calculated. Once all accessers have been gone through,
     * then the final type is set to the expression type of this idCall.
     * If the id refers to an object that is a record object, and that record type does not have a field that is in an accesser, or
     * if an index expression to access an array is not of type integer, or
     * if an index expression accesser is done to a non-array variable,
     * then errors with the corresponding messages are added to the list of errors.
     *
     * @param ctx The {@link PyVaParser.IdCallContext} being exited from.
     */
    @Override
    public void exitIdCall(PyVaParser.IdCallContext ctx) {
        Type type = varScope.type(ctx.ID().getText());
        if (ctx.accesser() != null) {
            for (PyVaParser.AccesserContext access : ctx.accesser()) {
                if (access.DOT() != null) {
                    Type varType = type.getVariable(access.ID().getText());
                    if (varType == NULL) {
                        addError(access.ID().getSymbol(), String.format("Type '%s' does not have a reference to field with identifier '%s'", type, access.ID().getText()));
                        break;
                    } else {
                        type = varType;
                    }
                } else {
                    for (PyVaParser.ExprContext expr : access.expr()) {
                        if (!exprType(expr).accepts(INT)) {
                            addError(expr.start, String.format("Indices must be of type %s but was %s", INT, exprType(expr)));
                        }
                        if (!type.isArray()) {
                            addError(ctx.ID().getSymbol(), String.format("Cannot take index from non array type '%s'", type));
                            setExprType(ctx, type);
                            return;
                        }
                        type = type.innerType;
                    }
                }
            }
        }
        if (inThreads > 0 && !addSharedVar(ctx.ID().getText())) {
            addError(ctx.ID().getSymbol(), "More than the maximum number of shared variables (4) have been created!");
        }
        setExprType(ctx, type);
    }

    /**
     * If no functions is visible from this scope that has the name of the function being called, or
     * if the number of given parameters do not match the number of required parameters, or
     * if the type of the given parameters do not match the type of the required parameters,
     * then errors with the corresponding error messages will be added to the list of errors.
     * Also, sets the type of this expression to the type of the function being called.
     *
     * @param ctx The {@link PyVaParser.FuncallContext} being exited from.
     */
    @Override
    public void exitFuncall(PyVaParser.FuncallContext ctx) {
        Function function = getFunction(ctx.ID().getText());
        if (function == null) {
            addError(ctx.ID().getSymbol(), "Function with the name '" + ctx.ID().getText() + "' does not exist or is not visible from current scope!");
        } else if (function.getParamTypes().size() != ctx.expr().size()) {
            String requiredParams = "";
            if (function.getParamTypes().size() > 0) {
                requiredParams += function.getParamTypes().get(0);
                for (int i = 1; i < function.getParamTypes().size(); i++) {
                    requiredParams += ", " + function.getParamTypes().get(i);
                }
            }
            String givenParams = "";
            if (ctx.expr().size() > 0) {
                givenParams += exprType(ctx.expr(0));
                for (int i = 1; i < ctx.expr().size(); i++) {
                    givenParams += ", " + exprType(ctx.expr(i));
                }
            }
            addError(ctx.expr(0).start, "Couldn't match expected types '" + requiredParams + "' with given types '" + givenParams + "'");
        } else {
            for (int i = 0; i < function.getParamTypes().size(); i++) {
                if (!function.getParamTypes().get(i).accepts(exprType(ctx.expr(i)))) {
                    addError(ctx.expr(i).start, String.format("Expected parameter type '%s' but expresion '%s' has type '%s'", function.getParamTypes().get(i), ctx.expr(i).getText(), exprType(ctx.expr(i))));
//                    addError(ctx.expr(i).start, "Expected identifier '" + function.getParamTypes().get(i) + "' does not match given identifier '" + exprType(ctx.expr(i)) + "'");
                }
            }
        }
        if (function == null) {
            setExprType(ctx, ANY);
        } else {
            setExprType(ctx, function.getReturnType());
        }
    }

    /**
     * If the maximum number of threads of the program has been reached, or
     * if the thread name already exists in the scope,
     * then errors with the corresponding error messages will be added to the list of errors.
     * Also increments the inThreads thread nest counter.
     *
     * @param ctx The {@link PyVaParser.ThreaddeclContext} being entered to.
     */
    @Override
    public void enterThreaddecl(PyVaParser.ThreaddeclContext ctx) {
        inThreads++;
        if (!threadScope.put(ctx.ID().getText(), THREAD)) {
            addError(ctx.ID().getSymbol(), "Thread with name '" + ctx.ID().getText() + "' already exists in scope!");
        }
        if (!addChildThread(ctx.ID().getText(), varScope.currentScopeLevel())) {
            addError(ctx.ID().getSymbol(), "Cannot create more than three child threads!");
        }
    }

    /**
     * Decrements the inThreads thread next counter.
     *
     * @param ctx The {@link PyVaParser.ThreaddeclContext} being exited from.
     */
    @Override
    public void exitThreaddecl(PyVaParser.ThreaddeclContext ctx) {
        inThreads--;
    }

    /**
     * Adds an error message to the list of error messages if
     * the thread that is to be joined does not exist in this scope
     * or is not visible from this scope.
     *
     * @param ctx The {@link PyVaParser.JoindeclContext} being exited from.
     */
    @Override
    public void enterJoindecl(PyVaParser.JoindeclContext ctx) {
        if (!threadScope.contains(ctx.ID().getText())) {
            addError(ctx.ID().getSymbol(), "The thread to be joined with the name '" + ctx.ID().getText() + "' does not exist in this scope.");
        }
    }

    /**
     * If the to be printed expression is neither an integer, boolean nor character,
     * an error message is added to the list of errors.
     *
     * @param ctx The {@link PyVaParser.PrintdeclContext} being exited from.
     */
    @Override
    public void exitPrintdecl(PyVaParser.PrintdeclContext ctx) {
        if (!exprType(ctx.expr()).equals(INT) && !exprType(ctx.expr()).equals(CHAR) && !exprType(ctx.expr()).equals(BOOL)) {
            addError(ctx.expr().getStart(), "The expression type '" + exprType(ctx.expr()).toString() +
                    "' for the print statement is unacceptable!");
        }
    }

    /**
     * Set this expression's type to boolean.
     *
     * @param ctx The {@link PyVaParser.TrueExprContext} being exited from.
     */
    @Override
    public void exitTrueExpr(PyVaParser.TrueExprContext ctx) {
        setExprType(ctx, BOOL);
    }

    /**
     * Set this expression's type to boolean.
     *
     * @param ctx The {@link PyVaParser.FalseExprContext} being exited from.
     */
    @Override
    public void exitFalseExpr(PyVaParser.FalseExprContext ctx) {
        setExprType(ctx, BOOL);
    }

    /**
     * Adds an error to the list of errors.
     *
     * @param token The token whose line and column number will be taken for the error message.
     * @param error The error message.
     */
    public void addError(Token token, String error) {
        errors.add(new Exception("(" + token.getLine() + ":" + token.getCharPositionInLine() + "): " + error));
    }

    /**
     * Opens the scopes that exist in this class.
     */
    private void openScopes() {
        threadScope.openScope();
        varScope.openScope();
    }

    /**
     * Closes the scopes that exist in this class.
     */
    private void closeScopes() {
        threadScope.closeScope();
        varScope.closeScope();
    }

    /**
     * Gets the type of an expression given the Expression Context given.
     *
     * @param node The Context to be searched for in the exprTypes {@link ParseTreeProperty}
     * @return The {@link Type} of the expression, the context is in exprTypes already.
     */
    public Type exprType(ParseTree node) {
        return exprTypes.get(node);
    }

    /**
     * Set the Context to a certain type and is stored in the exprTypes {@link ParseTreeProperty}.
     *
     * @param node The Context to which the type is to be assigned.
     * @param type The {@link Type} that is to be assigned to the expression.
     */
    public void setExprType(ParseTree node, Type type) {
        this.exprTypes.put(node, type);
    }

    /**
     * Adds a shared variable with an identifier. If true is returned, then the variable is successfully added as a shared variable.
     *
     * @param id The id of the thread
     * @return true if the variable is a variable defined in the same scope and if the total number of shared
     * variables have not been reached. False otherwise.
     */
    public boolean addSharedVar(String id) {
        if (sharedVars.containsKey(varScope.scopeLevel(id).toString()) && sharedVars.get(varScope.scopeLevel(id).toString()).containsKey(id)) {
            return true;
        }
        if (getTotalShVars() >= 4) {
            return false;
        }
        if (sharedVars.containsKey(varScope.scopeLevel(id).toString())) {
            sharedVars.get(varScope.scopeLevel(id).toString()).put(id, 4 + getTotalShVars());
        } else {
            HashMap<String, Integer> curScopeShVars = new HashMap<>();
            curScopeShVars.put(id, 4 + getTotalShVars());
            sharedVars.put(varScope.scopeLevel(id).toString(), curScopeShVars);
        }
        return true;
    }

    /**
     * Gets the total number of currently shared variables.
     *
     * @return The total number of currently shared variables.
     */
    public int getTotalShVars() {
        int total = 0;
        for (String scopeLevel : sharedVars.keySet()) {
            for (String variable : sharedVars.get(scopeLevel).keySet()) {
                total++;
            }
        }
        return total;
    }

    /**
     * Adds a new child thread if conditions are met. If true is returned,
     * the new child thread is succesfully added.
     *
     * @param id         The name of the new child thread.
     * @param scopeLevel The scope level of the new thread.
     * @return true if the total number of child threads is less than three.
     * False otherwise.
     */
    public boolean addChildThread(String id, ScopeLevel scopeLevel) {
        if (totalChildThreads >= 3) {
            return false;
        }
        if (childThreads.containsKey(scopeLevel.toString())) {
            childThreads.get(scopeLevel.toString()).put(id, ++totalChildThreads);
        } else {
            HashMap<String, Integer> curScopeChildThreads = new HashMap<>();
            curScopeChildThreads.put(id, ++totalChildThreads);
            childThreads.put(scopeLevel.toString(), curScopeChildThreads);
        }
        return true;
    }

    /**
     * Adds a new variable to the scope. Returns true if the addition is succesful.
     * False otherwise.
     *
     * @param varName The name of the to be added variable.
     * @param varType The {@link Type} of the to be added variable.
     * @return true if the addition of the variable to the current scope is successful.
     * False otherwise.
     */
    public boolean addVarToScope(String varName, Type varType) {
        return varScope.put(varName, varType);
    }

    /**
     * Gets the function object of an identifier
     * corresponding to the current {@link ScopeLevel}.
     *
     * @param name The name of the function to get.
     * @return The function object.
     */
    public Function getFunction(String name) {
        return funScope.data(name, varScope.currentScopeLevel());
    }

    /**
     * Gets the variable scope.
     *
     * @return The variable scope.
     */
    public Scope getVarScope() {
        return varScope;
    }

    /**
     * Gets the function scope.
     *
     * @return the function scope.
     */
    public Scope<Function> getFunScope() {
        return funScope;
    }

    /**
     * Gets the type scope.
     *
     * @return the type scope.
     */
    public Scope<Type> getTypeScope() {
        return typeScope;
    }

    /**
     * Gets the thread scope.
     *
     * @return the thread scope.
     */
    public Scope<PyVaThread> getThreadScope() {
        return threadScope;
    }

    /**
     * The expression types corresponding to the {@link ParseTree}.
     *
     * @return the expression types corresponding to the {@link ParseTree}.
     */
    public ParseTreeProperty<Type> getExprTypes() {
        return this.exprTypes;
    }

    /**
     * Gets the shared variable maps.
     *
     * @return the shared variable map.
     */
    public HashMap<String, HashMap<String, Integer>> getSharedVars() {
        return this.sharedVars;
    }

    /**
     * The child threads map.
     *
     * @return the child threads map.
     */
    public HashMap<String, HashMap<String, Integer>> getChildThreads() {
        return this.childThreads;
    }
}
