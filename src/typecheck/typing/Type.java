package typecheck.typing;

import pyva.PyVaLexer;
import typecheck.Function;
import typecheck.scope.ScopeLevel;
import typecheck.scope.Scope;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Class which allows the creation of new {@link Type} instances. This class can be any type, let it be primitive,
 * arrays or user defined types. The usage of this class is limited to the point where only classes within the same package
 * can create new types and update these new types.
 */
public class Type {
    /**
     * The size of each {@link Type} instance in bytes (size of pointer or concrete value)
     */
    public static final int SIZE = 4;
    static final String EMPTY_ARRAY = "[]";
    static final String TANY = getLexerLiteralName(PyVaLexer.STAR);
    static final String TINT = getLexerLiteralName(PyVaLexer.INT);
    static final String TBOOL = getLexerLiteralName(PyVaLexer.BOOLEAN);
    static final String TCHAR = getLexerLiteralName(PyVaLexer.CHAR);
    static final String TVOID = getLexerLiteralName(PyVaLexer.VOID);
    static final String TNULL = "null";
    static final String TTHREAD = getLexerLiteralName(PyVaLexer.THREAD);
    static final String[] TTYPES = new String[]{TANY, TINT, TBOOL, TCHAR, TVOID, TNULL, TTHREAD};

    // method needed because getLiteralName produces strings like: "'value'", but we dont want the '
    private static String getLexerLiteralName(int i) {
        String result = PyVaLexer.VOCABULARY.getLiteralName(i);
        return result.substring(1, result.length() - 1);
    }

    /**
     * sealed {@link Type} instance, comparing non-array types to the ANY type, will always result in true.
     * If used with array types, the restriction lies in the size of the array, that is, an ANY type may not
     * have more dimensions than a non ANY type.
     */
    public static final Type ANY = new Type(TANY);
    /**
     * sealed {@link Type} instance, representing integers.
     */
    public static final Type INT = new Type(TINT);
    /**
     * sealed {@link Type} instance, representing booleans.
     */
    public static final Type BOOL = new Type(TBOOL);
    /**
     * sealed {@link Type} instance, representing characters.
     */
    public static final Type CHAR = new Type(TCHAR);
    /**
     * sealed {@link Type} instance, representing void (non-type)
     */
    public static final Type VOID = new Type(TVOID);
    /**
     * sealed {@link Type} instance, representing the java null equivalent.
     * This type is accepted by all {@link Type types} with {@link ETypeClass#COMPOUND}
     */
    public static final Type NULL = new Type(TNULL);
    /**
     * sealed {@link Type} instance, representing threads.
     */
    public static final Type THREAD;

    static {
        Map<String, Type> variables = new HashMap<>();
        variables.put("id", CHAR.toArray(1));
        THREAD = new Type(TTHREAD, ETypeClass.COMPOUND, null, true, 0, NULL, variables, null);
    }

    private static final Type[] TYPES = new Type[]{ANY, INT, BOOL, CHAR, VOID, NULL, THREAD};

    private final String identifier;
    private final ETypeClass typeClass;
    private final EPrimitiveType primitiveType;

    /**
     * Final boolean whether this type instance is sealed or not (may be overridden or not)
     */
    final boolean sealed;

    private final int depth;
    /**
     * Inner {@link Type} of an array.
     * Example: The inner type of the type with identifier "int[][]" is "int[]"
     */
    public final Type innerType;

    private final Map<String, Type> variables;
    private Map<String, Integer> variableOffsets;
    private final Map<String, Function> functions;
    private Map<String, Integer> functionOffsets;

    /**
     * Function that tries to parse a {@link Type} instance. If the supplied type exists
     * in the supplied {@link Scope type scope}, it will return that Type.
     *
     * @param type       The identifier of the type to parse
     * @param types      The type scope
     * @param scopeLevel The scope level to start looking from
     * @return The parsed type or {@link Type#NULL} if it does not exist.
     */
    public static Type tryType(String type, Scope<Type> types, ScopeLevel scopeLevel) {
        Type t = parsePredefinedType(type);
        if (t != NULL) {
            return t;
        }
        if (types != null) {
            String baseId = parseBaseIdentifier(type);
            Type base = types.type(baseId, scopeLevel);
            int depth = countArrayDepth(type);
            if (base != null) {
                return toArray(base, depth);
            }
        }
        return NULL;
    }

    /**
     * Takes a Type and returns a new {@link Type} instance with the given dimension
     *
     * @param type      The Type to create an array of
     * @param dimension The dimension of the array
     * @return The new array type. {@link Type#NULL null} and {@link Type#VOID void} may never be converted to arrays.
     */
    private static Type toArray(Type type, int dimension) {
        // null and void cannot become arrays
        if (type.identifier.equals(TNULL)) {
            return NULL;
        } else if (type.identifier.equals(TVOID)) {
            return VOID;
        }
        return new Type(type, dimension);
    }

    /**
     * Creates a new array {@link Type} with specified dimension.
     *
     * @param dimension The dimension of the array
     * @return The new array type, if the type was already an array the dimension of {@code this} instance is ignored.
     */
    public Type toArray(int dimension) {
        return toArray(this, dimension);
    }

    /**
     * Parses a predefined type (any type that is contained in {@link Type#TYPES types})
     *
     * @param id The id to parse
     * @return The parsed predefined type or {@link Type#NULL null} if it could not parse
     */
    static Type parsePredefinedType(String id) {
        if (id == null) {
            return NULL;
        }
        String baseId = parseBaseIdentifier(id);
        int depth = countArrayDepth(id);
        Type baseType = NULL;

        for (Type t : TYPES) {
            if (baseId.equals(t.identifier)) {
                baseType = t;
                break;
            }
        }

        //NULL and VOID cannot be arrays
        if (baseType == NULL) {
            return NULL;
        } else if (baseType == VOID) {
            return VOID;
        } else if (depth > 0) {
            return baseType.toArray(depth);
        } else {
            return baseType;
        }
    }

    private Type(String identifier) {
        this(identifier, null, null);
    }


    private Type(Type type, int depth) {
        this.identifier = type.identifier;
        this.typeClass = type.typeClass;
        this.primitiveType = type.primitiveType;
        this.sealed = type.sealed;
        this.depth = depth;
        if (depth > 0) {
            this.innerType = new Type(type, depth - 1);
        } else {
            this.innerType = NULL;
        }
        this.variables = type.variables;
        this.variableOffsets = type.variableOffsets;
        this.functions = type.functions;
        this.functionOffsets = type.functionOffsets;
    }

    /**
     * Creates a new Type instance based off supplied parameters
     *
     * @param identifier The identifier of this new type
     * @param variables  The variables this type should contain, only used if the {@link Type#typeClass type class} equals {@link ETypeClass#COMPOUND compound}
     * @param functions  The functions this type should contain, only used if the {@link Type#typeClass type class} equals {@link ETypeClass#COMPOUND compound}
     */
    Type(String identifier, Map<String, Type> variables, Map<String, Function> functions) {
        this.identifier = parseBaseIdentifier(identifier);
        this.typeClass = ETypeClass.parseBase(identifier);
        this.primitiveType = EPrimitiveType.parse(this.identifier);
        this.sealed = this.typeClass != ETypeClass.COMPOUND; // you cannot override non compound types
        this.depth = countArrayDepth(identifier);
        if (depth > 0) {
            // we have to do it using the substring method, because 'this' instance will not be initialized
            // enough to be used like 'new Type(this, depth - 1);' Using that would throw InitializationException
            this.innerType = new Type(identifier.substring(0, identifier.length() - EMPTY_ARRAY.length()));
        } else {
            this.innerType = NULL;
        }
        if (this.typeClass == ETypeClass.COMPOUND) {
            this.variables = variables != null ? variables : new HashMap<>();
            this.functions = functions != null ? functions : new HashMap<>();
        } else {
            this.variables = null;
            this.functions = null;
        }
        computeOffsets();
    }

    private Type(String identifier, ETypeClass typeClass, EPrimitiveType primitiveType, boolean sealed, int depth, Type innerType, Map<String, Type> variables, Map<String, Function> functions) {
        this.identifier = identifier;
        this.typeClass = typeClass;
        this.primitiveType = this.typeClass == ETypeClass.PRIMITIVE ? primitiveType : null;
        this.sealed = sealed;
        this.depth = depth;
        this.innerType = innerType;
        if (this.typeClass == ETypeClass.COMPOUND) {
            this.variables = variables != null ? variables : new HashMap<>();
            this.functions = functions != null ? functions : new HashMap<>();
        } else {
            this.variables = null;
            this.functions = null;
        }
        computeOffsets();
    }

    /**
     * Computes the offsets of variables and functions.
     */
    private void computeOffsets() {
        if (variables != null && functions != null) {
            this.variableOffsets = new HashMap<>();
            this.functionOffsets = new HashMap<>();
            int i = 0;
            for (Map.Entry<String, Type> e : this.variables.entrySet()) {
                variableOffsets.put(e.getKey(), SIZE * i);
                i++;
            }
            i = 0;
            for (Map.Entry<String, Function> e : this.functions.entrySet()) {
                functionOffsets.put(e.getKey(), SIZE * i);
                i++;
            }
        }
    }

    /**
     * Test whether the Type is a primitive type
     *
     * @return True if primitive
     */
    public boolean isPrimitive() {
        return this.primitiveType != null && !this.isArray();
    }

    /**
     * Test whether the Type is a compound (user defined) type
     *
     * @return True if compound type
     */
    public boolean isCompound() {
        return this.typeClass == ETypeClass.COMPOUND;
    }

    /**
     * Tests whether the type is an array
     *
     * @return True if array
     */
    public boolean isArray() {
        return this.depth > 0;
    }

    /**
     * Counts the dimension of this type, based off the identifier
     *
     * @param type The identifier of the type
     * @return The dimension of the type
     */
    private static int countArrayDepth(String type) {
        int count = 0;
        String tmp = type;
        while (tmp.endsWith(EMPTY_ARRAY)) {
            count++;
            tmp = tmp.substring(0, tmp.length() - 2);
        }
        return count;
    }

    /**
     * Retrieves a variable from a {@link ETypeClass#COMPOUND compound} type
     *
     * @param id The variable's identifier this type is supposed to contain
     * @return The variable's type or {@link Type#NULL null} if it does not exist
     */
    public Type getVariable(String id) {
        Type t = variables.get(id);
        if (t == null) {
            return NULL; // Type can never be 'null', if anything its at least Type.NULL
        }
        return t;
    }

    /**
     * Retrieves the offset of a variable from a {@link ETypeClass#COMPOUND compound} type
     *
     * @param id The variable's identifier of which to retrieve the offset from
     * @return The offset of the variable
     */
    public Integer getVariableOffset(String id) {
        return this.variableOffsets.get(id);
    }

    /**
     * Test whether this type contains the variable supplied by id
     *
     * @param id The identifier of the variable
     * @return True if the variable exists
     */
    public boolean hasVariable(String id) {
        return variables.containsKey(id);
    }

    /**
     * Adds a variable to a {@link ETypeClass#COMPOUND compound} type along with its type.
     * Supplied type may not be {@link Type#NULL null}
     *
     * @param id   The identifier of the variable
     * @param type The type bound to the identifier
     */
    void addVariable(String id, Type type) {
        variableOffsets.put(id, variables.size() * SIZE);
        variables.put(id, type);
    }

    /**
     * Test whether this type has a functions supplied by id
     *
     * @param id The identifier of the function
     * @return True if exists
     */
    public boolean hasFunction(String id) {
        return functions.containsKey(id);
    }

    /**
     * Retrieves the {@link Function} from a {@link ETypeClass#COMPOUND compound} type
     *
     * @param id The identifier of the function
     * @return The functions bound to the identifier
     */
    public Function getFunction(String id) {
        return functions.get(id);
    }

    /**
     * Adds a {@link Function} to a {@link ETypeClass#COMPOUND compound} type
     *
     * @param id The identifier of the function
     * @param f  The function to add
     */
    void addFunction(String id, Function f) {
        functionOffsets.put(id, functions.size() * SIZE);
        functions.put(id, f);
    }

    /**
     * Retrieves the offset of a function
     *
     * @param id The identifier of the function
     * @return The offset of the function
     */
    public Integer getFunctionOffset(String id) {
        return functionOffsets.get(id);
    }

    /**
     * Strips the type from trailing brackets ('[]')
     *
     * @param type The type to strip
     * @return The stripped type
     */
    public static String parseBaseIdentifier(String type) {
        // strips the identifier from any following '[]'
        return type.split(Pattern.quote(EMPTY_ARRAY))[0];
    }

    /**
     * Returns the identifier of this type
     *
     * @return The identifier
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Returns the dimension of this type
     *
     * @return The dimension
     */
    public int getDepth() {
        return this.depth;
    }

    /**
     * The size this Type takes when placed onto the heap in bytes
     *
     * @return Heap size in bytes
     */
    public int heapSize() {
        if (this.typeClass == ETypeClass.COMPOUND) {
            return SIZE * variables.size();
        }
        return Type.SIZE;
    }

    /**
     * Test whether this instance accepts the type of other instance. {@link Type#ANY Any} and {@link Type#NULL null} types accept more than others.
     *
     * @param other The other type
     * @return True if this type accepts the other type
     */
    public boolean accepts(Type other) {
        if (other == null) {
            return false;
        }
        if (this.equals(other)) {
            return true;
        } else if (this == NULL || other == NULL) {
            // only arrays and compound can be nullable
            return (this.isArray() || this.isCompound()) || (other.isArray() || other.isCompound());
        } else if (this.typeClass == ETypeClass.ANY || other.typeClass == ETypeClass.ANY) {
            if (this.typeClass == other.typeClass) { // both any type
                return true;
            } else {
                if (this.typeClass == ETypeClass.ANY) {
                    return this.depth <= other.depth;
                } else {
                    return other.depth <= this.depth;
                }
            }
        }
        return false;
    }

    /**
     * Tests equivalence of types
     *
     * @param o The other type to test equivalence of
     * @return True if both types share the same identifier (as well as dimension for arrays)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {  //if same pointer in memory
            return true;
        }
        if (o instanceof Type) {
            Type other = (Type) o;

            if (this.isArray() || other.isArray()) {
                if (this.isArray() && other.isArray()) {
                    // if one is array, both need to be array and have the same depth and have same identifier
                    return this.identifier.equals(other.identifier) && this.depth == other.depth;
                } else {
                    return false;
                }
            }
            return this.identifier.equals(other.identifier);
        }
        return false;
    }

    /**
     * Hashcode is needed for Sets
     *
     * @return Hashcode
     */
    @Override
    public int hashCode() {
        return (identifier + depth).hashCode();
    }

    /**
     * Creates a string representation of the type which equals the identifier plus the dimension if its an array type
     *
     * @return The string rep
     */
    @Override
    public String toString() {
        StringBuilder arrays = new StringBuilder();
        for (int i = 0; i < depth; i++) {
            arrays.append(EMPTY_ARRAY);
        }
        return identifier + arrays.toString();
    }
}
