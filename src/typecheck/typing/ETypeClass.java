package typecheck.typing;

import java.util.regex.Pattern;

import static typecheck.typing.Type.*;

/**
 * Package protected class used by {@link Type} to keep track of the type class
 */
enum ETypeClass {
    PRIMITIVE, COMPOUND, ANY, VOID, NULL;

    /**
     * Parses a base type based of supplied type
     *
     * @param type The type to parse
     * @return The {@link Type} that has been parsed, if the supplied type does not equal a primitive or predefined type it will return {@link ETypeClass#COMPOUND}
     */
    private static ETypeClass parse(String type) {
        if (type.equals(TVOID)) {
            return VOID;
        } else if (type.equals(TANY)) {
            return ANY;
        } else if (type.equals(TNULL)) {
            return NULL;
        } else if (type.equals(TINT) | type.equals(TBOOL) | type.equals(TCHAR)) {
            return PRIMITIVE;
        } else {
            return COMPOUND;
        }
    }

    /**
     * Method used to strip the trailing brackets ('[]'). Used to decompose an array type into its base type
     *
     * @param type The type to strip the trailing of
     * @return The stripped type
     */
    static ETypeClass parseBase(String type) {
        // strips the identifier from any following '[]', thus returning any identifier class but ARRAY
        return parse(type.split(Pattern.quote(EMPTY_ARRAY))[0]);
    }
}
