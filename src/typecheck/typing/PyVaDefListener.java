package typecheck.typing;

import exceptions.Exception;
import exceptions.passes.FunctionRedefintionException;
import exceptions.passes.RedefinitionException;
import exceptions.passes.TypeOverrideException;
import exceptions.passes.TypeRedefinitionException;
import pyva.PyVaBaseListener;
import pyva.PyVaParser;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import typecheck.Function;
import typecheck.scope.Scope;

import java.util.ArrayList;
import java.util.HashMap;

import static typecheck.typing.Type.VOID;

/**
 * Lists the identifiers of functions and types in {@link Scope scopes} and makes instances of them, but
 * does not make the created functions and types concrete, this is done by {@link PyVaConcreteDefListener}
 * @see PyVaConcreteDefListener
 * @see Scope
 * @see Type
 * @see Function
 * @see Exception
 */
@SuppressWarnings("Duplicates")
class PyVaDefListener extends PyVaBaseListener {
    private Scope<Type> typeScope;
    private Scope<Function> funScope;
    private ArrayList<Exception> errors;

    /**
     * Lists the definitions of created types and functions
     *
     * @param tree The parse tree to list the types and functions of
     * @throws RedefinitionException Thrown when sealed types are overridden or type/functions identifiers' are already defined in the current open scope
     */
    void listDefinitions(ParseTree tree) throws RedefinitionException {
        this.funScope = new Scope<>();
        this.typeScope = new Scope<>();
        this.errors = new ArrayList<>();
        new ParseTreeWalker().walk(this, tree);
        if (errors.size() > 0) {
            throw new RedefinitionException(Exception.getMessage(errors));
        }
    }

    /**
     * Creates a new {@link Type} based off the identifier taken from context.
     * Adds an {@link TypeOverrideException override error} if the user tries to override {@link Type#sealed sealed} types
     * Adds a {@link TypeRedefinitionException redefinition error} if the identifier of this newly created type already exists in current scope
     *
     * @param ctx The context
     */
    @Override
    public void enterRecorddecl(PyVaParser.RecorddeclContext ctx) {
        Type type = Type.parsePredefinedType(ctx.identifier().getText());
        Type newType = new Type(ctx.identifier().getText(), new HashMap<>(), new HashMap<>());
        if (newType.equals(type) && type.sealed) {
            addError(new TypeOverrideException(errorMessage(ctx.identifier().start, String.format("Cannot define type '%s' because this type is sealed", type))));
        } else if (!addTypeToScope(newType)) {
            addError(new TypeRedefinitionException(errorMessage(ctx.identifier().start, String.format("Cannot redefine type '%s' within the same scope", type))));
        }
        openScopes();
    }

    /**
     * Creates a new {@link Function} with default return Type equal to {@link Type#VOID void}.
     * Adds a {@link FunctionRedefintionException redefintion error} when the identifier already existed in the current scope
     * Opens scopes after the function has been added to the scope
     *
     * @param ctx The context
     */
    @Override
    public void enterFuncdecl(PyVaParser.FuncdeclContext ctx) {
        Function function = new Function(ctx.ID().getText(), VOID, new ArrayList<>(), new ArrayList<>());
        if (!addFuncToScope(function)) {
            addError(new FunctionRedefintionException(errorMessage(ctx.ID().getSymbol(), String.format("Cannot redefine function '%s' within the same scope", function.identifier))));
        }
        openScopes(); // open scopes after the addition of function in previous scope
    }

    /**
     * Returns the function scope of this listener
     *
     * @return The functions scope
     */
    Scope<Function> getFunScope() {
        return this.funScope;
    }

    /**
     * Return the type scope of this listener
     *
     * @return The type scope
     */
    Scope<Type> getTypeScope() {
        return this.typeScope;
    }

    /**
     * Opens scopes on enter
     *
     * @param ctx The context
     */
    @Override
    public void enterBlockdecl(PyVaParser.BlockdeclContext ctx) {
        openScopes();
    }

    /**
     * Opens scopes on enter
     *
     * @param ctx The context
     */
    @Override
    public void enterFordecl(PyVaParser.FordeclContext ctx) {
        openScopes();
    }

    /**
     * Closes scopes on exit
     *
     * @param ctx The context
     */
    @Override
    public void exitFuncdecl(PyVaParser.FuncdeclContext ctx) {
        closeScopes();
    }

    /**
     * Closes scopes on exit
     *
     * @param ctx The context
     */
    @Override
    public void exitRecorddecl(PyVaParser.RecorddeclContext ctx) {
        closeScopes();
    }

    /**
     * Closes scopes on exit
     *
     * @param ctx The context
     */
    @Override
    public void exitBlockdecl(PyVaParser.BlockdeclContext ctx) {
        closeScopes();
    }

    /**
     * Closes scopes on exit
     *
     * @param ctx The context
     */
    @Override
    public void exitFordecl(PyVaParser.FordeclContext ctx) {
        closeScopes();
    }

    private void openScopes() {
        funScope.openScope();
        typeScope.openScope();
    }

    private void closeScopes() {
        funScope.closeScope();
        typeScope.closeScope();
    }

    private String errorMessage(Token token, String error) {
        return "(" + token.getLine() + ":" + token.getCharPositionInLine() + "): " + error;
    }

    private void addError(RedefinitionException e) {
        errors.add(e);
    }

    private boolean addTypeToScope(Type type) {
        return typeScope.put(type.getIdentifier(), type);
    }

    private boolean addFuncToScope(Function function) {
        return funScope.put(function.identifier, function.getReturnType(), function);
    }

}
