package typecheck.typing;

import static typecheck.typing.Type.*;

/**
 * Package protected class used by {@link Type} to keep track of the primitive type
 */
enum EPrimitiveType {
    INT, BOOL, CHAR;

    /**
     * Parses the enum based off supplied id
     *
     * @param id The id to parse
     * @return A {@link EPrimitiveType primitive type instance} if id equals the string literal of a primitive type from {@link Type#TTYPES}
     */
    static EPrimitiveType parse(String id) {
        if (id.equals(TINT)) {
            return INT;
        } else if (id.equals(TBOOL)) {
            return BOOL;
        } else if (id.equals(TCHAR)) {
            return CHAR;
        }
        return null;
    }
}
