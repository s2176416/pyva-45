package typecheck.typing;

import exceptions.DefinitionException;
import exceptions.Exception;
import exceptions.OutOfScopeException;
import exceptions.passes.*;
import pyva.PyVaBaseListener;
import pyva.PyVaParser;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import typecheck.Function;
import typecheck.scope.Scope;

import java.util.ArrayList;

/**
 * Builds a {@link Scope function scope} and a {@link Scope type scope}. This class uses
 * {@link PyVaDefListener} to list available functions and types and uses the result of that to make the
 * functions and types concrete and final.
 * @see PyVaDefListener
 */
@SuppressWarnings("Duplicates")
public class PyVaConcreteDefListener extends PyVaBaseListener {
    private PyVaDefListener defs = new PyVaDefListener();
    private Scope<Type> typeScope;
    private Scope<Function> funScope;
    private ArrayList<Exception> errors;

    /**
     * Creates the function scope and type scope
     *
     * @param tree The parse tree to list the types and functions of
     * @throws DefinitionException If types or functions are improperly defined
     */
    public void listDefinitions(ParseTree tree) throws DefinitionException {
        defs.listDefinitions(tree);
        this.funScope = new Scope<>();
        this.typeScope = new Scope<>();
        this.errors = new ArrayList<>();
        new ParseTreeWalker().walk(this, tree);
        if (errors.size() > 0) {
            throw new DefinitionException(Exception.getMessage(errors));
        }
        this.typeScope = defs.getTypeScope();
        this.funScope = defs.getFunScope();
    }

    /**
     * Completes the {@link Type} definition of types listed by {@link PyVaConcreteDefListener#defs PyVaDefListener}.
     * Adds an {@link OutOfScopeException} when variable types do not exist in the current scope.
     * Adds a {@link FieldRedefinitionException} when variables names are not unique.
     *
     * @param ctx The context
     */
    @Override
    public void enterRecorddecl(PyVaParser.RecorddeclContext ctx) {
        Type type = defs.getTypeScope().type(ctx.identifier().getText(), typeScope.currentScopeLevel());
        for (PyVaParser.RecordvartpyeContext var : ctx.recordvartpye()) {
            String varId = var.ID().getText();
            Type varType = Type.tryType(var.identifier().getText(), defs.getTypeScope(), typeScope.currentScopeLevel());
            if (varType == Type.NULL) {
                addError(new OutOfScopeException(errorMessage(var.identifier().start, String.format("Undefined field Type '%s' for field '%s'", var.identifier().getText(), varId))));
            } else if (type.hasVariable(varId)) {
                addError(new FieldRedefinitionException(errorMessage(var.ID().getSymbol(), String.format("Field named '%s' already exists within identifier declaration", varId))));
            } else {
                type.addVariable(varId, varType);
            }
        }
        openScopes();
    }

    /**
     * Completes the {@link Function} definition of functions listed by {@link PyVaConcreteDefListener#defs PyVaDefListener}.
     * Adds an {@link OutOfScopeException} when parameter or return type does not exist in the current scope.
     * Adds a {@link VarRedefinitionException} when parameter names are not unique
     *
     * @param ctx The context
     */
    @Override
    public void enterFuncdecl(PyVaParser.FuncdeclContext ctx) {
        Function f = defs.getFunScope().data(ctx.ID().getText(), funScope.currentScopeLevel());
        Type returnType = Type.tryType(ctx.funcType().getText(), defs.getTypeScope(), funScope.currentScopeLevel());
        if (returnType == Type.NULL) {
            addError(new OutOfScopeException(errorMessage(ctx.start, String.format("Undefined return Type '%s'", ctx.funcType().getText()))));
        } else {
            f.setReturnType(returnType);
        }
        openScopes(); // open scope for params (SAME AS WITH PyVaTypeScopeCheck)
        if (ctx.params().identifier() != null) {
            for (int i = 0; i < ctx.params().ID().size(); i++) {
                String typeString = ctx.params().identifier(i).getText();
                String paramId = ctx.params().ID(i).getText();
                Type paramType = Type.tryType(typeString, defs.getTypeScope(), funScope.currentScopeLevel());
                if (paramType == Type.NULL) {
                    addError(new OutOfScopeException(errorMessage(ctx.params().identifier(i).start, String.format("Undefined parameter Type '%s' for param '%s'", typeString, paramId))));
                } else if (f.getParamNames().contains(paramId)) {
                    addError(new VarRedefinitionException(errorMessage(ctx.params().identifier(i).start, String.format("Cannot use the same param identifier '%s' within the same function", paramId))));
                } else {
                    f.getParamNames().add(paramId);
                    f.getParamTypes().add(paramType);
                }
            }
        }
    }

    /**
     * Returns the function scope
     *
     * @return Returns {@link PyVaConcreteDefListener#funScope}
     */
    public Scope<Function> getFunScope() {
        return this.funScope;
    }

    /**
     * Returns the type scope
     *
     * @return Returns {@link PyVaConcreteDefListener#typeScope}
     */
    public Scope<Type> getTypeScope() {
        return this.typeScope;
    }

    /**
     * Opens scopes on enter
     *
     * @param ctx The context
     */
    @Override
    public void enterBlockdecl(PyVaParser.BlockdeclContext ctx) {
        openScopes();
    }

    /**
     * Opens scopes on enter
     *
     * @param ctx The context
     */
    @Override
    public void enterFordecl(PyVaParser.FordeclContext ctx) {
        openScopes();
    }

    /**
     * Closes scopes on exit
     *
     * @param ctx The context
     */
    @Override
    public void exitFuncdecl(PyVaParser.FuncdeclContext ctx) {
        closeScopes();
    }

    /**
     * Closes scopes on exit
     *
     * @param ctx The context
     */
    @Override
    public void exitRecorddecl(PyVaParser.RecorddeclContext ctx) {
        closeScopes();
    }

    /**
     * Closes scopes on exit
     *
     * @param ctx The context
     */
    @Override
    public void exitBlockdecl(PyVaParser.BlockdeclContext ctx) {
        closeScopes();
    }

    /**
     * Closes scopes on exit
     *
     * @param ctx The context
     */
    @Override
    public void exitFordecl(PyVaParser.FordeclContext ctx) {
        closeScopes();
    }

    private void openScopes() {
        funScope.openScope();
        typeScope.openScope();
    }

    private void closeScopes() {
        funScope.closeScope();
        typeScope.closeScope();
    }

    private String errorMessage(Token token, String error) {
        return "(" + token.getLine() + ":" + token.getCharPositionInLine() + "): " + error;
    }

    private void addError(Exception e) {
        errors.add(e);
    }
}
