package codegeneration;

import codegeneration.util.EAddrImmDI;
import codegeneration.util.EReg;
import codegeneration.util.ETarget;
import codegeneration.util.Op;
import exceptions.Exception;
import pyva.PyVaBaseVisitor;
import pyva.PyVaParser;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;
import typecheck.*;
import typecheck.scope.Scope;
import typecheck.scope.ScopeLevel;
import typecheck.typing.Type;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static codegeneration.util.AddrImmDI.*;
import static codegeneration.util.EReg.*;
import static codegeneration.util.ETarget.*;
import static codegeneration.util.Instruction.*;
import static codegeneration.util.Op.*;
import static typecheck.typing.Type.*;

/**
 * Class used to generate Sprockell code based off supplied parse tree.
 * This class depends heavily on {@link PyVaTypeScopeCheck} and may throw exceptions when {@link CodeGenerator#typeScopeCheck} does.
 * @see PyVaTypeScopeCheck
 * @see codegeneration.util.AddrImmDI
 * @see codegeneration.util.EAddrImmDI
 * @see codegeneration.util.EReg
 * @see codegeneration.util.ETarget
 * @see codegeneration.util.Instruction
 * @see codegeneration.util.Op
 */
@SuppressWarnings("Duplicates")
public class CodeGenerator extends PyVaBaseVisitor<String> {
    private final PyVaTypeScopeCheck typeScopeCheck = new PyVaTypeScopeCheck();
    private Scope varScope;
    private Scope<Function> funScope;
    private Scope<Type> typeScope;
    private Scope<PyVaThread> threadScope;
    private StringBuilder result = new StringBuilder();
    private Scope scope;
    private ParseTreeProperty<Integer> instructions;
    private ParseTreeProperty<Type> exprTypes;
    private ParseTreeProperty<Type> accessTypes;
    private ParseTreeProperty<Integer> startLocs;
    private Map<String, Integer> functionLocation;
    private List<String> functionFormatList;
    private HashMap<String, HashMap<String, Integer>> sharedVars;
    private HashMap<String, HashMap<String, Integer>> childThreads;
    private int threadTurn;
    private int inThreads;
    private int inBlock;

    private static final String NL = "\n\t";
    private static final String DELIM = NL + ", ";
    private static final String SEP = ", ";

    /**
     * Generates the Sprockell code from the given parse tree. This parse tree will first be passed on to a {@link PyVaTypeScopeCheck}
     * to initialize scopes of variables, functions, types and threads.
     *
     * @param tree The tree to generate code from
     * @return Executable Sprockell code
     * @throws Exception When {@link PyVaTypeScopeCheck} fails
     */
    public String generate(ParseTree tree) throws Exception {
        this.typeScopeCheck.check(tree);
        this.varScope = this.typeScopeCheck.getVarScope();
        this.funScope = this.typeScopeCheck.getFunScope();
        this.typeScope = this.typeScopeCheck.getTypeScope();
        this.threadScope = this.typeScopeCheck.getThreadScope();
        this.exprTypes = this.typeScopeCheck.getExprTypes();
        this.accessTypes = new ParseTreeProperty<>();
        this.sharedVars = this.typeScopeCheck.getSharedVars();
        this.childThreads = this.typeScopeCheck.getChildThreads();
        this.inThreads = 0;
        this.inBlock = 0;
        EReg reg1 = nextReg();
        EReg reg2 = nextReg();
        freeUpRegister(reg1);
        freeUpRegister(reg2);
        this.result = new StringBuilder("module Program where\n\nimport Sprockell\nimport ProgramFunctions\n\n" +
                "prog :: [Instruction]\nprog = [\n\t " +
                branch(SPRID, ETarget.REL, 2) + SEP +   // If main thread, do the jump of the next instruction. Otherwise jump two instructions
                jump(ETarget.REL, 6) + SEP +    // If main thread, skip loop and jump to the first load instruction 6 instruction below .
                // begin loop
                READINSTR + "(" + INDADDR + SPRID + ")" + SEP + // If not main thread, read value from "cue" shared memory
                RECEIVE + reg1 + SEP +  // If not main thread, receive the value in register 1
                COMPUTE + EQUAL + reg1 + ZERO + reg2 + SEP +    // If not main thread, check if equals to zero
                branch(reg2, REL, -3) + SEP +   // If not main thread and reg1 is equal to zero, go back 3 instructions to read instruction again (until it's not equal to 0).
                // if value in reg1 is not 0, jump to the absolute instruction number given by the main thread written in the "cue" shared memory
                // end loop
                jump(IND, reg1) + SEP +
                load(EAddrImmDI.IMMVALUE, 0, EReg.ARP) + DELIM +
                load(EAddrImmDI.IMMVALUE, 0, EReg.MEM) + DELIM
        ); // Check whether registers have a default value of 0 or they need to be instantiated. If the former, comment out the string after "["
        this.scope = new Scope();
        this.instructions = new ParseTreeProperty<>();
        this.startLocs = new ParseTreeProperty<>();
        this.functionLocation = new HashMap<>();
        this.functionFormatList = new LinkedList<>();
        putStart(tree, 2);
        this.threadTurn = 0;
        putStart(tree, 9);
        result.append(visit(tree)).append(ENDPROG + "]\n\nmain = run [prog");
        for (int i = 0; i < threadTurn; i++) {
            result.append(", prog");
        }
        result.append("]");
        return getResult();
    }

    /**
     * Uses the TestAndSet instruction to check whether the global lock is currently locked or not.
     * 0 means it's locked and 1 means it isn't.
     * If it's locked, then it busy waits until it isn't.
     * If it isn't locked, then the lock is locked and the program inside the sync
     * block is done by that thread. Other threads that want to enter sync blocks would have to
     * busy wait until this thread finishes the sync block and the lock becomes unlocked again.
     * @param ctx The {@link PyVaParser.SyncdeclContext} that is being visited.
     * @return the string representation of the instructions made by the sync block.
     */
    @Override
    public String visitSyncdecl(PyVaParser.SyncdeclContext ctx) {
        StringBuilder result = new StringBuilder();
        EReg reg = nextReg();
        freeUpRegister(reg);
        int ins = 0;

        result.append(TESTANDSET + "(" + DIRADDR + "0)" + SEP);
        result.append(RECEIVE).append(reg).append(SEP);
        result.append(COMPUTE).append(EQUAL).append(ZERO).append(reg).append(reg).append(SEP);
        result.append(branch(reg, REL, -3)).append(DELIM);
        ins += 4;

        putIns(ctx, ins);
        result.append(preScope(ctx));

        putStart(ctx.block(), startLoc(ctx) + ins(ctx));
        result.append(visit(ctx.block()));
        putIns(ctx, ins(ctx) + ins(ctx.block()));

        result.append(load(EAddrImmDI.INDADDR, EReg.ARP, EReg.ARP)).append(DELIM); // POST-SCOPE

        result.append(WRITEINSTR).append(ZERO).append("(").append(DIRADDR).append("0)").append(DELIM);

        putIns(ctx, ins(ctx) + 2);
        return result.toString();
    }

    /**
     * The main thread waits for the cue register of the child thread that is being joined
     * to be 0. If the thread was initially called, then the value in the cue register of that thread
     * should not be zero. Therefore, the main thread busy waits until the child thread that is being joined
     * to set the value of the cue shared register back to 0. This is complimented by the fact that child threads
     * will set the value in their respective shared cue registers back to zero once they have finished executing their thread blocks.
     * @param ctx The {@link PyVaParser.JoindeclContext} that is visited
     * @return the string representation of the instructions made by the join.
     */
    @Override
    public String visitJoindecl(PyVaParser.JoindeclContext ctx) {
        StringBuilder result = new StringBuilder();
        ScopeLevel scopeLevel = threadScope.scopeLevel(ctx.ID().getText(), scope.currentScopeLevel());
        int childNumber = childThreads.get(scopeLevel.toString()).get(ctx.ID().getText());

        EReg reg = nextReg();
        freeUpRegister(reg);

        result.append(READINSTR + "(" + DIRADDR).append(childNumber).append(")").append(SEP);
        result.append(RECEIVE).append(reg).append(SEP);
        result.append(COMPUTE).append(NEQ).append(reg).append(ZERO).append(reg).append(SEP);
        result.append(branch(reg, REL, -3)).append(DELIM);

        putIns(ctx, 4);
        return result.toString();
    }

    /**
     * The calling thread will set the value of the shared cue register of the to be called thread
     * to the absolute index of the instruction the child thread is to start their instructions on.
     * The currently busy waiting child thread will notice that the value in their shared cue register is
     * no longer zero and jump to the absolute instruction index and start executing instructions.
     * Once the thread block has been executed by the child thread, the child thread will set the value
     * in their respective shared cue register back to zero, and they will do EndProg.
     * The calling thread will just skip over the thread block and continue with whatever instructions
     * comes after the thread block.
     * @param ctx The {@link PyVaParser.ThreaddeclContext} that is visited
     * @return the string representation of the instructions made by the thread block.
     */
    @Override
    public String visitThreaddecl(PyVaParser.ThreaddeclContext ctx) {
        inThreads++;
        EReg reg = nextReg();
        freeUpRegister(reg);
        StringBuilder result = new StringBuilder();
        putIns(ctx, 0);

        String prescope = preScope(ctx);
        int prescopeIns = ins(ctx);

        putStart(ctx.block(), startLoc(ctx) + 3 + prescopeIns); // 3 from the three instructions before appending the blockVisit below
        String blockVisit = visit(ctx.block());

        result.append(load(EAddrImmDI.IMMVALUE, startLoc(ctx.block()), reg)).append(SEP);
        result.append(WRITEINSTR).append(reg).append("(").append(DIRADDR).append(++threadTurn).append(")").append(SEP);
        result.append(jump(ETarget.REL, ins(ctx.block()) + 4 + prescopeIns)).append(SEP);
        result.append(prescope); // PRE-SCOPE
        result.append(blockVisit); // append block visit instructions
        result.append(load(EAddrImmDI.INDADDR, EReg.ARP, EReg.ARP)).append(DELIM); // POST-SCOPE
        result.append(WRITEINSTR).append(ZERO).append("(").append(INDADDR).append(SPRID).append(")").append(SEP);
        result.append(ENDPROG + DELIM);


        inThreads--;
        putIns(ctx, ins(ctx.block()) + 6 + prescopeIns);
        return result.toString();
    }

    /**
     * Generates code to print an integer, character or boolean
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitPrintdecl(PyVaParser.PrintdeclContext ctx) {
        putStart(ctx.expr(), startLoc(ctx));
        StringBuilder result = new StringBuilder(visit(ctx.expr()));
        EReg reg = nextReg();
        freeUpRegister(reg);
        if (exprTypes.get(ctx.expr()).equals(INT)) {
            result.append(pop(reg)).append(SEP);
            result.append(WRITEINSTR).append(reg).append(NUMBERIO).append(DELIM);
            putIns(ctx, 2 + ins(ctx.expr()));
        } else if (exprTypes.get(ctx.expr()).equals(BOOL)) {
            result.append(POP).append(reg);
            result.append("] ++ (writeBool ").append(reg).append(")\n\t++ [");
            putIns(ctx, 25 + ins(ctx.expr()));
        } else {
            //char
            result.append(pop(reg)).append(SEP);
            result.append(WRITEINSTR).append(reg).append(CHARIO).append(SEP);
            result.append(load(EAddrImmDI.IMMVALUE, 10, reg)).append(SEP);
            result.append(WRITEINSTR).append(reg).append(CHARIO).append(DELIM);
            putIns(ctx, 4 + ins(ctx.expr()));
        }
        return result.toString();
    }

    /**
     * Generates code to do for-loops using branch instructions. Also opens and closes scopes within this method.
     * Method also updates {@link CodeGenerator#inBlock} for return statements within blocks
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitFordecl(PyVaParser.FordeclContext ctx) {
        putIns(ctx, 0);
        StringBuilder result = new StringBuilder();
        EReg reg1 = nextReg();
        EReg reg2 = nextReg();
        freeUpRegister(reg1);
        freeUpRegister(reg2);

        // OPEN NEW SCOPE
        scope.openScope();
        // preScope and prologue (setup AR and reserve memory)
        result.append(preScope(ctx)).append(prologueScope(ctx));
        // Setup strings
        int start = ins(ctx) + startLoc(ctx);
        int ins = 0;

        // DO INITIAL VARIABLE DECLARATION
        putStart(ctx.vardeclare(0), start);
        result.append(visit(ctx.vardeclare(0)));
        start += ins(ctx.vardeclare(0));
        ins += ins(ctx.vardeclare(0));
        // CHECK CONDITION
        putStart(ctx.expr(), start);
        result.append(visit(ctx.expr()));
        start += ins(ctx.expr());
        ins += ins(ctx.expr());


        this.inBlock++; //IMPORTANT to have this BEFORE visiting program

        putStart(ctx.program(), start + 4);
        String program = visit(ctx.program());

        putStart(ctx.vardeclare(1), start + 4 + ins(ctx.program()));
        String vardeclare1 = visit(ctx.vardeclare(1));

        this.inBlock--; //IMPORTANT to have this AFTER visiting program

        result.append(pop(reg1)).append(SEP)
                .append(load(EAddrImmDI.IMMVALUE, 1, reg2)).append(SEP)
                .append(compute(Op.XOR, reg1, reg2, reg1)).append(SEP)
                .append(branch(reg1, ETarget.REL, 2 + ins(ctx.program()) + ins(ctx.vardeclare(1)))).append(DELIM);
        ins += 4;
        // IF CONDITION TRUE, DO PROGRAM
        result.append(program);
        ins += ins(ctx.program());
        // DO SECOND VARIABLE DECLARATION AFTER EACH ITERATION
        result.append(vardeclare1);
        ins += ins(ctx.vardeclare(1));
        // JUMP BACK TO CHECK CONDITION
        result.append(jump(ETarget.REL, -4 - ins(ctx.program()) - ins(ctx.vardeclare(1)) - ins(ctx.expr()))).append(DELIM);
        ins++;
        result.append(epilogueScope(ctx));
        scope.closeScope();

        putIns(ctx, ins + ins(ctx));
        return result.toString();
    }

    /**
     * Generates code to do while-loops using branching.
     * Method also updates {@link CodeGenerator#inBlock} for return statements within blocks
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitWhiledecl(PyVaParser.WhiledeclContext ctx) {
        putIns(ctx, 0);
        StringBuilder result = new StringBuilder();
        int start = startLoc(ctx);
        int ins = 0;

        EReg reg1 = nextReg();
        EReg reg2 = nextReg();
        freeUpRegister(reg1);
        freeUpRegister(reg2);

        // Condition check
        putStart(ctx.expr(), start);
        result.append(visit(ctx.expr()));
        start += ins(ctx.expr());
        ins += ins(ctx.expr());

        String preScope = preScope(ctx);
        putStart(ctx.block(), start + 4 + ins(ctx));// ins(ctx) == ins of preScope
        this.inBlock++; //IMPORTANT to have this BEFORE visiting block
        String block = visit(ctx.block());
        this.inBlock--; //IMPORTANT to have this AFTER visiting block
        result.append(pop(reg1)).append(SEP)
                .append(load(EAddrImmDI.IMMVALUE, 1, reg2)).append(SEP)
                .append(compute(Op.XOR, reg1, reg2, reg1)).append(SEP)
                .append(branch(reg1, ETarget.REL, 2 + ins(ctx) + ins(ctx.block()))).append(DELIM);
        ins += 4;
        // PRE-CALL
        result.append(preScope);
        // CALL
        result.append(block);
        ins += ins(ctx.block());
        // JUMP BACK TO CONDITION CHECK
        result.append(jump(ETarget.REL, -(4 + ins(ctx)) - ins(ctx.expr()) - ins(ctx.block()))).append(DELIM);// ins(ctx) == ins of preScope
        ins++;

        putIns(ctx, ins + ins(ctx));
        return result.toString();
    }

    /**
     * Generates code for if and if else statements using branching.
     * Method also updates {@link CodeGenerator#inBlock} for return statements within blocks
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitIfdecl(PyVaParser.IfdeclContext ctx) {
        putIns(ctx, 0);
        int start = startLoc(ctx);
        int ins = 0;

        EReg reg1 = nextReg();
        EReg reg2 = nextReg();
        freeUpRegister(reg1);
        freeUpRegister(reg2);

        putStart(ctx.expr(), start);
        StringBuilder result = new StringBuilder(visit(ctx.expr()));
        start += ins(ctx.expr());
        ins += ins(ctx.expr());

        String preScope = preScope(ctx);
        if (ctx.ELSE() == null) {
            // Condition check
            // ins(ctx) == number of ins of preScope
            putStart(ctx.block(0), start + 4 + ins(ctx));

            this.inBlock++; //IMPORTANT to have this BEFORE visiting block
            String blockVisit0 = visit(ctx.block(0));
            this.inBlock--; //IMPORTANT to have this AFTER visiting block

            result.append(pop(reg1)).append(SEP)
                    .append(load(EAddrImmDI.IMMVALUE, 1, reg2)).append(SEP)
                    .append(compute(Op.XOR, reg1, reg2, reg1)).append(SEP)
                    .append(branch(reg1, ETarget.REL, 1 + ins(ctx) + ins(ctx.block(0)))).append(DELIM);
            ins += 4;
            // PRE-SCOPE
            result.append(preScope);
            // call block 0
            result.append(blockVisit0);
            ins += ins(ctx.block(0));

            putIns(ctx, ins + ins(ctx));
        } else {
            // ins(ctx) == number of ins of preScope
            putStart(ctx.block(0), start + 4 + ins(ctx));
            this.inBlock++; // IMPORTANT to have this BEFORE visiting block
            String blockVisit0 = visit(ctx.block(0));
            putStart(ctx.block(1), start + 5 + ins(ctx) + ins(ctx.block(0)));
            String blockVisit1 = visit(ctx.block(1));
            this.inBlock--; // IMPORTANT to have this AFTER visiting block;

            // PRE-SCOPE
            result.append(preScope);

            // Condition check
            result.append(pop(reg1)).append(SEP)
                    .append(load(EAddrImmDI.IMMVALUE, 1, reg2)).append(SEP)
                    .append(compute(Op.XOR, reg1, reg2, reg1)).append(SEP)
                    // If true, go to block 1, else continue with block 0
                    .append(branch(reg1, ETarget.REL, 2 + ins(ctx.block(0)))).append(DELIM);
            ins += 4;
            // Call block 0
            result.append(blockVisit0);
            ins += ins(ctx.block(0));
            // If block 0 called, skip block 1 (the else)
            result.append(jump(ETarget.REL, 1 + ins(ctx.block(1)))).append(DELIM);
            ins++;
            // Call block 1
            result.append(blockVisit1);
            ins += ins(ctx.block(1));

            putIns(ctx, ins + ins(ctx));
        }
        return result.toString();
    }

    /**
     * Generates code for block declarations, within blocks. This only includes adding a preScope before visiting the functionality within the block.
     * Method also updates {@link CodeGenerator#inBlock} for return statements within blocks
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitBlockdecl(PyVaParser.BlockdeclContext ctx) {
        putIns(ctx, 0);
        StringBuilder result = new StringBuilder();
        EReg reg1 = nextReg();
        freeUpRegister(reg1);

        result.append(preScope(ctx));
        inBlock++;
        putStart(ctx.block(), startLoc(ctx) + ins(ctx));
        result.append(visit(ctx.block()));
        inBlock--;
        putIns(ctx, ins(ctx) + ins(ctx.block()));
        return result.toString();
    }

    /**
     * Generates code for blocks. This includes adding a prologueScope, then visiting the block and then an epilogueScope.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitBlock(PyVaParser.BlockContext ctx) {
        putIns(ctx, 0);
        scope.openScope();
        String prologueScope = prologueScope(ctx);
        putStart(ctx.program(), startLoc(ctx) + ins(ctx));
        String program = visit(ctx.program());
        String epilogueScope = epilogueScope(ctx);
        scope.closeScope();
        putIns(ctx, ins(ctx) + ins(ctx.program()));
        return prologueScope + program + epilogueScope;
    }

    @Override
    public String visitProgram(PyVaParser.ProgramContext ctx) {
        StringBuilder result = new StringBuilder();
        int ins = 0;
        int start = startLoc(ctx);
        if (ctx.decl() != null) {
            for (PyVaParser.DeclContext decl : ctx.decl()) {
                putStart(decl, start);
                result.append(visit(decl));
                start += ins(decl);
                ins += ins(decl);
            }
        }
        putIns(ctx, ins);
        return result.toString();
    }

    /**
     * Generates code for var declarations, may it be a new var declaration or reassignment
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitVardecl(PyVaParser.VardeclContext ctx) {
        putStart(ctx.vardeclare(), startLoc(ctx));
        String result = visit(ctx.vardeclare());
        putIns(ctx, ins(ctx.vardeclare()));
        return result;
    }

    /**
     * Generates code for new vars declarations. First the expression on the right is evaluated,
     * then it is used to assign it to the given variables.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitTypevardecl(PyVaParser.TypevardeclContext ctx) {
        putStart(ctx.expr(), startLoc(ctx));
        StringBuilder result = new StringBuilder(visit(ctx.expr()));
        EReg reg1 = nextReg();
        EReg reg2 = nextReg();
        result.append(pop(reg1)).append(SEP);
        int ins = 1 + ins(ctx.expr());
        for (TerminalNode id : ctx.ID()) {
            // If the main thread executes...
            if (inThreads == 0) {
                // offset / 4, because sprockell uses a list instead of bytes
                result.append(load(EAddrImmDI.IMMVALUE, varScope.offset(id.getText(), scope.currentScopeLevel()) / 4, reg2)).append(SEP);
                result.append(compute(Op.ADD, EReg.ARP, reg2, reg2)).append(SEP);
                result.append(store(reg1, EAddrImmDI.INDADDR, reg2)).append(DELIM);
                ins += 3;
                // If variable is shared, write the value of the expression to the shared memory.
                boolean shared = sharedVars.containsKey(scope.currentScopeLevel().toString()) && sharedVars.get(scope.currentScopeLevel().toString()).containsKey(id.getText());
                if (shared) {
                    result.append(WRITEINSTR).append(reg1).append("(").append(DIRADDR).append(sharedVars.get(scope.currentScopeLevel().toString()).get(id.getText())).append(")").append(DELIM);
                    ins += 1;
                }
                // If child thread executes...
            } else {
                result.append(WRITEINSTR).append(reg1).append("(").append(DIRADDR).append(sharedVars.get(scope.currentScopeLevel().toString()).get(id.getText())).append(")").append(DELIM);
                ins += 1;
            }
        }
        freeUpRegister(reg1);
        freeUpRegister(reg2);
        putIns(ctx, ins);
        return result.toString();
    }

    /**
     * Generates code for reassignment to existing variables. First the expression on the right is evaluated,
     * then the expression is assigned to all variables on the left hand side. This includes using access links
     * to find the location of where the variable is stored.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitTypelessvardecl(PyVaParser.TypelessvardeclContext ctx) {
        putStart(ctx.expr(), startLoc(ctx));
        StringBuilder result = new StringBuilder(visit(ctx.expr()));
        int ins = 1 + ins(ctx.expr());
        EReg reg1 = nextReg();
        EReg reg2 = nextReg();
        EReg offset = nextReg();
        result.append(pop(reg1)).append(SEP);
        for (PyVaParser.IdCallContext idCall : ctx.idCall()) {
            ScopeLevel scopeLevel = varScope.scopeLevel(idCall.ID().getText(), scope.currentScopeLevel());
            boolean shared = sharedVars.containsKey(scopeLevel.toString()) && sharedVars.get(scopeLevel.toString()).containsKey(idCall.ID().getText());
            if (inThreads == 0) {// If main thread executes...
                if (shared) {
                    result.append(compute(Op.ADD, EReg.ARP, EReg.ZERO, reg2)).append(SEP);
                    ins++;
                    for (int i = scopeLevel.getDepths().length; i < scope.currentScopeLevel().getDepths().length; i++) {
                        result.append(compute(DECR, reg2, ZERO, reg2)).append(SEP);
                        result.append(load(EAddrImmDI.INDADDR, reg2, reg2)).append(SEP);
                        ins += 2;
                    }
                    // offset / 4 because sprockell uses list instead of bytes
                    result.append(load(EAddrImmDI.IMMVALUE, varScope.offset(idCall.getText(), scopeLevel) / 4, offset)).append(SEP);
                    result.append(compute(Op.ADD, reg2, offset, reg2)).append(SEP);
                    result.append(store(reg1, EAddrImmDI.INDADDR, reg2)).append(DELIM);
                    ins += 3;

                    result.append(WRITEINSTR).append(reg1).append("(").append(DIRADDR).append(sharedVars.get(scopeLevel.toString()).get(idCall.ID().getText())).append(")").append(SEP);
                    ins += 1;
                } else {
                    putStart(idCall, startLoc(ctx) + ins);
                    result.append(visit(idCall));               // visit idCall, pushing a location in memory onto the stack
                    result.append(pop(reg2)).append(SEP);       // pop the location into reg2
                    result.append(store(reg1, EAddrImmDI.INDADDR, reg2)).append(SEP);// store the value of the expression into the address pointed to by reg2
                    ins += 2 + ins(idCall);
                }
            } else {// If child thread executes...
                result.append(WRITEINSTR).append(reg1).append("(").append(DIRADDR).append(sharedVars.get(scopeLevel.toString()).get(idCall.ID().getText())).append(")").append(SEP);
                ins += 1;
            }
        }
        freeUpRegister(reg1);
        freeUpRegister(reg2);
        freeUpRegister(offset);
        putIns(ctx, ins);
        return result.toString();
    }

    /**
     * Generates code for function calls by visiting funCall from the context
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitFuncallExpr(PyVaParser.FuncallExprContext ctx) {
        putStart(ctx.funcall(), startLoc(ctx));
        String result = visit(ctx.funcall());
        putIns(ctx, ins(ctx.funcall()));
        return result;
    }

    /**
     * Generates code for function call declarations. It first visit the function call and then
     * possibly adds an additional instruction to discard the return value of the function call.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitFuncalldecl(PyVaParser.FuncalldeclContext ctx) {
        Function f = funScope.data(ctx.funcall().ID().getText(), scope.currentScopeLevel());
        putStart(ctx.funcall(), startLoc(ctx));
        String result = visit(ctx.funcall());
        if (!f.getReturnType().equals(VOID)) {
            EReg bin = nextReg();
            freeUpRegister(bin);
            result += pop(bin) + SEP;
            putIns(ctx, 1 + ins(ctx.funcall()));
        } else {
            putIns(ctx, ins(ctx.funcall()));
        }
        return result;
    }

    /**
     * Generates code for function calls.
     * This includes visiting the supplied parameter expression (in reversed order).
     * Then the call is made to the function using {@link CodeGenerator#makeCall(ParseTree, Function)}
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitFuncall(PyVaParser.FuncallContext ctx) {
        putIns(ctx, 0);
        Function f = funScope.data(ctx.ID().getText(), scope.currentScopeLevel());
        StringBuilder result = new StringBuilder("-- function call to '" + f.identifier + "'").append(NL);
        //visiting expr, these calls will push values onto the stack, which are the parameters
        //the values are pushed in reversed order onto the stack, such that the first pop corresponds to the first param of the function
        int start = startLoc(ctx);
        for (int i = ctx.expr().size() - 1; i >= 0; i--) {
            putStart(ctx.expr(i), start);
            result.append(visit(ctx.expr(i)));
            start += ins(ctx.expr(i));
            putIns(ctx, ins(ctx) + ins(ctx.expr(i)));
        }
        result.append(makeCall(ctx, f));
        result.append("-- function call ended for '").append(f.identifier).append("'").append(NL);
        return result.toString();
    }

    /**
     * Opens and closes scopes, does nothing else because there is no need for code generation for type declarations.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitRecorddecl(PyVaParser.RecorddeclContext ctx) {
        scope.openScope();
        // if a pointer x points to the start of an instance of record, then x is the location of the first variable contained in the record
        // you can retrieve this offset from the type of this record
        scope.closeScope();
        putIns(ctx, 0);
        return "";//nothing to do here in terms of code
    }

    /**
     * Generate code for function declarations.
     * This includes a prologue, visiting the functionality of the function and possibly an epilogue.
     * Because a function should not be executed directly one its defined, first a jump is done to jump past the
     * functionality of the function.
     * An epilogue is only added if the function is a void, because void functions do not return.
     * The absolute location of the function is stored internally such that we can find the start of the function
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitFuncdecl(PyVaParser.FuncdeclContext ctx) {
        putIns(ctx, 0);
        Function f = funScope.data(ctx.ID().getText(), scope.currentScopeLevel());
        ScopeLevel currentScopeLevel = scope.currentScopeLevel();
        StringBuilder result = new StringBuilder().append("-- Function '").append(f.identifier).append("' in scope: ").append(currentScopeLevel).append(NL);
        putFunctionLocation(f, startLoc(ctx) + 1);// +1, because JUMP is first, MAKE SURE THAT THIS IS BEFORE OPENING SCOPE
        scope.openScope(); //open scope for the function definition (used for prologue as well)
        int localDataAreaSizeInBytes = varScope.localDataAreaSize(scope.currentScopeLevel());
        String prologue = prologue(ctx, f.getParamTypes().size(), localDataAreaSizeInBytes);
        int prologueSize = ins(ctx);
        putStart(ctx.program(), ins(ctx) + startLoc(ctx) + 1);// +1, because we first have a JUMP
        String functionality = visit(ctx.program());
        int functionSize = ins(ctx.program());
        // whenever the function is a void, it doesnt have an epilogue by default. That's because voids in our language cannot return
        // however, visitReturn is the only way a (non-void) function happens to do the epilogue
        // so we need this check and add it to the end of the function
        String voidEpilogue = f.getReturnType().equals(VOID) ? epilogue(ctx) : "";
        int voidEpilogueSize = f.getReturnType().equals(VOID) ? ins(ctx) - prologueSize : 0;
        // jump over the function, since we do not want to execute it unless its called
        result.append(jump(ETarget.REL, prologueSize + functionSize + voidEpilogueSize + 1)).append(SEP)
                .append(prologue) // prologue first, then function execution
                .append(functionality)
                .append(voidEpilogue);
        result.append("-- End of function '").append(f.identifier).append("' in scope: ").append(currentScopeLevel).append(NL);
        scope.closeScope();
        putIns(ctx, voidEpilogueSize + prologueSize + functionSize + 1);
        return result.toString();
    }

    /**
     * Generates code for return statements.
     * This includes visiting the expression to the right of the return statement and an epilogue.
     * As well as possibly multiple epilogueScopes, depending on whether we are inside a block (because you can return in if statements for example)
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitReturndecl(PyVaParser.ReturndeclContext ctx) {
        putIns(ctx, 0);
        putStart(ctx.expr(), startLoc(ctx));
        String expr = visit(ctx.expr());
        StringBuilder epilogueScopes = new StringBuilder();
        for (int i = 0; i < inBlock; i++) {
            epilogueScopes.append(epilogueScope(ctx));
        }
        putIns(ctx, ins(ctx) + ins(ctx.expr()));
        // epilogueScopes must go first
        return "--return " + ctx.getText() + NL + expr + epilogueScopes.toString() + epilogue(ctx) + "-- end return\n";
    }

    /**
     * Generates code for prefix operations. Such as minus (-) and not (!)
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitPrfExpr(PyVaParser.PrfExprContext ctx) {
        putStart(ctx.expr(), startLoc(ctx));
        StringBuilder result = new StringBuilder(visit(ctx.expr()));

        EReg reg1 = nextReg();
        EReg reg2 = nextReg();
        freeUpRegister(reg1);
        freeUpRegister(reg2);

        result.append(pop(reg1)).append(SEP);
        int ins = ins(ctx.expr()) + 1;

        if (ctx.prfOp().getText().equals("!")) {
            result.append(load(EAddrImmDI.IMMVALUE, 1, reg2)).append(SEP);
            result.append(COMPUTE).append(XOR).append(reg1).append(reg2).append(reg1).append(SEP);
        } else {
            result.append(load(EAddrImmDI.IMMVALUE, -1, reg2)).append(SEP);
            result.append(COMPUTE).append(MUL).append(reg1).append(reg2).append(reg1).append(SEP);
        }
        result.append(PUSH).append(reg1).append(SEP);

        ins += 3;
        putIns(ctx, ins);
        return result.toString();
    }

    /**
     * Generates code for expressions between parentheses.
     * This only includes visiting the expression inside the parentheses.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitParExpr(PyVaParser.ParExprContext ctx) {
        putStart(ctx.expr(), startLoc(ctx));
        String result = visit(ctx.expr());
        putIns(ctx, ins(ctx.expr()));
        return result;
    }

    /**
     * Generates code for visiting variables as expressions.
     * This includes first visiting idCall. If the variable we are calling is threaded, we do not have
     * to do any additional things. Else we have to load the value pointed to by the result of idCall.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitIdExpr(PyVaParser.IdExprContext ctx) {
        putStart(ctx.idCall(), startLoc(ctx));
        Type type = exprTypes.get(ctx);
        StringBuilder result = new StringBuilder("-- visitIdExpr with type: ").append(type).append(NL);
        result.append(visit(ctx.idCall()));
        EReg pointer = nextReg();
        freeUpRegister(pointer);
        ScopeLevel scopeLevel = varScope.scopeLevel(ctx.idCall().ID().getText(), scope.currentScopeLevel());
        boolean threaded = inThreads > 0 || sharedVars.containsKey(scopeLevel.toString()) && sharedVars.get(scopeLevel.toString()).containsKey(ctx.idCall().ID().getText());
        if (threaded) {
            putIns(ctx, ins(ctx.idCall()));
        } else {
            // idCall produced a pointer towards the actual value, now extract the value and push that
            result.append(pop(pointer)).append(SEP);
            result.append(load(EAddrImmDI.INDADDR, pointer, pointer)).append(SEP);
            result.append(push(pointer)).append(DELIM);
            putIns(ctx, ins(ctx.idCall()) + 3);
        }
        result.append("-- end of visitIdExpr with type: ").append(type).append(NL);
        return result.toString();
    }

    /**
     * Generates code for visiting variables.
     * A distinction is made between the main thread and child threads.
     * The generated code includes using access links to find the variable in enclosing scopes.
     * Also all access contexts will be visited. Like array indices and record variables.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitIdCall(PyVaParser.IdCallContext ctx) {
        StringBuilder result = new StringBuilder("-- idCall to " + ctx.getText()).append(NL);
        int ins = 0;
        ScopeLevel scopeLevel = varScope.scopeLevel(ctx.ID().getText(), scope.currentScopeLevel());
        Type type = varScope.type(ctx.ID().getText(), scope.currentScopeLevel());// the type we are about to access
        boolean shared = sharedVars.containsKey(scopeLevel.toString()) && sharedVars.get(scopeLevel.toString()).containsKey(ctx.ID().getText());
        EReg reg1 = nextReg();
        EReg reg2 = nextReg();
        // If the main thread executes...
        if (inThreads == 0) {
            result.append(compute(Op.ADD, EReg.ARP, EReg.ZERO, reg1)).append(SEP);
            result.append(load(EAddrImmDI.IMMVALUE, 1, reg2)).append(SEP);
            ins += 2;
            for (int i = scopeLevel.getDepths().length; i < scope.currentScopeLevel().getDepths().length; i++) {
                result.append(compute(Op.SUB, reg1, reg2, reg1)).append(SEP);
                result.append(load(EAddrImmDI.INDADDR, reg1, reg1)).append(SEP);
                ins += 2;
            }
            result.append(load(EAddrImmDI.IMMVALUE, varScope.offset(ctx.ID().getText(), scopeLevel) / 4, reg2)).append(SEP);
            result.append(compute(Op.ADD, reg1, reg2, reg1)).append(SEP); // reg1 has address of id in heap
            ins += 2;
            // If the variable is a shared variable, get the variable value that's in the shared memory,
            // And update the variable value that's in the heap with it. This improves consistency.
            if (shared) {
                result.append(READINSTR + "(" + DIRADDR).append(sharedVars.get(scopeLevel.toString()).get(ctx.ID().getText())).append(")").append(SEP);
                result.append(RECEIVE).append(reg2).append(SEP);
                result.append(store(reg2, EAddrImmDI.INDADDR, reg1)).append(SEP);
                result.append(load(EAddrImmDI.INDADDR, reg1, reg1)).append(SEP);
                ins += 4;
            }
            result.append(push(reg1)).append(DELIM);
            ins += 1;
            // free some registers before visiting accesser
            freeUpRegister(reg1);
            freeUpRegister(reg2);
            putAccessType(ctx, type);// put the access type, relevant for visitAccesser
            for (PyVaParser.AccesserContext accesser : ctx.accesser()) {
                putStart(accesser, startLoc(ctx) + ins);
                result.append(visit(accesser));
                ins += ins(accesser);
            }
        } else {// If a child thread executes...
            result.append(READINSTR + "(" + DIRADDR).append(sharedVars.get(scopeLevel.toString()).get(ctx.ID().getText())).append(")").append(SEP);
            result.append(RECEIVE).append(reg2).append(SEP);
            result.append(push(reg2)).append(SEP);
            ins += 3;
            freeUpRegister(reg1);
            freeUpRegister(reg2);
        }
        result.append("-- end idCall to ").append(ctx.getText()).append(NL);
        putIns(ctx, ins);
        return result.toString();
    }

    /**
     * Generates code for accessing elements of arrays or variables inside records.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitAccesser(PyVaParser.AccesserContext ctx) {
        StringBuilder result = new StringBuilder("-- accesser: ").append(ctx.getText()).append(NL);
        int ins = 0;
        EReg val = nextReg();
        Type type = accessType(ctx.getParent());// type of the object currently pointed to by val
        if (ctx.ID() != null) {
            //accessField
            EReg offset = nextReg();
            freeUpRegister(offset);
            result.append(pop(val)).append(SEP);                            // pop pointer into val
            result.append(load(EAddrImmDI.INDADDR, val, val)).append(SEP);
            // get the offset of the variable to access from the type
            result.append(load(EAddrImmDI.IMMVALUE, type.getVariableOffset(ctx.ID().getText()) / 4, offset)).append(SEP);
            Type accessedType = type.getVariable(ctx.ID().getText());
            result.append(compute(ADD, offset, val, val)).append(SEP);  // add offset to val
            result.append(push(val)).append(SEP);                       // push the pointer
            ins += 5;
            putAccessType(ctx.getParent(), accessedType);               // update access type
        } else {
            //elementReference
            EReg index = nextReg();
            for (int i = 0; i < ctx.expr().size(); i++) {
                PyVaParser.ExprContext expr = ctx.expr(i);
                putStart(expr, ins + startLoc(ctx));
                result.append(visit(expr));                                     // visit expr
                result.append(pop(index)).append(SEP);                          // pop result into index (should be a concrete integer)
                result.append(pop(val)).append(SEP);                            // pop pointer to array location in AR into val
                result.append(load(EAddrImmDI.INDADDR, val, val)).append(SEP);  // load the start of the array into val
                result.append(compute(ADD, val, index, val)).append(SEP);       // val <- array offset + index
                result.append(push(val)).append(SEP);                           // push array + offset
                ins += 5 + ins(expr);
                type = type.innerType;                                          // update access type
            }
            putAccessType(ctx.getParent(), type);
            freeUpRegister(index);
        }
        result.append("-- end accesser: ").append(ctx.getText()).append(NL);
        freeUpRegister(val);
        putIns(ctx, ins);
        return result.toString();
    }

    /**
     * Generates code for multiplication and division.
     * If the operation is multiplication, the Sprockell compute multiplication is generated.
     * Otherwise soft division is used which is implemented using branching.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitMultExpr(PyVaParser.MultExprContext ctx) {
        Op op = ctx.multOp().getText().equals("*") ? Op.MUL : Op.DIV;
        int start = startLoc(ctx);

        putStart(ctx.expr(0), start);
        String expr0 = visit(ctx.expr(0));
        start += ins(ctx.expr(0));

        putStart(ctx.expr(1), start);
        String expr1 = visit(ctx.expr(1));
        start += ins(ctx.expr(1));
        if (op == Op.MUL) {
            putIns(ctx, ins(ctx.expr(0)) + ins(ctx.expr(1)));
            return compute(ctx, expr0, expr1, op);
        } else {
            StringBuilder result = new StringBuilder(expr0 + expr1);
            EReg regA = nextReg();
            EReg regB = nextReg();
            EReg regC = nextReg();
            EReg regD = nextReg();
            result.append(pop(regB)).append(SEP)
                    .append(pop(regA)).append(SEP)
                    .append(load(EAddrImmDI.IMMVALUE, 0, regD)).append(SEP)
                    .append(compute(Op.LT, regA, regB, regC)).append(SEP)
                    .append(branch(regC, ETarget.REL, 5)).append(SEP)
                    .append(compute(Op.SUB, regA, regB, regA)).append(SEP)
                    .append(load(EAddrImmDI.IMMVALUE, 1, regC)).append(SEP)
                    .append(compute(Op.ADD, regD, regC, regD)).append(SEP)
                    .append(jump(ETarget.REL, -5)).append(SEP)
                    .append(push(regD)).append(DELIM);
            freeUpRegister(regA);
            freeUpRegister(regB);
            freeUpRegister(regC);
            freeUpRegister(regD);
            putIns(ctx, ins(ctx.expr(0)) + ins(ctx.expr(1)) + 10);
            return result.toString();
        }
    }

    /**
     * Generates code for visiting boolean expressions.
     * This includes visiting left and right hand side, then use the
     * results of these expressions in an AND or OR operation.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitBoolExpr(PyVaParser.BoolExprContext ctx) {
        Op op = ctx.boolOp().getText().equals("&&") ? Op.AND : Op.OR;
        int start = startLoc(ctx);

        putStart(ctx.expr(0), start);
        String expr0 = visit(ctx.expr(0));
        start += ins(ctx.expr(0));

        putStart(ctx.expr(1), start);
        String expr1 = visit(ctx.expr(1));
        putIns(ctx, ins(ctx.expr(0)) + ins(ctx.expr(1)));
        start += ins(ctx.expr(1));

        putIns(ctx, ins(ctx.expr(0)) + ins(ctx.expr(1)));
        return compute(ctx, expr0, expr1, op);
    }

    /**
     * Generates code for comparison.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitCompExpr(PyVaParser.CompExprContext ctx) {
        Op op;
        if (ctx.compOp().LE() != null) {
            op = Op.LTE;
        } else if (ctx.compOp().LT() != null) {
            op = Op.LT;
        } else if (ctx.compOp().GE() != null) {
            op = Op.GTE;
        } else if (ctx.compOp().GT() != null) {
            op = Op.GT;
        } else if (ctx.compOp().EQ() != null) {
            op = EQUAL;
        } else {
            op = Op.NEQ;
        }
        int start = startLoc(ctx);

        putStart(ctx.expr(0), start);
        String expr0 = visit(ctx.expr(0));
        start += ins(ctx.expr(0));

        putStart(ctx.expr(1), start);
        String expr1 = visit(ctx.expr(1));
        start += ins(ctx.expr(1));

        putIns(ctx, ins(ctx.expr(0)) + ins(ctx.expr(1)));
        return compute(ctx, expr0, expr1, op);
    }

    /**
     * Generates code for sum expressions.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitPlusExpr(PyVaParser.PlusExprContext ctx) {
        Op op = ctx.plusOp().getText().equals("+") ? Op.ADD : Op.SUB;
        int start = startLoc(ctx);

        putStart(ctx.expr(0), start);
        String expr0 = visit(ctx.expr(0));
        start += ins(ctx.expr(0));

        putStart(ctx.expr(1), start);
        String expr1 = visit(ctx.expr(1));
        start += ins(ctx.expr(1));

        putIns(ctx, ins(ctx.expr(0)) + ins(ctx.expr(1)));
        return compute(ctx, expr0, expr1, op);
    }

    /**
     * Generates code for number literals.
     * This includes pushing the number onto the stack.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitNumExpr(PyVaParser.NumExprContext ctx) {
        EReg reg = nextReg();
        freeUpRegister(reg);
        putIns(ctx, 2);
        return load(EAddrImmDI.IMMVALUE, Integer.parseInt(ctx.getText()), reg) + SEP + push(reg) + DELIM;
    }

    /**
     * Generates code for char literals.
     * This includes converting the char to an integer and pushing the result onto the stack.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitCharExpr(PyVaParser.CharExprContext ctx) {
        EReg reg = nextReg();
        int c = ctx.CHR().getText().charAt(1);
        freeUpRegister(reg);
        putIns(ctx, 2);
        return load(EAddrImmDI.IMMVALUE, c, reg) + SEP + push(reg) + DELIM;
    }

    /**
     * Generates code to push boolean true onto the stack
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitTrueExpr(PyVaParser.TrueExprContext ctx) {
        EReg reg = nextReg();
        freeUpRegister(reg);
        putIns(ctx, 2);
        return load(EAddrImmDI.IMMVALUE, 1, reg) + SEP + push(reg) + DELIM;
    }

    /**
     * Generates code to push boolean false onto the stack
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitFalseExpr(PyVaParser.FalseExprContext ctx) {
        EReg reg = nextReg();
        freeUpRegister(reg);
        putIns(ctx, 2);
        return load(EAddrImmDI.IMMVALUE, 0, reg) + SEP + push(reg) + DELIM;
    }

    /**
     * Generates code to create a string, which is equal to the char[] type.
     * This includes allocating memory based off the size of the string, and then loading
     * the characters one by one into the array.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitStringExpr(PyVaParser.StringExprContext ctx) {
        putIns(ctx, 0);

        String s = ctx.getText();
        s = s.substring(1, s.length() - 1); // removes ' " '
        EReg val = nextReg();
        EReg array = nextReg();
        freeUpRegister(val);
        freeUpRegister(array);

        StringBuilder result = new StringBuilder("-- creation of string '").append(s).append("'").append(NL);
        result.append(compute(ADD, MEM, ZERO, array)).append(SEP);                  // store starting address in array
        result.append(push(array)).append(SEP);                                     // push the array onto the stack
        result.append(allocate(ctx, 4 * s.length()));                         // reserve memory for array
        for (int i = 0; i < s.length(); i++) {
            result.append(load(EAddrImmDI.IMMVALUE, s.charAt(i), val)).append(SEP); // load char into val
            result.append(store(val, EAddrImmDI.INDADDR, array)).append(SEP);       // store val in memory pointed to by array
            result.append(compute(INCR, array, ZERO, array)).append(SEP);           // let array point to next index
        }
        result.append("-- end of string creation").append(NL);
        putIns(ctx, ins(ctx) + s.length() * 3 + 2);
        return result.toString();
    }

    /**
     * Generates code to push zero onto the stack (null pointer)
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitNullExpr(PyVaParser.NullExprContext ctx) {
        putIns(ctx, 1);
        return push(ZERO) + SEP;// just push zero onto the stack
    }

    /**
     * Generates code to create a new record.
     * This includes pushing the pointer to the start of this new record onto the stack and
     * allocating memory based off the size of the type.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitRecordExpr(PyVaParser.RecordExprContext ctx) {
        Type type = Type.tryType(ctx.ID().getText(), typeScope, scope.currentScopeLevel());
        putIns(ctx, 1);
        // push the start point of the record onto the stack, then allocate the size needed for the type
        return push(MEM) + SEP + allocate(ctx, type.heapSize());
    }

    /**
     * Generates code to create an array.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitArrayInitExpr(PyVaParser.ArrayInitExprContext ctx) {
        putStart(ctx.arrayInit(), startLoc(ctx));
        String res = "--array creation of: " + ctx.getText() + NL + visit(ctx.arrayInit()) + "--end of array creation" + NL;
        putIns(ctx, ins(ctx.arrayInit()));
        return res;
    }

    /**
     * Generates code to create an array, initialized by values.
     * This includes first visiting all the expressions the array may contain.
     * Then allocating memory for the resulting array and loading in the values from the expressions.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitValuedArrayInit(PyVaParser.ValuedArrayInitContext ctx) {
        putIns(ctx, 0);
        putStart(ctx.arrayValue(), startLoc(ctx));
        StringBuilder result = new StringBuilder();

        if (ctx.arrayValue() != null) {
            result.append(visit(ctx.arrayValue())); // first create the inner values of the array
            int size = ctx.arrayValue().expr().size();
            EReg val = nextReg();
            EReg array = nextReg();
            EReg arrayStart = nextReg();
            freeUpRegister(val);
            freeUpRegister(array);
            freeUpRegister(arrayStart);
            result.append(compute(ADD, MEM, ZERO, array)).append(SEP);              // load start address (first free pointer in memory)
            result.append(compute(ADD, MEM, ZERO, arrayStart)).append(SEP);         // load start address in arrayStart, will not be changed after this but needed for pushing (cant push now because we need to pop first)
            result.append(allocate(ctx, 4 * size));                           // allocate memory for array
            for (int i = 0; i < size; i++) {
                // array values are pushed in reversed order, therefore the first pop is the first element of the array
                result.append(pop(val)).append(SEP);                                // get value
                result.append(store(val, EAddrImmDI.INDADDR, array)).append(SEP);   // store val into array
                result.append(compute(INCR, array, ZERO, array)).append(SEP);       // next array index
            }
            result.append(push(arrayStart)).append(DELIM);                          // push the start of the array onto the stack
            putIns(ctx, 3 + 3 * size + ins(ctx) + ins(ctx.arrayValue()));
        } else {
            result.append(push(MEM)).append(DELIM);                                 // array had size zero, just push memory. User should know the size is zero
            putIns(ctx, 1 + ins(ctx) + ins(ctx.arrayValue()));
        }
        return result.toString();
    }

    /**
     * Generates code for an array value.
     * This includes pushing the contents in reversed order onto the stack.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitArrayValue(PyVaParser.ArrayValueContext ctx) {
        putIns(ctx, 0);
        int start = startLoc(ctx);
        StringBuilder result = new StringBuilder();
        for (int i = ctx.expr().size() - 1; i >= 0; i--) {
            // visit expr in reversed order
            PyVaParser.ExprContext expr = ctx.expr(i);
            putStart(expr, start);
            result.append(visit(expr));
            start += ins(expr);
            putIns(ctx, ins(ctx) + ins(expr));
        }
        return result.toString();
    }

    /**
     * Generates code for creating arrays by their size.
     * This includes first visiting all sizes in reverse order.
     * Then creating the array. Because inner arrays depend on the size of the parent array
     * 6 registers are needed to keep track of the parent array and its inner array. Branching
     * is used to iteratively add arrays.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitSizedArrayInit(PyVaParser.SizedArrayInitContext ctx) {
        EReg array = nextReg();
        int start = startLoc(ctx) + 2;
        putIns(ctx, 2);
        StringBuilder result = new StringBuilder(compute(ADD, MEM, ZERO, array)).append(SEP);
        result.append(push(array)).append(SEP); //push the start of the array onto the stack (for whoever called this creation of the array)

        // first visit all expressions, in reversed order
        for (int i = ctx.expr().size() - 1; i >= 0; i--) {
            PyVaParser.ExprContext expr = ctx.expr(i);
            putStart(expr, start);
            result.append(visit(expr));
            start += ins(expr);
            putIns(ctx, ins(ctx) + ins(expr));
        }

        EReg size = nextReg();
        EReg array2 = nextReg();
        EReg size2 = nextReg();
        EReg comparison = nextReg();
        EReg index = nextReg();

        if (ctx.expr().size() > 1) {
            // new int[x][y]...[]
            EReg multSize = nextReg();

            result.append(compute(ADD, MEM, ZERO, array)).append(SEP);      // array now points to the start of the array
            result.append(pop(size)).append(SEP);                           // pop x from stack
            result.append(allocateFromReg(size)).append(SEP);               // allocate size of x
            result.append(pop(size2)).append(SEP);                          // pop y from stack
            result.append(load(EAddrImmDI.IMMVALUE, 0, index)).append(SEP);   // index starts at 0
            result.append(compute(EQUAL, ZERO, size, comparison)).append(SEP);      // check if non zero
            result.append(compute(ADD, MEM, ZERO, array2)).append(SEP);             // array2 now contains start address of next array
            result.append(branch(comparison, REL, 7)).append(SEP);            // branch past if equal to zero
            result.append(store(MEM, EAddrImmDI.INDADDR, array)).append(SEP);       // store MEM at ARRAY (store the start of the new array in the actual array)
            result.append(allocateFromReg(size2)).append(SEP);                      // allocate new memory of size y
            result.append(compute(INCR, index, ZERO, index)).append(SEP);           // index += 1
            result.append(compute(Op.LT, index, size, comparison)).append(SEP);     // index < size?
            result.append(compute(INCR, array, ZERO, array)).append(SEP);           // array now points to next element
            result.append(branch(comparison, REL, -5)).append(SEP);           // loop back if index < size
            // otherwise continue here
            result.append(compute(MUL, size, size2, multSize)).append(SEP);         // compute x*y, store in multSize
            // 15 instructions before the coming for-loop
            for (int i = 2; i < ctx.expr().size(); i++) {// start at i=2
                // 14 instructions in for loop
                result.append(compute(ADD, size2, ZERO, size)).append(SEP);             // move size2 to size
                result.append(compute(ADD, array2, ZERO, array)).append(SEP);           // move array2 to array
                result.append(pop(size2)).append(SEP);                                  // pop z into size2
                result.append(load(EAddrImmDI.IMMVALUE, 0, index)).append(SEP);   // index starts at 0
                result.append(compute(EQUAL, ZERO, size, comparison)).append(SEP);      // check if non zero
                result.append(compute(ADD, MEM, ZERO, array2)).append(SEP);             // array2 now contains start address of next array
                result.append(branch(comparison, REL, 7)).append(SEP);            // branch past if equal to zero
                result.append(store(MEM, EAddrImmDI.INDADDR, array)).append(SEP);       // store MEM at ARRAY (store the start of the new array in the actual array)
                result.append(allocateFromReg(size2)).append(SEP);                      // allocate new memory of size z
                result.append(compute(INCR, index, ZERO, index)).append(SEP);           // index += 1
                result.append(compute(Op.LT, index, multSize, comparison)).append(SEP); // index < multSize?
                result.append(compute(INCR, array, ZERO, array)).append(SEP);           // array now points to next element
                result.append(branch(comparison, REL, -5)).append(SEP);           // loop back if index < size
                // otherwise continue here
                result.append(compute(MUL, size, size2, multSize)).append(SEP);         // compute y*z, store in multSize
            }
            freeUpRegister(multSize);
            putIns(ctx, ins(ctx) + 15 + 14 * (ctx.expr().size() - 2));
        } else {
            // new int[x]
            result.append(pop(size)).append(SEP);// just pop this into size register
            result.append(allocateFromReg(size));// allocates the size needed
            putIns(ctx, ins(ctx) + 2);
        }

        freeUpRegister(array);
        freeUpRegister(size);
        freeUpRegister(array2);
        freeUpRegister(size2);
        freeUpRegister(comparison);
        freeUpRegister(index);
        return result.toString();
    }

    /**
     * Generates code for the full program.
     * This includes first allocating local data for main, as well as setting null pointer.
     * Then it visits all declarations one by one.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    @Override
    public String visitGoal(PyVaParser.GoalContext ctx) {
        putIns(ctx, 0);
        EReg reg = nextReg();
        freeUpRegister(reg);
        StringBuilder result = new StringBuilder();
        // allocate 12 + local size of MAIN, we need the 12 because of AR of main (link, caller's arp and local data area)
        // and the first value in the heap is the null pointer!
        result.append(load(EAddrImmDI.IMMVALUE, 1, reg)).append(SEP);
        result.append(compute(ADD, reg, ZERO, ARP)).append(SEP);// let ARP point to 1: The CARP (which is always zero) of main
        result.append(allocate(ctx, 12 + varScope.localDataAreaSize(scope.currentScopeLevel())));
        int ins = 2;
        int start = 2 + startLoc(ctx) + ins(ctx);
        for (PyVaParser.DeclContext decl : ctx.decl()) {
            putStart(decl, start);
            result.append(visit(decl));
            start += ins(decl);
            ins += ins(decl);
        }
        putIns(ctx, ins + ins(ctx));
        return result.toString();
    }

    /**
     * Generates code to do a preScope.
     * This is the same idea as with preCall, the only difference is that access link is always the caller's ARP.
     * Also it does not have to store the return address.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    private String preScope(ParseTree ctx) {
        StringBuilder result = new StringBuilder("-- preScope").append(NL);
        result.append(store(EReg.ARP, EAddrImmDI.INDADDR, EReg.MEM)).append(SEP)    // store link (which is always CARP when using preScope) at MEM
                .append(compute(INCR, EReg.MEM, ZERO, EReg.MEM)).append(SEP)        // MEM = MEM + 1
                .append(store(EReg.ARP, EAddrImmDI.INDADDR, EReg.MEM)).append(SEP)  // store CARP (current ARP) at MEM
                .append(compute(Op.ADD, EReg.MEM, EReg.ZERO, EReg.ARP)).append(SEP) // ARP <- MEM
                .append(compute(INCR, MEM, ZERO, MEM)).append(SEP);                 // MEM = MEM + 1
        result.append("-- end of preScope").append(NL);
        putIns(ctx, ins(ctx) + 5);
        return result.toString();
    }

    /**
     * Generates code to do a prologueScope.
     * This only includes allocating size for local data area.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    private String prologueScope(ParseTree ctx) {
        int byteSize = varScope.localDataAreaSize(scope.currentScopeLevel());
        StringBuilder result = new StringBuilder();
        result.append("-- prologueScope...").append(NL);
        result.append(allocate(ctx, byteSize));// reserve memory
        result.append("-- end prologueScope...").append(NL);
        return result.toString();
    }

    /**
     * Generates code to do an epilogueScope.
     * This only includes updating regARP with caller's ARP.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    private String epilogueScope(ParseTree ctx) {
        EReg carp = nextReg();
        freeUpRegister(carp);
        StringBuilder result = new StringBuilder("-- epilogueScope").append(NL);
        result.append(load(EAddrImmDI.INDADDR, EReg.ARP, carp)).append(SEP); //load CARP into carp
        result.append(compute(Op.ADD, carp, EReg.ZERO, EReg.ARP)).append(SEP);// update ARP with newARP
        result.append("-- end of epilogueScope").append(NL);
        putIns(ctx, ins(ctx) + 2);
        return result.toString();
    }

    /**
     * Generates code for calling the supplied function f.
     * This includes a preCall and a jump towards the location of the function.
     * Because functions can be used before they are defined in the current scope, a distinction
     * is made between whether the function location is already known or not.
     *
     * @param ctx The context
     * @param f   The function to call
     * @return Executable Sprockell code
     */
    private String makeCall(ParseTree ctx, Function f) {
        int retA = startLoc(ctx) + ins(ctx) + 1;
        putIns(ctx, ins(ctx) + 1);// +1 from the jump instruction
        Integer fLocation = getFunctionLocation(f);
        if (fLocation != null) {
            return preCall(ctx, f, retA) + jump(ETarget.ABS, fLocation) + DELIM;
        } else {
            this.functionFormatList.add(identifier(f));// add to format list
            return preCall(ctx, f, retA) + jumpFormat(ETarget.ABS) + DELIM;
        }
    }

    /**
     * Generates code for a preCall.
     * A preCall includes creating an activation record for the function to call.
     * This activation record will have return address, access link and caller's ARP stored in there.
     * Once the AR is set, regARP will be updated accordingly.
     *
     * @param ctx The context
     * @param f   The function for which we make an activation record
     * @return Executable Sprockell code
     */
    private String preCall(ParseTree ctx, Function f, int retA) {
        ScopeLevel s = funScope.scopeLevel(f.identifier, scope.currentScopeLevel());
        int scopeDifference = scope.currentScopeLevel().getDepths().length - s.getDepths().length;
        EReg vals = nextReg();
        int INS = scopeDifference == 0 ? 8 : 7 + 2 * scopeDifference + 2;             //the amount of instructions this method produces, here for consistency
        StringBuilder result = new StringBuilder("-- precall").append(NL);
        result.append(load(EAddrImmDI.IMMVALUE, retA + INS, vals)).append(SEP); // load retA into vals, add INS to return address, to jump past this precall
        result.append(store(vals, EAddrImmDI.INDADDR, EReg.MEM)).append(SEP);         //store retA into MEM
        result.append(compute(INCR, MEM, ZERO, MEM)).append(SEP);                     // MEM = MEM + 1
        if (scopeDifference == 0) {
            result.append(store(EReg.ARP, EAddrImmDI.INDADDR, EReg.MEM)).append(SEP);// store link (which is CARP) into MEM + 1
        } else {
            result.append(compute(ADD, ARP, ZERO, vals)).append(SEP);               // load ARP into vals
            for (int i = 0; i < scopeDifference; i++) {
                result.append(compute(DECR, vals, ZERO, vals)).append(SEP);         // vals <- ARP - 1, vals now points to CALLER's link
                result.append(load(EAddrImmDI.INDADDR, vals, vals)).append(SEP);    // load actual value of link into vals

            }
            result.append(store(vals, EAddrImmDI.INDADDR, MEM)).append(SEP);        // store link into MEM + 1
        }
        result.append(compute(INCR, MEM, ZERO, MEM)).append(SEP)                    // MEM = MEM + 1
                .append(store(EReg.ARP, EAddrImmDI.INDADDR, EReg.MEM)).append(SEP)  // store carp into MEM + 2, assume EReg.ARP is caller's arp
                .append(compute(Op.ADD, EReg.ZERO, EReg.MEM, EReg.ARP)).append(SEP) // ARP <- MEM, update arp
                .append(compute(INCR, MEM, ZERO, MEM)).append(DELIM);               // MEM = MEM + 1
        result.append("-- end of precall").append(NL);
        freeUpRegister(vals);
        putIns(ctx, INS + ins(ctx));
        return result.toString();
    }

    /**
     * Generates code for a prologue.
     * This includes allocating memory for local data area, as well as loading the parameters into the correct location
     * in local data area.
     *
     * @param ctx                    The context
     * @param numberOfParams         How many parameters this prologue takes.
     * @param localDataAreaSizeBytes How large the local data area of this AR is.
     * @return Executable Sprockell code
     */
    private String prologue(ParseTree ctx, int numberOfParams, int localDataAreaSizeBytes) {
        EReg param = nextReg();
        EReg offset = nextReg();
        freeUpRegister(param);
        freeUpRegister(offset);
        StringBuilder result = new StringBuilder();
        result.append("-- prologue...").append(NL).append(allocate(ctx, localDataAreaSizeBytes)); // reserve memory
        result.append(compute(Op.ADD, EReg.ZERO, EReg.ARP, offset)).append(SEP);
        for (int i = 0; i < numberOfParams; i++) {
            result.append(compute(INCR, offset, ZERO, offset)).append(SEP);         // first param is located at ARP + 1
            result.append(pop(param)).append(SEP);                                  // pop parameter from stack
            result.append(store(param, EAddrImmDI.INDADDR, offset)).append(SEP);    // store param at offset in memory
        }
        result.append("-- end prologue...").append(NL);
        putIns(ctx, 3 * numberOfParams + 1 + ins(ctx));
        return result.toString();
    }

    /**
     * Generates code for an epilogue.
     * This includes updating ARP to caller's AR as well as jumping to return address.
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    private String epilogue(ParseTree ctx) {
        StringBuilder result = new StringBuilder("-- epilogue").append(NL);
        EReg carp = nextReg();
        EReg retA = nextReg();
        freeUpRegister(carp);
        freeUpRegister(retA);
        result.append(load(EAddrImmDI.INDADDR, EReg.ARP, carp)).append(SEP);    //load CARP into carp
        result.append(compute(DECR, EReg.ARP, ZERO, EReg.ARP)).append(SEP);     // subtract 1 from ARP, now ARP points to link
        result.append(compute(DECR, EReg.ARP, ZERO, EReg.ARP)).append(SEP);     //subtract 1 again, now ARP points to return address
        result.append(load(EAddrImmDI.INDADDR, EReg.ARP, retA)).append(SEP);    // load return address into retA
        result.append(compute(Op.ADD, carp, EReg.ZERO, EReg.ARP)).append(SEP);  // update ARP with newARP
        result.append(jump(ETarget.IND, retA)).append("-- end of epilogue").append(DELIM);// jump to retA
        putIns(ctx, ins(ctx) + 6);
        return result.toString();
    }

    private String postCall(ParseTree ctx) {
        //we dont use register saves. So this is probably not needed
        return null;
    }

    /**
     * Returns the resulting code that has been generated.
     * This function first formats the result of the stringBuilder, this is needed because
     * function that have been defined later are now defined but in the stringBuilder result it has not
     * been updated yet.
     *
     * @return Executable Sprockell code
     */
    public String getResult() {
        Object[] functionLocations = new Object[this.functionFormatList.size()];
        for (int i = 0; i < functionLocations.length; i++) {
            functionLocations[i] = this.functionLocation.get(this.functionFormatList.get(i));
        }
        return String.format(result.toString(), functionLocations);
    }

    private void putIns(ParseTree node, int ins) {
        instructions.put(node, ins);
    }

    private int ins(ParseTree node) {
        return instructions.get(node);
    }

    private void putStart(ParseTree node, int start) {
        startLocs.put(node, start);
    }

    private int startLoc(ParseTree node) {
        return startLocs.get(node);
    }

    private void putAccessType(ParseTree node, Type type) {
        this.accessTypes.put(node, type);
    }

    private Type accessType(ParseTree node) {
        return this.accessTypes.get(node);
    }

    /**
     * Returns a unique identifier for a function, based off in which scope the function resides
     *
     * @param f The function
     * @return Unique identifier for the function
     */
    private String identifier(Function f) {
        return f.identifier + funScope.scopeLevel(f.identifier, scope.currentScopeLevel());
    }

    private void putFunctionLocation(Function f, int absoluteLocation) {
        // assumption that scope.currentScopeLevel is the scope this Function f is defined in
        this.functionLocation.put(identifier(f), absoluteLocation);
    }

    private Integer getFunctionLocation(Function f) {
        return this.functionLocation.get(identifier(f));
    }

    /**
     * Allocates memory based off a value that is stored in register
     *
     * @param size The register which contains the size to allocate
     * @return Sprockell instruction
     */
    private String allocateFromReg(EReg size) {
        return compute(ADD, MEM, size, MEM);
    }

    /**
     * Allocates memory based off a value that is currently on the stack
     *
     * @param ctx The context
     * @return Executable Sprockell code
     */
    private String allocateFromStack(ParseTree ctx) {
        putIns(ctx, 2 + ins(ctx));
        EReg size = nextReg();
        freeUpRegister(size);
        return pop(size) + SEP + compute(ADD, MEM, size, MEM) + SEP;
    }

    /**
     * Allocates memory based off the supplied bytes parameter
     *
     * @param ctx   The context
     * @param bytes The size to allocate
     * @return Executable Sprockell code
     */
    private String allocate(ParseTree ctx, int bytes) {
        putIns(ctx, 2 + ins(ctx));
        EReg bytesReg = nextReg();
        freeUpRegister(bytesReg);
        // adds the value of (bytes / 4) to Reg.MEM to reserve memory space
        return load(EAddrImmDI.IMMVALUE, bytes / 4, bytesReg) + SEP + compute(Op.ADD, EReg.MEM, bytesReg, EReg.MEM) + SEP;
    }

    private String compute(ParseTree ctx, ParseTree expr0, ParseTree expr1, Op operator) {
        EReg n1 = nextReg();
        EReg n2 = nextReg();
        freeUpRegister(n1);
        freeUpRegister(n2);
        int start = startLoc(ctx);

        putStart(expr0, start);
        String e0 = visit(expr0);
        start += ins(expr0);

        putStart(expr1, start);
        String e1 = visit(expr1);
        start += ins(expr1);

        putIns(ctx, ins(expr0) + ins(expr1) + 4);
        return e0 + e1 + pop(n2) + SEP + pop(n1) + SEP +
                compute(operator, n1, n2, n1) + SEP + push(n1) + DELIM;
    }

    /**
     * Returns a COMPUTE Sprockell instruction
     *
     * @param op The operator to use
     * @param r1 The register which contains the first expression
     * @param r2 The register which contains the second expression
     * @param r3 The register to store the result in
     * @return Sprockell instruction
     */
    private String compute(Op op, EReg r1, EReg r2, EReg r3) {
        return COMPUTE + op + r1 + r2 + r3;
    }

    /**
     * Returns a POP Sprockell instruction
     *
     * @param target The target register
     * @return Sprockell instruction
     */
    private String pop(EReg target) {
        return POP + target;
    }

    /**
     * Returns a PUSH Sprockell instruction
     *
     * @param from The register which contains the value to push
     * @return Sprockell instruction
     */
    private String push(EReg from) {
        return PUSH + from;
    }

    /**
     * Returns a JUMP Sprockell instruction
     *
     * @param target The target type
     * @param reg    The register to use
     * @return Sprockell instruction
     */
    private String jump(ETarget target, EReg reg) {
        return JUMP + "(" + target + reg + ")";
    }

    /**
     * Returns a JUMP Sprockell instruction
     *
     * @param target The target type
     * @param value  The value to jump to
     * @return Sprockell instruction
     */
    private String jump(ETarget target, int value) {
        return JUMP + "(" + target + "(" + value + "))";
    }

    /**
     * Returns a JUMP Sprockell instruction.
     * Does not take any arguments other than the target, instead it creates an instruction that can be reformatted.
     *
     * @param target The target type
     * @return Sprockell instruction
     */
    private String jumpFormat(ETarget target) {
        return JUMP + "(" + target + "(%d))";
    }

    /**
     * Returns a BRANCH SProckell instruction. If condition then jump to target
     *
     * @param condition The register that contains the condition
     * @param target    The target type
     * @param value     The value of the jump
     * @return Sprockell instruction
     */
    private String branch(EReg condition, ETarget target, int value) {
        return BRANCH + condition + "(" + target + "(" + value + "))";
    }

    /**
     * Returns a BRANCH SProckell instruction. If condition then jump to target
     *
     * @param condition The register that contains the condition
     * @param target    The target type
     * @param reg       The register to use the value of
     * @return Sprockell instruction
     */
    private String branch(EReg condition, ETarget target, EReg reg) {
        return BRANCH + condition + "(" + target + reg + ")";
    }

    /**
     * Returns a LOAD Sprockell instruction
     *
     * @param addrImmDI Addressing type
     * @param value     The value to load
     * @param into      The target register
     * @return Sprockell instruction
     */
    private String load(EAddrImmDI addrImmDI, int value, EReg into) {
        return LOAD + "(" + addrImmDI + "(" + value + ")) " + into;
    }

    /**
     * Returns a LOAD Sprockell instruction.
     *
     * @param addrImmDI Addressing type
     * @param reg       The register to take the value of
     * @param into      The target register
     * @return Sprockell instruction
     */
    private String load(EAddrImmDI addrImmDI, EReg reg, EReg into) {
        return LOAD + "(" + addrImmDI + reg + ") " + into;
    }

    /**
     * Returns a STORE Sprockell instruction.
     *
     * @param from      The register that contains the value to store
     * @param addrImmDI Addressing type
     * @param reg       Register that contains the target address
     * @return Sprockell instruction
     */
    private String store(EReg from, EAddrImmDI addrImmDI, EReg reg) {
        return STORE + from + "(" + addrImmDI + reg + ")";
    }

    /**
     * Returns a STORE Sprockell instruction.
     *
     * @param from      The register that contains the value to store
     * @param addrImmDI Addressing type
     * @param value     The location in memory to store the value in
     * @return Sprockell instruction
     */
    private String store(EReg from, EAddrImmDI addrImmDI, int value) {
        return STORE + from + "(" + addrImmDI + "(" + value + "))";
    }

    /**
     * Generates code for computations.
     * This includes popping two values from the stack, then compute the result and push the result.
     *
     * @param ctx   The context
     * @param expr0 Exectuable Sprockell code for the first expression
     * @param expr1 Executable Sprockell code for the seocnd expression
     * @param op    The operation to use
     * @return Executable Sprockell code
     */
    private String compute(ParseTree ctx, String expr0, String expr1, Op op) {
        EReg next1 = nextReg();
        EReg next2 = nextReg();
        freeUpRegister(next1);
        freeUpRegister(next2);
        putIns(ctx, ins(ctx) + 4);
        return expr0 + expr1 + pop(next2) + SEP + pop(next1) + SEP + compute(op, next1, next2, next1) + SEP + push(next1) + DELIM;
    }

    /**
     * Used to free a register, making it available for other methods.
     *
     * @param reg The register to free
     */
    private void freeUpRegister(EReg reg) {
        EReg.FREE_REGISTERS.add(reg);
    }

    /**
     * Used to allocate a register, making it unavailable for other methods.
     * May cause System failure when called when there are no registers free
     *
     * @return The register to use
     */
    private EReg nextReg() {
        return EReg.FREE_REGISTERS.remove(0);
    }
}
