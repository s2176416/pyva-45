module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regA , Compute Equal regA reg0 regB , Branch regB (Rel (-3)), Jump (Ind regA ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regC , Compute Add regC reg0 regARP , Load (ImmValue (7)) regD , Compute Add regMEM regD regMEM , -- Function 'bubbleSort' in scope: [0]
	Jump (Rel (377)), -- prologue...
	Load (ImmValue (3)) regG , Compute Add regMEM regG regMEM , Compute Add reg0 regARP regF , Compute Incr regF reg0 regF , Pop regE , Store regE (IndAddr regF ), Compute Incr regF reg0 regF , Pop regE , Store regE (IndAddr regF ), -- end prologue...
	Load (ImmValue (1)) regA , Push regA 
	, Pop regB , Load (ImmValue (3)) regC , Compute Add regARP regC regC , Store regB (IndAddr regC )
	, -- visitIdExpr with type: boolean
	-- idCall to swapped
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regG , Load (ImmValue (3)) regG , Compute Add regF regG regF , Push regF 
	, -- end idCall to swapped
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: boolean
	Pop regD , Load (ImmValue (1)) regE , Compute Xor regD regE regD , Branch regD (Rel (344))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regB , Compute Add regMEM regB regMEM , -- end prologueScope...
	Load (ImmValue (0)) regC , Push regC 
	, Pop regD , -- idCall to swapped
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regA , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Load (ImmValue (3)) regA , Compute Add regG regA regG , Push regG 
	, -- end idCall to swapped
	Pop regE , Store regD (IndAddr regE ), -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	Load (ImmValue (0)) regA , Push regA 
	, Pop regD , Load (ImmValue (1)) regE , Compute Add regARP regE regE , Store regD (IndAddr regE )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regF regB regF , Push regF 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regA , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Load (ImmValue (2)) regA , Compute Add regG regA regG , Push regG 
	, -- end idCall to n
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regE , Compute Lt regE regF regE , Push regE 
	, Pop regB , Load (ImmValue (1)) regC , Compute Xor regB regC regB , Branch regB (Rel (253))
	, -- visitIdExpr with type: int
	-- idCall to A[i-1]
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regA , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regA , Compute Add regG regA regG , Push regG 
	, -- accesser: [i-1]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regF regB regF , Push regF 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regG , Push regG 
	, Pop regF , Pop regA , Compute Sub regA regF regA , Push regA 
	, Pop regE , Pop regD , Load (IndAddr regD ) regD , Compute Add regD regE regD , Push regD , -- end accesser: [i-1]
	-- end idCall to A[i-1]
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to A[i]
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regG , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Load (ImmValue (1)) regG , Compute Add regC regG regC , Push regC 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regE regD regE , Push regE 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regA , Load (IndAddr regA ) regA , Compute Add regA regF regA , Push regA , -- end accesser: [i]
	-- end idCall to A[i]
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regE , Pop regG , Compute Gt regG regE regG , Push regG 
	, Pop regB , Load (ImmValue (1)) regC , Compute Xor regB regC regB , Branch regB (Rel (166))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regD , Compute Add regMEM regD regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to A[i]
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regF , Compute Sub regB regF regB , Load (IndAddr regB ) regB , Compute Sub regB regF regB , Load (IndAddr regB ) regB , Compute Sub regB regF regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regF , Compute Add regB regF regB , Push regB 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regE , Compute Sub regG regE regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regE , Compute Add regG regE regG , Push regG 
	, -- end idCall to i
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Pop regC , Pop regA , Load (IndAddr regA ) regA , Compute Add regA regC regA , Push regA , -- end accesser: [i]
	-- end idCall to A[i]
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regF , Load (ImmValue (1)) regG , Compute Add regARP regG regG , Store regF (IndAddr regG )
	, -- visitIdExpr with type: int
	-- idCall to A[i-1]
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Compute Sub regE regD regE , Load (IndAddr regE ) regE , Compute Sub regE regD regE , Load (IndAddr regE ) regE , Compute Sub regE regD regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regD , Compute Add regE regD regE , Push regE 
	, -- accesser: [i-1]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regF , Compute Sub regB regF regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regF , Compute Add regB regF regB , Push regB 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regE , Push regE 
	, Pop regB , Pop regD , Compute Sub regD regB regD , Push regD 
	, Pop regA , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regA regC , Push regC , -- end accesser: [i-1]
	-- end idCall to A[i-1]
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regG , -- idCall to A[i]
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regA , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regA , Compute Add regB regA regB , Push regB 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regA , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regA , Compute Add regB regA regB , Push regB 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regF regC , Push regC , -- end accesser: [i]
	-- end idCall to A[i]
	Pop regE , Store regG (IndAddr regE ), -- visitIdExpr with type: int
	-- idCall to temp
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regA regB regA , Push regA 
	, -- end idCall to temp
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regC , -- idCall to A[i-1]
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regA , Compute Sub regD regA regD , Load (IndAddr regD ) regD , Compute Sub regD regA regD , Load (IndAddr regD ) regD , Compute Sub regD regA regD , Load (IndAddr regD ) regD , Load (ImmValue (1)) regA , Compute Add regD regA regD , Push regD 
	, -- accesser: [i-1]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regA , Compute Sub regD regA regD , Load (IndAddr regD ) regD , Load (ImmValue (1)) regA , Compute Add regD regA regD , Push regD 
	, -- end idCall to i
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regA , Push regA 
	, Pop regA , Pop regD , Compute Sub regD regA regD , Push regD 
	, Pop regF , Pop regB , Load (IndAddr regB ) regB , Compute Add regB regF regB , Push regB , -- end accesser: [i-1]
	-- end idCall to A[i-1]
	Pop regG , Store regC (IndAddr regG ), Load (ImmValue (1)) regD , Push regD 
	, Pop regA , -- idCall to swapped
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regG , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Load (ImmValue (3)) regG , Compute Add regC regG regC , Push regC 
	, -- end idCall to swapped
	Pop regF , Store regA (IndAddr regF ), -- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regD regC regD , Push regD 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regA , Push regA 
	, Pop regB , Pop regF , Compute Add regF regB regF , Push regF 
	, Pop regE , -- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regG regA regG , Push regG 
	, -- end idCall to i
	Pop regD , Store regE (IndAddr regD ), Jump (Rel (-279))
	, -- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regG , Compute Sub regB regG regB , Load (IndAddr regB ) regB , Load (ImmValue (2)) regG , Compute Add regB regG regB , Push regB 
	, -- end idCall to n
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regE , Push regE 
	, Pop regC , Pop regD , Compute Sub regD regC regD , Push regD 
	, Pop regF , -- idCall to n
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regE , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Load (ImmValue (2)) regE , Compute Add regA regE regA , Push regA 
	, -- end idCall to n
	Pop regB , Store regF (IndAddr regB ), -- epilogueScope
	Load (IndAddr regARP ) regD , Compute Add regD reg0 regARP , -- end of epilogueScope
	Jump (Rel (-354))
	, -- epilogue
	Load (IndAddr regARP ) regC , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regA , Compute Add regC reg0 regARP , Jump (Ind regA )-- end of epilogue
	, -- End of function 'bubbleSort' in scope: [0]
	Load (ImmValue (7)) regE , Push regE 
	, Pop regF , Load (ImmValue (1)) regB , Compute Add regARP regB regB , Store regF (IndAddr regB )
	, --array creation of: [4,8,42,1,2,3,69]
	Load (ImmValue (69)) regG , Push regG 
	, Load (ImmValue (3)) regD , Push regD 
	, Load (ImmValue (2)) regC , Push regC 
	, Load (ImmValue (1)) regA , Push regA 
	, Load (ImmValue (42)) regE , Push regE 
	, Load (ImmValue (8)) regF , Push regF 
	, Load (ImmValue (4)) regB , Push regB 
	, Compute Add regMEM reg0 regD , Compute Add regMEM reg0 regC , Load (ImmValue (7)) regA , Compute Add regMEM regA regMEM , Pop regG , Store regG (IndAddr regD ), Compute Incr regD reg0 regD , Pop regG , Store regG (IndAddr regD ), Compute Incr regD reg0 regD , Pop regG , Store regG (IndAddr regD ), Compute Incr regD reg0 regD , Pop regG , Store regG (IndAddr regD ), Compute Incr regD reg0 regD , Pop regG , Store regG (IndAddr regD ), Compute Incr regD reg0 regD , Pop regG , Store regG (IndAddr regD ), Compute Incr regD reg0 regD , Pop regG , Store regG (IndAddr regD ), Compute Incr regD reg0 regD , Push regC 
	, --end of array creation
	Pop regE , Load (ImmValue (2)) regF , Compute Add regARP regF regF , Store regE (IndAddr regF )
	, -- function call to 'bubbleSort'
	-- visitIdExpr with type: int
	-- idCall to len
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regB regG regB , Push regB 
	, -- end idCall to len
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int[]
	-- idCall to arr
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regA , Load (ImmValue (2)) regA , Compute Add regC regA regC , Push regC 
	, -- end idCall to arr
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int[]
	-- precall
	Load (ImmValue (465)) regF , Store regF (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'bubbleSort'
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regD , Compute Add regMEM regD regMEM , -- end prologueScope...
	Load (ImmValue (0)) regC , Push regC 
	, Pop regA , Load (ImmValue (1)) regE , Compute Add regARP regE regE , Store regA (IndAddr regE )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regF regB regF , Push regF 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to len
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regC , Compute Sub regD regC regD , Load (IndAddr regD ) regD , Load (ImmValue (1)) regC , Compute Add regD regC regD , Push regD 
	, -- end idCall to len
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regE , Compute Lt regE regF regE , Push regE 
	, Pop regB , Load (ImmValue (1)) regG , Compute Xor regB regG regB , Branch regB (Rel (49))
	, -- visitIdExpr with type: int
	-- idCall to arr[i]
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regG , Compute Sub regB regG regB , Load (IndAddr regB ) regB , Load (ImmValue (2)) regG , Compute Add regB regG regB , Push regB 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regE , Load (ImmValue (1)) regE , Compute Add regA regE regA , Push regA 
	, -- end idCall to i
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regC , Pop regD , Load (IndAddr regD ) regD , Compute Add regD regC regD , Push regD , -- end accesser: [i]
	-- end idCall to arr[i]
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regG , WriteInstr regG numberIO
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regE , Load (ImmValue (1)) regE , Compute Add regA regE regA , Push regA 
	, -- end idCall to i
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regB , Pop regD , Compute Add regD regB regD , Push regD 
	, Pop regG , -- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regF regC regF , Push regF 
	, -- end idCall to i
	Pop regA , Store regG (IndAddr regA ), Jump (Rel (-73))
	, -- epilogueScope
	Load (IndAddr regARP ) regD , Compute Add regD reg0 regARP , -- end of epilogueScope
	Push regMEM , Load (ImmValue (2)) regB , Compute Add regMEM regB regMEM , Pop regF , Load (ImmValue (3)) regC , Compute Add regARP regC regC , Store regF (IndAddr regC )
	, Load (ImmValue (0)) regG , Push regG 
	, Pop regA , -- idCall to l.v
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regF , Load (ImmValue (3)) regF , Compute Add regB regF regB , Push regB 
	, -- accesser: .v
	Pop regC , Load (IndAddr regC ) regC , Load (ImmValue (1)) regG , Compute Add regG regC regC , Push regC , -- end accesser: .v
	-- end idCall to l.v
	Pop regE , Store regA (IndAddr regE ), -- Function 'append' in scope: [0]
	Jump (Rel (134)), -- prologue...
	Load (ImmValue (4)) regG , Compute Add regMEM regG regMEM , Compute Add reg0 regARP regF , Compute Incr regF reg0 regF , Pop regB , Store regB (IndAddr regF ), Compute Incr regF reg0 regF , Pop regB , Store regB (IndAddr regF ), -- end prologue...
	-- visitIdExpr with type: List
	-- idCall to l
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regC regA regC , Push regC 
	, -- end idCall to l
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: List
	Pop regD , Load (ImmValue (3)) regB , Compute Add regARP regB regB , Store regD (IndAddr regB )
	, -- visitIdExpr with type: List
	-- idCall to current.next
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regA , Load (ImmValue (3)) regA , Compute Add regC regA regC , Push regC 
	, -- accesser: .next
	Pop regE , Load (IndAddr regE ) regE , Load (ImmValue (0)) regD , Compute Add regD regE regE , Push regE , -- end accesser: .next
	-- end idCall to current.next
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: List
	Push reg0 , Pop regG , Pop regF , Compute NEq regF regG regF , Push regF 
	, Pop regF , Load (ImmValue (1)) regG , Compute Xor regF regG regF , Branch regF (Rel (36))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regC , Compute Add regMEM regC regMEM , -- end prologueScope...
	-- visitIdExpr with type: List
	-- idCall to current.next
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regD , Compute Sub regA regD regA , Load (IndAddr regA ) regA , Load (ImmValue (3)) regD , Compute Add regA regD regA , Push regA 
	, -- accesser: .next
	Pop regE , Load (IndAddr regE ) regE , Load (ImmValue (0)) regB , Compute Add regB regE regE , Push regE , -- end accesser: .next
	-- end idCall to current.next
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: List
	Pop regG , -- idCall to current
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regB , Compute Sub regD regB regD , Load (IndAddr regD ) regD , Load (ImmValue (3)) regB , Compute Add regD regB regD , Push regD 
	, -- end idCall to current
	Pop regC , Store regG (IndAddr regC ), -- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	Jump (Rel (-56))
	, Push regMEM , Load (ImmValue (2)) regF , Compute Add regMEM regF regMEM , Pop regD , Load (ImmValue (4)) regB , Compute Add regARP regB regB , Store regD (IndAddr regB )
	, -- visitIdExpr with type: int
	-- idCall to v
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Load (ImmValue (2)) regC , Compute Add regG regC regG , Push regG 
	, -- end idCall to v
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regE , -- idCall to node.v
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regG , Load (ImmValue (4)) regG , Compute Add regB regG regB , Push regB 
	, -- accesser: .v
	Pop regC , Load (IndAddr regC ) regC , Load (ImmValue (1)) regA , Compute Add regA regC regC , Push regC , -- end accesser: .v
	-- end idCall to node.v
	Pop regF , Store regE (IndAddr regF ), -- visitIdExpr with type: List
	-- idCall to node
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regG , Load (ImmValue (4)) regG , Compute Add regB regG regB , Push regB 
	, -- end idCall to node
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: List
	Pop regC , -- idCall to current.next
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regB , Load (ImmValue (3)) regB , Compute Add regD regB regD , Push regD 
	, -- accesser: .next
	Pop regG , Load (IndAddr regG ) regG , Load (ImmValue (0)) regA , Compute Add regA regG regG , Push regG , -- end accesser: .next
	-- end idCall to current.next
	Pop regE , Store regC (IndAddr regE ), -- epilogue
	Load (IndAddr regARP ) regD , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regB , Compute Add regD reg0 regARP , Jump (Ind regB )-- end of epilogue
	, -- End of function 'append' in scope: [0]
	-- function call to 'append'
	Load (ImmValue (5)) regA , Push regA 
	, -- visitIdExpr with type: List
	-- idCall to l
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Load (ImmValue (3)) regC , Compute Add regG regC regG , Push regG 
	, -- end idCall to l
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: List
	-- precall
	Load (ImmValue (729)) regF , Store regF (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (577))
	, -- function call ended for 'append'
	-- visitIdExpr with type: int
	-- idCall to l.next.v
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regB , Load (ImmValue (3)) regB , Compute Add regD regB regD , Push regD 
	, -- accesser: .next
	Pop regA , Load (IndAddr regA ) regA , Load (ImmValue (0)) regG , Compute Add regG regA regA , Push regA , -- end accesser: .next
	-- accesser: .v
	Pop regC , Load (IndAddr regC ) regC , Load (ImmValue (1)) regE , Compute Add regE regC regC , Push regC , -- end accesser: .v
	-- end idCall to l.next.v
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regD , WriteInstr regD numberIO
	, Load (ImmValue (0)) regB , Push regB 
	, Pop regG , Load (ImmValue (4)) regA , Compute Add regARP regA regA , Store regG (IndAddr regA )
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regC , Compute Add regMEM regC regMEM , -- end prologueScope...
	-- function call to 'f'
	-- precall
	Load (ImmValue (771)) regF , Store regF (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (772))
	, -- function call ended for 'f'
	-- Function 'f' in scope: [0, 4]
	Jump (Rel (52)), -- prologue...
	Load (ImmValue (0)) regG , Compute Add regMEM regG regMEM , Compute Add reg0 regARP regB , -- end prologue...
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regE , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Load (ImmValue (4)) regE , Compute Add regA regE regA , Push regA 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regF , Push regF 
	, Pop regB , Pop regD , Compute Add regD regB regD , Push regD 
	, Pop regG , -- idCall to i
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regF , Compute Sub regC regF regC , Load (IndAddr regC ) regC , Compute Sub regC regF regC , Load (IndAddr regC ) regC , Load (ImmValue (4)) regF , Compute Add regC regF regC , Push regC 
	, -- end idCall to i
	Pop regA , Store regG (IndAddr regA ), -- function call to 'g'
	-- precall
	Load (ImmValue (817)) regD , Store regD (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Compute Add regARP reg0 regD , Compute Decr regD reg0 regD , Load (IndAddr regD ) regD , Store regD (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (824))
	, -- function call ended for 'g'
	-- epilogue
	Load (IndAddr regARP ) regB , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regC , Compute Add regB reg0 regARP , Jump (Ind regC )-- end of epilogue
	, -- End of function 'f' in scope: [0, 4]
	-- Function 'g' in scope: [0, 4]
	Jump (Rel (137)), -- prologue...
	Load (ImmValue (0)) regA , Compute Add regMEM regA regMEM , Compute Add reg0 regARP regG , -- end prologue...
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Compute Sub regE regD regE , Load (IndAddr regE ) regE , Compute Sub regE regD regE , Load (IndAddr regE ) regE , Load (ImmValue (4)) regD , Compute Add regE regD regE , Push regE 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regG , Pop regF , Compute Add regF regG regF , Push regF 
	, Pop regA , -- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Compute Sub regB regC regB , Load (IndAddr regB ) regB , Compute Sub regB regC regB , Load (IndAddr regB ) regB , Load (ImmValue (4)) regC , Compute Add regB regC regB , Push regB 
	, -- end idCall to i
	Pop regE , Store regA (IndAddr regE ), -- function call to 'h'
	-- precall
	Load (ImmValue (866)) regF , Store regF (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (867))
	, -- function call ended for 'h'
	-- Function 'h' in scope: [0, 4, 1]
	Jump (Rel (88)), -- prologue...
	Load (ImmValue (0)) regC , Compute Add regMEM regC regMEM , Compute Add reg0 regARP regB , -- end prologue...
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regE , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Load (ImmValue (4)) regE , Compute Add regA regE regA , Push regA 
	, -- end idCall to i
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regF , Push regF 
	, Pop regB , Pop regG , Compute Add regG regB regG , Push regG 
	, Pop regC , -- idCall to i
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regF , Compute Sub regD regF regD , Load (IndAddr regD ) regD , Compute Sub regD regF regD , Load (IndAddr regD ) regD , Compute Sub regD regF regD , Load (IndAddr regD ) regD , Load (ImmValue (4)) regF , Compute Add regD regF regD , Push regD 
	, -- end idCall to i
	Pop regA , Store regC (IndAddr regA ), -- function call to 'inc'
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Load (ImmValue (4)) regB , Compute Add regG regB regG , Push regG 
	, -- end idCall to i
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	-- precall
	Load (ImmValue (934)) regF , Store regF (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Compute Add regARP reg0 regF , Compute Decr regF reg0 regF , Load (IndAddr regF ) regF , Compute Decr regF reg0 regF , Load (IndAddr regF ) regF , Compute Decr regF reg0 regF , Load (IndAddr regF ) regF , Store regF (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (963))
	, -- function call ended for 'inc'
	Pop regC , -- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Load (ImmValue (4)) regB , Compute Add regG regB regG , Push regG 
	, -- end idCall to i
	Pop regA , Store regC (IndAddr regA ), -- epilogue
	Load (IndAddr regARP ) regD , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regF , Compute Add regD reg0 regARP , Jump (Ind regF )-- end of epilogue
	, -- End of function 'h' in scope: [0, 4, 1]
	-- epilogue
	Load (IndAddr regARP ) regG , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regB , Compute Add regG reg0 regARP , Jump (Ind regB )-- end of epilogue
	, -- End of function 'g' in scope: [0, 4]
	-- epilogueScope
	Load (IndAddr regARP ) regC , Compute Add regC reg0 regARP , -- end of epilogueScope
	-- Function 'inc' in scope: [0]
	Jump (Rel (27)), -- prologue...
	Load (ImmValue (1)) regD , Compute Add regMEM regD regMEM , Compute Add reg0 regARP regE , Compute Incr regE reg0 regE , Pop regA , Store regA (IndAddr regE ), -- end prologue...
	--return returni+1;
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regF regG regF , Push regF 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regE , Pop regA , Compute Add regA regE regA , Push regA 
	, -- epilogue
	Load (IndAddr regARP ) regD , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regF , Compute Add regD reg0 regARP , Jump (Ind regF )-- end of epilogue
	, -- end return
-- End of function 'inc' in scope: [0]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Load (ImmValue (4)) regB , Compute Add regG regB regG , Push regG 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regA , WriteInstr regA numberIO
	, EndProg ]

main = run [prog]