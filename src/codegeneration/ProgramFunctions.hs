module ProgramFunctions where

import Sprockell
import Data.Char

-- | Generate code to print a (Haskell) String
writeString :: String -> [Instruction]
writeString str = concat $ map writeChar str

-- | Generate code to print a single character
writeChar :: Char -> [Instruction]
writeChar c =
    [ Load (ImmValue $ ord c) regA
    , WriteInstr regA charIO
    ]

-- | Generate code to print a boolean from a register
writeBool :: RegAddr -> [Instruction]
writeBool reg =
    [ Branch reg (Rel 14) ]
    ++ (writeString "false\n")
    ++ [ Jump (Rel 11) ]
    ++ (writeString "true\n")