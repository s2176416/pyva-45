package codegeneration.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Enum used for available registers
 */
public enum EReg {
    ZERO, SPRID, A, B, C, D, E, F, G, ARP, MEM, SP, PC;

    public static final String SZERO = "reg0 ";
    public static final String SSPRID = "regSprID ";
    public static final String SA = "regA ";
    public static final String SB = "regB ";
    public static final String SC = "regC ";
    public static final String SD = "regD ";
    public static final String SE = "regE ";
    public static final String SF = "regF ";
    public static final String SG = "regG ";
    public static final String SARP = "regARP ";
    public static final String SMEM = "regMEM ";
    public static final String SSP = "regSP ";
    public static final String SPC = "regPC ";
    /**
     * A list of registers that are available for use. Registers with predefined behaviour such as {@link EReg#ZERO zero register}
     * do not exist in this list.
     */
    public static final List<EReg> FREE_REGISTERS = new ArrayList<>(Arrays.asList(A, B, C, D, E, F, G));
    private static final String[] names = new String[]{SZERO, SSPRID, SA, SB, SC, SD, SE, SF, SG, SARP, SMEM, SSP, SPC};

    /**
     * Returns the register as a string
     * @return The string representation of the register
     */
    public String toString() {
        return names[this.ordinal()];
    }
}
