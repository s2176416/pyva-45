package codegeneration.util;

/**
 * Class that contains all string literals of Sprockell instructions
 */
public class Instruction {
    public static final String COMPUTE = "Compute ";
    public static final String JUMP = "Jump ";
    public static final String BRANCH = "Branch ";
    public static final String LOAD = "Load ";
    public static final String STORE = "Store ";
    public static final String PUSH = "Push ";
    public static final String POP = "Pop ";
    public static final String READINSTR = "ReadInstr ";
    public static final String RECEIVE = "Receive ";
    public static final String WRITEINSTR = "WriteInstr ";
    public static final String TESTANDSET = "TestAndSet ";
    public static final String ENDPROG = "EndProg ";
    public static final String NOP = "Nop ";
    public static final String DEBUG = "Debug ";
}
