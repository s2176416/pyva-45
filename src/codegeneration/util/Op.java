package codegeneration.util;

/**
 * Enum that contains all available operators
 */
public enum Op {
    ADD, SUB, MUL, DIV, EQUAL, NEQ, GT, LT, GTE, LTE, AND, OR, XOR, LSHIFT, RSHIFT, DECR, INCR;

    /**
     * Converts an enum operator to its string equivalent operator
     * @return String representation of the operator
     */
    public String toString() {
        switch (this) {
            case ADD:
                return SADD;
            case SUB:
                return SSUB;
            case MUL:
                return SMUL;
            case DIV:
                return SDIV;
            case EQUAL:
                return SEQUAL;
            case NEQ:
                return SNEQ;
            case GT:
                return SGT;
            case LT:
                return SLT;
            case GTE:
                return SGTE;
            case LTE:
                return SLTE;
            case AND:
                return SAND;
            case OR:
                return SOR;
            case XOR:
                return SXOR;
            case LSHIFT:
                return SLSHIFT;
            case RSHIFT:
                return SRSHIFT;
            case DECR:
                return SDECR;
            case INCR:
                return SINCR;
            default:
                return null;
        }
    }

    private static final String SADD = "Add ";
    private static final String SSUB = "Sub ";
    private static final String SMUL = "Mul ";
    private static final String SDIV = "Div ";
    private static final String SEQUAL = "Equal ";
    private static final String SNEQ = "NEq ";
    private static final String SGT = "Gt ";
    private static final String SLT = "Lt ";
    private static final String SGTE = "GtE ";
    private static final String SLTE = "LtE ";
    private static final String SAND = "And ";
    private static final String SOR = "Or ";
    private static final String SXOR = "Xor ";
    private static final String SLSHIFT = "LShift ";
    private static final String SRSHIFT = "RShift ";
    private static final String SDECR = "Decr ";
    private static final String SINCR = "Incr ";
}
