package codegeneration.util;

/**
 * Class used to store addressing types as strings
 */
public class AddrImmDI {
    public static final String IMMVALUE = "ImmValue ";
    public static final String DIRADDR = "DirAddr ";
    public static final String INDADDR = "IndAddr ";
    public static final String NUMBERIO = "numberIO";
    public static final String CHARIO = "charIO";
}
