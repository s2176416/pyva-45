package codegeneration.util;

/**
 * Enum used to for addressing types
 */
public enum EAddrImmDI {
    IMMVALUE, DIRADDR, INDADDR, NUMBERIO, CHARIO;

    private static final String SIMMVALUE = "ImmValue ";
    private static final String SDIRADDR = "DirAddr ";
    private static final String SINDADDR = "IndAddr ";
    private static final String SNUMBERIO = "numberIO ";
    private static final String SCHARIO = "charIO ";
    public static final String[] names = new String[]{SIMMVALUE, SDIRADDR, SINDADDR, SNUMBERIO, SCHARIO};

    /**
     * Returns the Sprockell address type as a string
     * @return Sprockell address type
     */
    public String toString() {
        return names[this.ordinal()];
    }
}
