package codegeneration.util;

/**
 * Enum used for available target types
 */
public enum ETarget {
    ABS, REL, IND;

    private static final String TABS = "Abs ";
    private static final String TREL = "Rel ";
    private static final String TIND = "Ind ";
    public static final String[] names = new String[]{TABS, TREL, TIND};

    /**
     * Transform this enum into Sprockell accepted string representation
     * @return String representation of this enum type
     */
    public String toString() {
        return names[this.ordinal()];
    }
}
