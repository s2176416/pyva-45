package testing.contextual;

import exceptions.Exception;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import testing.syntax.SyntaxTest;
import typecheck.PyVaTypeScopeCheck;

public class ContextualTest {

    @Test
    public void simpleTypeCheck() {
        succeeds("int i = 0; i = 1;");
        succeeds("char a = 'a'; a = 'A';");
        succeeds("boolean b = true; b = false;");

        fails("int i = 0; i = false;"); // Cannot reassign a boolean to integer variable
        fails("char a = 'a'; a = 97;"); // Cannot reassign an integer to a character variable
        fails("boolean b = true; b = 0;"); // Cannot reassign an integer to a boolean variable
    }

    @Test
    public void identifierDeclarationTest() {
        succeeds("int i = 0; i = 100;");
        succeeds("int i = 0; {i = 1;}");
        succeeds("int i = 0; def void inci() {i = i + 1;}");

        fails("int i = 0; a = 0;"); // a is not yet defined;
        fails("int i = 0; {a = 0;}"); // a is not defined in the scope nor in the enclosing scope(s).
        fails("int i = 0; def void inca() {a = a + 1;}"); // a is not defined in the scope nor in the enclosing scope(s).
    }

    @Test
    public void expectedTypeTest() {
        succeeds("boolean b = true; if (b) {print b;}");
        succeeds("int i = 0; while (i < 5) {print i; i = i + 1;}");
        succeeds("for (int i = 0; i < 10; i = i + 1) {}");
        succeeds("def int inc(int i) {return i + 1;}");

        fails("for (int i = 0; i; i = i + 1) {print i;}"); // It expects the expression in the middle of the for loop to be a boolean expression.
        fails("int i = 0; if (i) {print i;}"); // The expression in the condition of the if statement needs to be a boolean expression.
        fails("def char a() {return 97;}"); // The return statement of the function needs to be a char.
    }

    @Test
    public void funcDefAndCallTest() {
        succeeds("def int one() {return 1;}");
        succeeds("int i = 0; def void inci() {i = i + 1;}");
        succeeds("int i = 0; def void inci() {def int inc(int a) {return a + 1;} i = inc(i);}");
        succeeds("def int incCond(int i) {if (i < 5) {return i + 1;} else {return i;}}"); // Succeeds because return statement exists in both if and else statements.
        succeeds("def int inc(int i) {return i + 1;} int i = inc(0);");

        fails("int i = 0; def int inci() {i = i + 1;}"); // If the function type isn't void, it needs to have a return statement.
        fails("def void inc(int i) {return  i + 1;}"); // If the function type is void, there shouldn't be a return statement.
        fails("def int incCond(int i) {if (i < 5) {return i + 1;}}"); // Return statement might not be reachable.
        fails("def int incTry(int i) {int a = 0; while (a < 10) {return i + 1;}"); // Return statement might not be reachable.
        fails("def int one() {return 'a';}"); // Wrong return type.
        fails("def boolean right() {return 1;}"); // return type and function type don't match.
        fails("def int inc(int i) {return i + 1;} char a = inc(0);"); // Variable's type does not match function's type.
        fails("int i = 0; def void lol() {i = 0;} int ii = lol();"); // Void functions do not return anything.
    }

    @Test
    public void ifTest() {
        succeeds("if (true) { print true; }");
        succeeds("if (1 == 1) { print 1; }");
        succeeds("int i = 0; if (i < 10) {print 10;}");
        succeeds("if (!false) {print false;}");
        succeeds("def boolean bigger(int i, int o) {return i > o;} if (bigger(5, 2)) {print 5;}");
        succeeds("int i = 0; if (i < 5) {print i;} else {print 5;}");
        succeeds("int[] ia = [1, 2, 3]; if (ia[2] < 5) {ia[2] = 5;}");
        succeeds("def record Coor {int x; int y;} Coor c = new Coor; c.x = 0; c.y = 0; if (c.x == c.y) {print c.x;}");
        succeeds("int i = 0; if (true) {boolean i = true;}");

        fails("if (1) {}"); // Condition expression has to be a boolean for the tests below.
        fails("if ('a') {}");
        fails("def int one() {return 1;} if (one()) {print one();}");
        fails("def void voidFunc() {} if (voidFunc()) {}");
        fails("def record Coor {int x; int y;} Coor c = new Coor; c.x = 0; c.y = 0; if (c) {} if (c.x) {}");
        fails("int[] ia = [1, 2, 3]; if (ia) {ia = 2;}");
    }

    @Test
    public void whileTest() {
        succeeds("int i = 0; while (i < 10) {print i; i = i + 1;}");
        succeeds("boolean gotit = false; int[] ia = [1, 2, 3]; int i = 0; while (!gotit) {if (ia[i] == 3) {gotit = true;} i = i + 1;}");

        // The following copies from ifTest(). Else statements are removed.
        // This is to show that only boolean expressions can be in the while condition.
        succeeds("while (true) { print true; }");
        succeeds("while (1 == 1) { print 1; }");
        succeeds("int i = 0; while (i < 10) {print 10;}");
        succeeds("while (!false) {print false;}");
        succeeds("def boolean bigger(int i, int o) {return i > o;} while (bigger(5, 2)) {print 5;}");
        succeeds("int[] ia = [1, 2, 3]; while (ia[2] < 5) {ia[2] = 5;}");
        succeeds("def record Coor {int x; int y;} Coor c = new Coor; c.x = 0; c.y = 0; while (c.x == c.y) {print c.x;}");
        succeeds("int i = 0; while (true) {boolean i = true;}");

        fails("while (1) {}"); // Condition expression has to be a boolean fot the tests below.
        fails("while ('a') {}");
        fails("def int one() {return 1;} while (one()) {print one();}");
        fails("def void voidFunc() {} while (voidFunc()) {}");
        fails("def record Coor {int x; int y;} Coor c = new Coor; c.x = 0; c.y = 0; while (c) {} while (c.x) {}");
        fails("int[] ia = [1, 2, 3]; while (ia) {ia = 2;}");
    }

    @Test
    public void forTest() {
        succeeds("for (int i = 0; i < 10; i = i + 1) {print i;}");
        succeeds("int i = 0; for (i = 1; i < 10; i = i + 1) {print i;}");
        succeeds("int i = 0; for (int i = 0; i < 10; i = i + 1) {print i;}");

        fails("int i = 0; for (int a = 0 ; a; a = a + 1) {i = a;}"); // Expression in the middle has to be boolean.
    }

    @Test
    public void threadAndSyncTest() {
        succeeds("int i = 0; thread pp {for (int a = 0; a < 10; a = a + 1) {i = i + a;}}");
        succeeds("int i = 0; thread pp {for (int a = 0; a < 10; a = a + 1) {i = i + a;}} join pp;");
        succeeds("int i = 0; thread pp {for (int a = 0; a < 10; a = a + 1) {sync {i = i + a;}}} join pp;");

        fails("thread pp {} join ll;"); // thread ll is not defined.
    }

    @Test
    public void arrayTest() {
        succeeds("int[] ia = new int[3];");
        succeeds("int[] ia = [1, 2, 3];");
        succeeds("int[] ia = [1, 2, 3]; int i = ia[0];");
        succeeds("int[] ia = [1, 2, 3]; int[] ia2 = ia;");
        succeeds("int[][] iaa = new int[3][2];");
        succeeds("int[][] iaa = [[1], [1, 2]];");
        succeeds("int[][] iaa = [[1], [1, 2]]; int[] ia = iaa[0]; int i = ia[0];");
        succeeds("int[][] iaa = new int[3][2]; int i = iaa[0][0];");
        succeeds("def record Coor{int x; int y;} Coor c = new Coor; c.x = 0; c.y = 0; Coor[] cs = [c]; Coor newC = cs[0];");
        succeeds("def record Coor{int x; int y;} Coor[][] css = new Coor[2][3];");

        fails("int[] ia = [1, 2, 3]; int[] is = ia[0];"); // type of ia[0] is int, not int[]
        fails("int[][] iaa = [[1, 2, 3], [1, 3, 4]]; int i = iaa[0];"); // iaa[0] is of type int[], not int.
        fails("def record Coor{int x; int y;} Coor c = new Coor; c.x = 0; c.y = 0; Coor[][] css = [[c]]; Coor newC = css[0];"); // Ibid
        fails("def record Coor{int x; int y;} Coor c = new Coor; c.x = 0; c.y = 0; Coor[][] css = [[c]]; Coor[] cs = css[0][0];"); // Ibid
    }

    @Test
    public void recordTest() {
        succeeds("def record Coor {int x; int y;}");
        succeeds("def record Coor {int x; int y;} Coor c = new Coor; c.x = 0; c.y = 0; int x = c.x;");
        succeeds("def record Person {int nr; Person mother; Person father;}");
        succeeds("def record Person {int nr; Person mother; Person father;}" +
                "Person me = new Person; me.nr =  0; me.father = new Person; Person father = me.father;");
        succeeds("def record Coor {int x; int y;} def record City {boolean awesome; Coor coor;}");

        fails("def record int {int value;}"); // int is already predefined and cannot be redefined.
    }

    public void succeeds(String input) {
        try {
            typeCheck(input);
        } catch (Exception e) {
            System.err.println("Input '" + input + "' should be acceptable but isn't!");
            assert false;
        }
    }

    public void fails(String input) {
        try {
            typeCheck(input);
            fails("Input '" + input + "' should have failed in type checking but didn't!");
        } catch (Exception e) {
            // passes
        }
    }

    public static void typeCheck(String input) throws Exception {
        SyntaxTest.syntaxCheck(input);

        ParseTree tree = SyntaxTest.syntaxCheck(input);
        PyVaTypeScopeCheck typeChecker = new PyVaTypeScopeCheck();
        typeChecker.check(tree);
    }
}
