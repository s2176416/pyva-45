def boolean isPrime(int n) {
    boolean result = false;
    boolean gotit = false;

    if (n <= 1) {
        gotit = true;
        result = false;
    }

    for (int i = 2; !gotit && i < n; i = i + 1) {
        if ((n - (i * (n / i))) == 0) {
            gotit = true;
            result = false;
        }

    }

    if (!gotit) {
        result = true;
    }

    return result;
}


print isPrime(2);
print isPrime(3);
print isPrime(4);
print isPrime(5);
print isPrime(6);
print isPrime(7);
print isPrime(8);
print isPrime(9);
print isPrime(10);
print isPrime(11);
print isPrime(12);
print isPrime(13);
print isPrime(14);
print isPrime(15);
print isPrime(16);
print isPrime(17);
print isPrime(18);
print isPrime(19);
print isPrime(20);