module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regE , Compute Equal regE reg0 regF , Branch regF (Rel (-3)), Jump (Ind regE ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regD , Compute Add regD reg0 regARP , Load (ImmValue (3)) regB , Compute Add regMEM regB regMEM , Load (ImmValue (21)) regG , WriteInstr regG (DirAddr 1), Jump (Rel (45)), -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regC , Compute Add regMEM regC regMEM , -- end prologueScope...
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regF , Compute Add regMEM regF regMEM , -- end prologueScope...
	Load (ImmValue (0)) regD , Push regD 
	, Pop regB , WriteInstr regB (DirAddr 4)
	, -- visitIdExpr with type: int
	-- idCall to i
	ReadInstr (DirAddr 4), Receive regA , Push regA , -- end idCall to i
	-- end of visitIdExpr with type: int
	Load (ImmValue (1)) regF , Push regF 
	, Pop regB , Pop regD , Compute Lt regD regB regD , Push regD 
	, Pop regA , Load (ImmValue (1)) regE , Compute Xor regA regE regA , Branch regA (Rel (7))
	, -- visitIdExpr with type: int
	-- idCall to i
	ReadInstr (DirAddr 4), Receive regC , Push regC , -- end idCall to i
	-- end of visitIdExpr with type: int
	Pop regE , WriteInstr regE (DirAddr 4), Jump (Rel (-18))
	, -- epilogueScope
	Load (IndAddr regARP ) regB , Compute Add regB reg0 regARP , -- end of epilogueScope
	-- epilogueScope
	Load (IndAddr regARP ) regG , Compute Add regG reg0 regARP , -- end of epilogueScope
	Load (IndAddr regARP ) regARP 
	, WriteInstr reg0 (IndAddr regSprID ), EndProg 
	, ReadInstr (DirAddr 1), Receive regC , Compute NEq regC reg0 regC , Branch regC (Rel (-3))
	, EndProg ]

main = run [prog, prog]