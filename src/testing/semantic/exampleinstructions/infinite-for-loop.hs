module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regC , Compute Equal regC reg0 regD , Branch regD (Rel (-3)), Jump (Ind regC ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regB , Compute Add regB reg0 regARP , Load (ImmValue (3)) regE , Compute Add regMEM regE regMEM , -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	Load (ImmValue (0)) regC , Push regC 
	, Pop regD , Load (ImmValue (1)) regB , Compute Add regARP regB regB , Store regD (IndAddr regB )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regE regF regE , Push regE 
	, -- end idCall to i
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regG , Push regG 
	, Pop regD , Pop regC , Compute Lt regC regD regC , Push regC 
	, Pop regF , Load (ImmValue (1)) regA , Compute Xor regF regA regF , Branch regF (Rel (18))
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regE , Load (ImmValue (1)) regE , Compute Add regB regE regB , Push regB 
	, -- end idCall to i
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regA , -- idCall to i
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regD regB regD , Push regD 
	, -- end idCall to i
	Pop regG , Store regA (IndAddr regG ), Jump (Rel (-34))
	, -- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	EndProg ]

main = run [prog]