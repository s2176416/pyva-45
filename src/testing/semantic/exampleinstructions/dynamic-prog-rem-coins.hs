module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regD , Compute Equal regD reg0 regF , Branch regF (Rel (-3)), Jump (Ind regD ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regA , Compute Add regA reg0 regARP , Load (ImmValue (7)) regB , Compute Add regMEM regB regMEM , --array creation of: [10,7,4,2]
	Load (ImmValue (2)) regC , Push regC 
	, Load (ImmValue (4)) regG , Push regG 
	, Load (ImmValue (7)) regE , Push regE 
	, Load (ImmValue (10)) regD , Push regD 
	, Compute Add regMEM reg0 regA , Compute Add regMEM reg0 regB , Load (ImmValue (4)) regC , Compute Add regMEM regC regMEM , Pop regF , Store regF (IndAddr regA ), Compute Incr regA reg0 regA , Pop regF , Store regF (IndAddr regA ), Compute Incr regA reg0 regA , Pop regF , Store regF (IndAddr regA ), Compute Incr regA reg0 regA , Pop regF , Store regF (IndAddr regA ), Compute Incr regA reg0 regA , Push regB 
	, --end of array creation
	Pop regG , Load (ImmValue (1)) regE , Compute Add regARP regE regE , Store regG (IndAddr regE )
	, Load (ImmValue (16)) regD , Push regD 
	, Pop regF , Load (ImmValue (2)) regA , Compute Add regARP regA regA , Store regF (IndAddr regA )
	, Load (ImmValue (3)) regB , Push regB 
	, Pop regC , Load (ImmValue (3)) regG , Compute Add regARP regG regG , Store regC (IndAddr regG )
	, --array creation of: newint[n+1][amount+1]
	Compute Add regMEM reg0 regE , Push regE , -- visitIdExpr with type: int
	-- idCall to amount
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regF , Load (ImmValue (2)) regF , Compute Add regD regF regD , Push regD 
	, -- end idCall to amount
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regB , Push regB 
	, Pop regG , Pop regC , Compute Add regC regG regC , Push regC 
	, -- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regF , Load (ImmValue (3)) regF , Compute Add regD regF regD , Push regD 
	, -- end idCall to n
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regB , Push regB 
	, Pop regG , Pop regC , Compute Add regC regG regC , Push regC 
	, Compute Add regMEM reg0 regE , Pop regD , Compute Add regMEM regD regMEM , Pop regA , Load (ImmValue (0)) regC , Compute Equal reg0 regD regB , Compute Add regMEM reg0 regF , Branch regB (Rel (7)), Store regMEM (IndAddr regE ), Compute Add regMEM regA regMEM , Compute Incr regC reg0 regC , Compute Lt regC regD regB , Compute Incr regE reg0 regE , Branch regB (Rel (-5)), Compute Mul regD regA regG , --end of array creation
	Pop regG , Load (ImmValue (4)) regE , Compute Add regARP regE regE , Store regG (IndAddr regE )
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regA , Compute Add regMEM regA regMEM , -- end prologueScope...
	Load (ImmValue (0)) regB , Push regB 
	, Pop regC , Load (ImmValue (1)) regG , Compute Add regARP regG regG , Store regC (IndAddr regG )
	, -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regE regD regE , Push regE 
	, -- end idCall to j
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to amount
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regB , Compute Sub regA regB regA , Load (IndAddr regA ) regA , Load (ImmValue (2)) regB , Compute Add regA regB regA , Push regA 
	, -- end idCall to amount
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regG , Push regG 
	, Pop regD , Pop regE , Compute Add regE regD regE , Push regE 
	, Pop regA , Pop regF , Compute Lt regF regA regF , Push regF 
	, Pop regD , Load (ImmValue (1)) regF , Compute Xor regD regF regD , Branch regD (Rel (70))
	, -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regB regC regB , Push regB 
	, -- end idCall to j
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Pop regE , -- idCall to M[n][j]
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regB , Compute Sub regA regB regA , Load (IndAddr regA ) regA , Load (ImmValue (4)) regB , Compute Add regA regB regA , Push regA 
	, -- accesser: [n][j]
	-- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regB , Compute Sub regA regB regA , Load (IndAddr regA ) regA , Load (ImmValue (3)) regB , Compute Add regA regB regA , Push regA 
	, -- end idCall to n
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regG , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regG regC , Push regC , -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regB regA regB , Push regB 
	, -- end idCall to j
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regG , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regG regC , Push regC , -- end accesser: [n][j]
	-- end idCall to M[n][j]
	Pop regD , Store regE (IndAddr regD ), -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regA regB regA , Push regA 
	, -- end idCall to j
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regD , Pop regE , Compute Add regE regD regE , Push regE 
	, Pop regF , -- idCall to j
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regG regC regG , Push regG 
	, -- end idCall to j
	Pop regA , Store regF (IndAddr regA ), Jump (Rel (-100))
	, -- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regC , Compute Add regMEM regC regMEM , -- end prologueScope...
	Load (ImmValue (1)) regF , Push regF 
	, Pop regA , Load (ImmValue (1)) regB , Compute Add regARP regB regB , Store regA (IndAddr regB )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regE regD regE , Push regE 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regF , Compute Sub regC regF regC , Load (IndAddr regC ) regC , Load (ImmValue (3)) regF , Compute Add regC regF regC , Push regC 
	, -- end idCall to n
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regB , Push regB 
	, Pop regD , Pop regE , Compute Add regE regD regE , Push regE 
	, Pop regC , Pop regG , Compute Lt regG regC regG , Push regG 
	, Pop regD , Load (ImmValue (1)) regG , Compute Xor regD regG regD , Branch regD (Rel (56))
	, Load (ImmValue (0)) regF , Push regF 
	, Pop regA , -- idCall to M[i][0]
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regG , Compute Sub regD regG regD , Load (IndAddr regD ) regD , Load (ImmValue (4)) regG , Compute Add regD regG regD , Push regD 
	, -- accesser: [i][0]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regD regG regD , Push regD 
	, -- end idCall to i
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regF regC , Push regC , Load (ImmValue (0)) regG , Push regG 
	, Pop regF , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regF regC , Push regC , -- end accesser: [i][0]
	-- end idCall to M[i][0]
	Pop regB , Store regA (IndAddr regB ), -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regD regG regD , Push regD 
	, -- end idCall to i
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regB , Pop regA , Compute Add regA regB regA , Push regA 
	, Pop regE , -- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regF regC regF , Push regF 
	, -- end idCall to i
	Pop regD , Store regE (IndAddr regD ), Jump (Rel (-86))
	, -- epilogueScope
	Load (IndAddr regARP ) regA , Compute Add regA reg0 regARP , -- end of epilogueScope
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regC , Compute Add regMEM regC regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Compute Sub regE regD regE , Load (IndAddr regE ) regE , Load (ImmValue (3)) regD , Compute Add regE regD regE , Push regE 
	, -- end idCall to n
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regA , Push regA 
	, Pop regF , Pop regB , Compute Sub regB regF regB , Push regB 
	, Pop regC , Load (ImmValue (1)) regE , Compute Add regARP regE regE , Store regC (IndAddr regE )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regD regG regD , Push regD 
	, -- end idCall to i
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (0)) regB , Push regB 
	, Pop regC , Pop regF , Compute Gt regF regC regF , Push regF 
	, Pop regB , Load (ImmValue (1)) regF , Compute Xor regB regF regB , Branch regB (Rel (677))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	Load (ImmValue (1)) regA , Push regA 
	, Pop regB , Load (ImmValue (1)) regF , Compute Add regARP regF regF , Store regB (IndAddr regF )
	, -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regE , Load (ImmValue (1)) regE , Compute Add regC regE regC , Push regC 
	, -- end idCall to j
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to amount
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regA , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Load (ImmValue (2)) regA , Compute Add regG regA regG , Push regG 
	, -- end idCall to amount
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regF , Push regF 
	, Pop regE , Pop regC , Compute Add regC regE regC , Push regC 
	, Pop regG , Pop regD , Compute Lt regD regG regD , Push regD 
	, Pop regE , Load (ImmValue (1)) regD , Compute Xor regE regD regE , Branch regE (Rel (605))
	, -- visitIdExpr with type: int
	-- idCall to coins[i]
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regC , Compute Sub regF regC regF , Load (IndAddr regF ) regF , Compute Sub regF regC regF , Load (IndAddr regF ) regF , Load (ImmValue (1)) regC , Compute Add regF regC regF , Push regF 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regA , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regA , Compute Add regG regA regG , Push regG 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regD , Pop regE , Load (IndAddr regE ) regE , Compute Add regE regD regE , Push regE , -- end accesser: [i]
	-- end idCall to coins[i]
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regC regG regC , Push regC 
	, -- end idCall to j
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regD , Pop regB , Compute LtE regB regD regB , Push regB 
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	Pop regA , Load (ImmValue (1)) regB , Compute Xor regA regB regA , Branch regA (Rel (432))
	, -- prologueScope...
	Load (ImmValue (0)) regE , Compute Add regMEM regE regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to M[i+1][j]
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regA , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Load (ImmValue (4)) regA , Compute Add regG regA regG , Push regG 
	, -- accesser: [i+1][j]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regF , Compute Sub regE regF regE , Load (IndAddr regE ) regE , Compute Sub regE regF regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regF , Compute Add regE regF regE , Push regE 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regG , Push regG 
	, Pop regE , Pop regA , Compute Add regA regE regA , Push regA 
	, Pop regD , Pop regB , Load (IndAddr regB ) regB , Compute Add regB regD regB , Push regB , -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regC , Compute Sub regF regC regF , Load (IndAddr regF ) regF , Load (ImmValue (1)) regC , Compute Add regF regC regF , Push regF 
	, -- end idCall to j
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Pop regD , Pop regB , Load (IndAddr regB ) regB , Compute Add regB regD regB , Push regB , -- end accesser: [i+1][j]
	-- end idCall to M[i+1][j]
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regE , Push regE 
	, -- visitIdExpr with type: int
	-- idCall to M[i][j-coins[i]]
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regC , Compute Sub regF regC regF , Load (IndAddr regF ) regF , Compute Sub regF regC regF , Load (IndAddr regF ) regF , Compute Sub regF regC regF , Load (IndAddr regF ) regF , Load (ImmValue (4)) regC , Compute Add regF regC regF , Push regF 
	, -- accesser: [i][j-coins[i]]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regA , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regA , Compute Add regB regA regB , Push regB 
	, -- end idCall to i
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Pop regD , Pop regG , Load (IndAddr regG ) regG , Compute Add regG regD regG , Push regG , -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regC , Compute Sub regF regC regF , Load (IndAddr regF ) regF , Load (ImmValue (1)) regC , Compute Add regF regC regF , Push regF 
	, -- end idCall to j
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to coins[i]
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regE , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Load (ImmValue (1)) regE , Compute Add regA regE regA , Push regA 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regA , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regA , Compute Add regB regA regB , Push regB 
	, -- end idCall to i
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Pop regC , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regC regF , Push regF , -- end accesser: [i]
	-- end idCall to coins[i]
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regE , Pop regA , Compute Sub regA regE regA , Push regA 
	, Pop regD , Pop regG , Load (IndAddr regG ) regG , Compute Add regG regD regG , Push regG , -- end accesser: [i][j-coins[i]]
	-- end idCall to M[i][j-coins[i]]
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regB , Pop regF , Compute Add regF regB regF , Push regF 
	, Pop regE , Pop regA , Compute Lt regA regE regA , Push regA 
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	Pop regF , Load (ImmValue (1)) regC , Compute Xor regF regC regF , Branch regF (Rel (116))
	, -- prologueScope...
	Load (ImmValue (0)) regD , Compute Add regMEM regD regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to M[i+1][j]
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Compute Sub regG regC regG , Load (IndAddr regG ) regG , Compute Sub regG regC regG , Load (IndAddr regG ) regG , Compute Sub regG regC regG , Load (IndAddr regG ) regG , Compute Sub regG regC regG , Load (IndAddr regG ) regG , Load (ImmValue (4)) regC , Compute Add regG regC regG , Push regG 
	, -- accesser: [i+1][j]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regE , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Load (ImmValue (1)) regE , Compute Add regA regE regA , Push regA 
	, -- end idCall to i
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regG , Push regG 
	, Pop regA , Pop regC , Compute Add regC regA regC , Push regC 
	, Pop regB , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regB regF , Push regF , -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Compute Sub regE regD regE , Load (IndAddr regE ) regE , Compute Sub regE regD regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regD , Compute Add regE regD regE , Push regE 
	, -- end idCall to j
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Pop regB , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regB regF , Push regF , -- end accesser: [i+1][j]
	-- end idCall to M[i+1][j]
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regA , -- idCall to M[i][j]
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Load (ImmValue (4)) regB , Compute Add regG regB regG , Push regG 
	, -- accesser: [i][j]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regB , Compute Add regG regB regG , Push regG 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Pop regC , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regC regF , Push regF , -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regG , Compute Sub regB regG regB , Load (IndAddr regB ) regB , Compute Sub regB regG regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regG , Compute Add regB regG regB , Push regB 
	, -- end idCall to j
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regC , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regC regF , Push regF , -- end accesser: [i][j]
	-- end idCall to M[i][j]
	Pop regE , Store regA (IndAddr regE ), -- epilogueScope
	Load (IndAddr regARP ) regG , Compute Add regG reg0 regARP , -- end of epilogueScope
	Jump (Rel (160))
	, -- prologueScope...
	Load (ImmValue (0)) regB , Compute Add regMEM regB regMEM , -- end prologueScope...
	Load (ImmValue (1)) regC , Push regC 
	, -- visitIdExpr with type: int
	-- idCall to M[i][j-coins[i]]
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regA , Compute Sub regF regA regF , Load (IndAddr regF ) regF , Compute Sub regF regA regF , Load (IndAddr regF ) regF , Compute Sub regF regA regF , Load (IndAddr regF ) regF , Compute Sub regF regA regF , Load (IndAddr regF ) regF , Load (ImmValue (4)) regA , Compute Add regF regA regF , Push regF 
	, -- accesser: [i][j-coins[i]]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regB , Compute Add regG regB regG , Push regG 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regD , Pop regE , Load (IndAddr regE ) regE , Compute Add regE regD regE , Push regE , -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regA , Compute Sub regF regA regF , Load (IndAddr regF ) regF , Compute Sub regF regA regF , Load (IndAddr regF ) regF , Load (ImmValue (1)) regA , Compute Add regF regA regF , Push regF 
	, -- end idCall to j
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to coins[i]
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Compute Sub regB regC regB , Load (IndAddr regB ) regB , Compute Sub regB regC regB , Load (IndAddr regB ) regB , Compute Sub regB regC regB , Load (IndAddr regB ) regB , Compute Sub regB regC regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regC , Compute Add regB regC regB , Push regB 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regB , Compute Add regG regB regG , Push regG 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regA , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regA regF , Push regF , -- end accesser: [i]
	-- end idCall to coins[i]
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Pop regC , Pop regB , Compute Sub regB regC regB , Push regB 
	, Pop regD , Pop regE , Load (IndAddr regE ) regE , Compute Add regE regD regE , Push regE , -- end accesser: [i][j-coins[i]]
	-- end idCall to M[i][j-coins[i]]
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regG , Pop regF , Compute Add regF regG regF , Push regF 
	, Pop regB , -- idCall to M[i+1][j]
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regA , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Load (ImmValue (4)) regA , Compute Add regE regA regE , Push regE 
	, -- accesser: [i+1][j]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regA , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regA , Compute Add regE regA regE , Push regE 
	, -- end idCall to i
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regA , Push regA 
	, Pop regA , Pop regE , Compute Add regE regA regE , Push regE 
	, Pop regG , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regG regF , Push regF , -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regA , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regA , Compute Add regE regA regE , Push regE 
	, -- end idCall to j
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Pop regG , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regG regF , Push regF , -- end accesser: [i+1][j]
	-- end idCall to M[i+1][j]
	Pop regC , Store regB (IndAddr regC ), -- epilogueScope
	Load (IndAddr regARP ) regA , Compute Add regA reg0 regARP , -- end of epilogueScope
	-- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	Jump (Rel (103))
	, -- prologueScope...
	Load (ImmValue (0)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to M[i+1][j]
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regB , Compute Sub regF regB regF , Load (IndAddr regF ) regF , Compute Sub regF regB regF , Load (IndAddr regF ) regF , Compute Sub regF regB regF , Load (IndAddr regF ) regF , Load (ImmValue (4)) regB , Compute Add regF regB regF , Push regF 
	, -- accesser: [i+1][j]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regE , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Compute Sub regA regE regA , Load (IndAddr regA ) regA , Load (ImmValue (1)) regE , Compute Add regA regE regA , Push regA 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regF , Push regF 
	, Pop regA , Pop regB , Compute Add regB regA regB , Push regB 
	, Pop regD , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regD regC , Push regC , -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regG , Compute Sub regE regG regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regG , Compute Add regE regG regE , Push regE 
	, -- end idCall to j
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regD , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regD regC , Push regC , -- end accesser: [i+1][j]
	-- end idCall to M[i+1][j]
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regA , -- idCall to M[i][j]
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regD , Compute Sub regF regD regF , Load (IndAddr regF ) regF , Compute Sub regF regD regF , Load (IndAddr regF ) regF , Compute Sub regF regD regF , Load (IndAddr regF ) regF , Load (ImmValue (4)) regD , Compute Add regF regD regF , Push regF 
	, -- accesser: [i][j]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regD , Compute Sub regF regD regF , Load (IndAddr regF ) regF , Compute Sub regF regD regF , Load (IndAddr regF ) regF , Load (ImmValue (1)) regD , Compute Add regF regD regF , Push regF 
	, -- end idCall to i
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regB , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regB regC , Push regC , -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regF , Compute Sub regD regF regD , Load (IndAddr regD ) regD , Load (ImmValue (1)) regF , Compute Add regD regF regD , Push regD 
	, -- end idCall to j
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Pop regB , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regB regC , Push regC , -- end accesser: [i][j]
	-- end idCall to M[i][j]
	Pop regE , Store regA (IndAddr regE ), -- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regD regB regD , Push regD 
	, -- end idCall to j
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regA , Push regA 
	, Pop regG , Pop regE , Compute Add regE regG regE , Push regE 
	, Pop regF , -- idCall to j
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regC regA regC , Push regC 
	, -- end idCall to j
	Pop regD , Store regF (IndAddr regD ), Jump (Rel (-637))
	, -- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regG regC regG , Push regG 
	, -- end idCall to i
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regF , Push regF 
	, Pop regB , Pop regD , Compute Sub regD regB regD , Push regD 
	, Pop regE , -- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regA regF regA , Push regA 
	, -- end idCall to i
	Pop regG , Store regE (IndAddr regG ), Jump (Rel (-693))
	, -- epilogueScope
	Load (IndAddr regARP ) regD , Compute Add regD reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: int
	-- idCall to M[1][amount]
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regA , Load (ImmValue (4)) regA , Compute Add regB regA regB , Push regB 
	, -- accesser: [1][amount]
	Load (ImmValue (1)) regG , Push regG 
	, Pop regE , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regE regF , Push regF , -- visitIdExpr with type: int
	-- idCall to amount
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regD , Load (ImmValue (2)) regD , Compute Add regC regD regC , Push regC 
	, -- end idCall to amount
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regE , Pop regF , Load (IndAddr regF ) regF , Compute Add regF regE regF , Push regF , -- end accesser: [1][amount]
	-- end idCall to M[1][amount]
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regG , WriteInstr regG numberIO
	, EndProg ]

main = run [prog]