module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regB , Compute Equal regB reg0 regG , Branch regG (Rel (-3)), Jump (Ind regB ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regC , Compute Add regC reg0 regARP , Load (ImmValue (3)) regA , Compute Add regMEM regA regMEM , Load (ImmValue (1)) regE , Push regE 
	, Load (ImmValue (0)) regF , Push regF 
	, Pop regB , Pop regD , Load (ImmValue (0)) regC , Compute Lt regD regB regG , Branch regG (Rel (5)), Compute Sub regD regB regD , Load (ImmValue (1)) regG , Compute Add regC regG regC , Jump (Rel (-5)), Push regC 
	, Pop regA , WriteInstr regA numberIO
	, EndProg ]

main = run [prog]