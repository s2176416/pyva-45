module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regE , Compute Equal regE reg0 regF , Branch regF (Rel (-3)), Jump (Ind regE ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regD , Compute Add regD reg0 regARP , Load (ImmValue (4)) regB , Compute Add regMEM regB regMEM , --array creation of: [[1],[1,2]]
	--array creation of: [1,2]
	Load (ImmValue (2)) regG , Push regG 
	, Load (ImmValue (1)) regC , Push regC 
	, Compute Add regMEM reg0 regE , Compute Add regMEM reg0 regF , Load (ImmValue (2)) regD , Compute Add regMEM regD regMEM , Pop regA , Store regA (IndAddr regE ), Compute Incr regE reg0 regE , Pop regA , Store regA (IndAddr regE ), Compute Incr regE reg0 regE , Push regF 
	, --end of array creation
	--array creation of: [1]
	Load (ImmValue (1)) regB , Push regB 
	, Compute Add regMEM reg0 regC , Compute Add regMEM reg0 regA , Load (ImmValue (1)) regE , Compute Add regMEM regE regMEM , Pop regG , Store regG (IndAddr regC ), Compute Incr regC reg0 regC , Push regA 
	, --end of array creation
	Compute Add regMEM reg0 regD , Compute Add regMEM reg0 regB , Load (ImmValue (2)) regG , Compute Add regMEM regG regMEM , Pop regF , Store regF (IndAddr regD ), Compute Incr regD reg0 regD , Pop regF , Store regF (IndAddr regD ), Compute Incr regD reg0 regD , Push regB 
	, --end of array creation
	Pop regC , Load (ImmValue (1)) regA , Compute Add regARP regA regA , Store regC (IndAddr regA )
	, -- visitIdExpr with type: int
	-- idCall to iaa[1][1]
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regE regF regE , Push regE 
	, -- accesser: [1][1]
	Load (ImmValue (1)) regG , Push regG 
	, Pop regB , Pop regD , Load (IndAddr regD ) regD , Compute Add regD regB regD , Push regD , Load (ImmValue (1)) regC , Push regC 
	, Pop regB , Pop regD , Load (IndAddr regD ) regD , Compute Add regD regB regD , Push regD , -- end accesser: [1][1]
	-- end idCall to iaa[1][1]
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regE , WriteInstr regE numberIO
	, EndProg ]

main = run [prog]