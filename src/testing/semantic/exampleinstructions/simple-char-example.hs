module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regG , Compute Equal regG reg0 regC , Branch regC (Rel (-3)), Jump (Ind regG ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regA , Compute Add regA reg0 regARP , Load (ImmValue (3)) regE , Compute Add regMEM regE regMEM , Load (ImmValue (97)) regF , Push regF 
	, Pop regD , WriteInstr regD charIO, Load (ImmValue (10)) regD , WriteInstr regD charIO
	, EndProg ]

main = run [prog]