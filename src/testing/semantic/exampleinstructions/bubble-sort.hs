module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regF , Compute Equal regF reg0 regG , Branch regG (Rel (-3)), Jump (Ind regF ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regD , Compute Add regD reg0 regARP , Load (ImmValue (7)) regA , Compute Add regMEM regA regMEM , --array creation of: [5,4,3,2,1]
	Load (ImmValue (1)) regC , Push regC 
	, Load (ImmValue (2)) regB , Push regB 
	, Load (ImmValue (3)) regE , Push regE 
	, Load (ImmValue (4)) regF , Push regF 
	, Load (ImmValue (5)) regG , Push regG 
	, Compute Add regMEM reg0 regA , Compute Add regMEM reg0 regC , Load (ImmValue (5)) regB , Compute Add regMEM regB regMEM , Pop regD , Store regD (IndAddr regA ), Compute Incr regA reg0 regA , Pop regD , Store regD (IndAddr regA ), Compute Incr regA reg0 regA , Pop regD , Store regD (IndAddr regA ), Compute Incr regA reg0 regA , Pop regD , Store regD (IndAddr regA ), Compute Incr regA reg0 regA , Pop regD , Store regD (IndAddr regA ), Compute Incr regA reg0 regA , Push regC 
	, --end of array creation
	Pop regE , Load (ImmValue (1)) regF , Compute Add regARP regF regF , Store regE (IndAddr regF )
	, Load (ImmValue (5)) regG , Push regG 
	, Pop regD , Load (ImmValue (2)) regA , Compute Add regARP regA regA , Store regD (IndAddr regA )
	, -- visitIdExpr with type: int
	-- idCall to len
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regB , Load (ImmValue (2)) regB , Compute Add regC regB regC , Push regC 
	, -- end idCall to len
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Pop regF , Load (ImmValue (3)) regG , Compute Add regARP regG regG , Store regF (IndAddr regG )
	, Load (ImmValue (1)) regD , Push regD 
	, Pop regA , Load (ImmValue (4)) regC , Compute Add regARP regC regC , Store regA (IndAddr regC )
	, -- visitIdExpr with type: boolean
	-- idCall to swapped
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regG , Load (ImmValue (4)) regG , Compute Add regF regG regF , Push regF 
	, -- end idCall to swapped
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: boolean
	Pop regB , Load (ImmValue (1)) regE , Compute Xor regB regE regB , Branch regB (Rel (344))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regA , Compute Add regMEM regA regMEM , -- end prologueScope...
	Load (ImmValue (0)) regC , Push regC 
	, Pop regB , -- idCall to swapped
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regD , Compute Sub regG regD regG , Load (IndAddr regG ) regG , Load (ImmValue (4)) regD , Compute Add regG regD regG , Push regG 
	, -- end idCall to swapped
	Pop regE , Store regB (IndAddr regE ), -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	Load (ImmValue (0)) regD , Push regD 
	, Pop regB , Load (ImmValue (1)) regE , Compute Add regARP regE regE , Store regB (IndAddr regE )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regF regA regF , Push regF 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regD , Compute Sub regG regD regG , Load (IndAddr regG ) regG , Compute Sub regG regD regG , Load (IndAddr regG ) regG , Load (ImmValue (3)) regD , Compute Add regG regD regG , Push regG 
	, -- end idCall to n
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regE , Compute Lt regE regF regE , Push regE 
	, Pop regA , Load (ImmValue (1)) regC , Compute Xor regA regC regA , Branch regA (Rel (253))
	, -- visitIdExpr with type: int
	-- idCall to A[i-1]
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regD , Compute Sub regG regD regG , Load (IndAddr regG ) regG , Compute Sub regG regD regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regD , Compute Add regG regD regG , Push regG 
	, -- accesser: [i-1]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regF regA regF , Push regF 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regG , Push regG 
	, Pop regF , Pop regD , Compute Sub regD regF regD , Push regD 
	, Pop regE , Pop regB , Load (IndAddr regB ) regB , Compute Add regB regE regB , Push regB , -- end accesser: [i-1]
	-- end idCall to A[i-1]
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to A[i]
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regG , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Load (ImmValue (1)) regG , Compute Add regC regG regC , Push regC 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regE regB regE , Push regE 
	, -- end idCall to i
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regD , Load (IndAddr regD ) regD , Compute Add regD regF regD , Push regD , -- end accesser: [i]
	-- end idCall to A[i]
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regE , Pop regG , Compute Gt regG regE regG , Push regG 
	, Pop regA , Load (ImmValue (1)) regC , Compute Xor regA regC regA , Branch regA (Rel (166))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regB , Compute Add regMEM regB regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to A[i]
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Compute Sub regA regF regA , Load (IndAddr regA ) regA , Compute Sub regA regF regA , Load (IndAddr regA ) regA , Compute Sub regA regF regA , Load (IndAddr regA ) regA , Load (ImmValue (1)) regF , Compute Add regA regF regA , Push regA 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regE , Compute Sub regG regE regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regE , Compute Add regG regE regG , Push regG 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regC , Pop regD , Load (IndAddr regD ) regD , Compute Add regD regC regD , Push regD , -- end accesser: [i]
	-- end idCall to A[i]
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regF , Load (ImmValue (1)) regG , Compute Add regARP regG regG , Store regF (IndAddr regG )
	, -- visitIdExpr with type: int
	-- idCall to A[i-1]
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regB , Compute Sub regE regB regE , Load (IndAddr regE ) regE , Compute Sub regE regB regE , Load (IndAddr regE ) regE , Compute Sub regE regB regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regB , Compute Add regE regB regE , Push regE 
	, -- accesser: [i-1]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Compute Sub regA regF regA , Load (IndAddr regA ) regA , Load (ImmValue (1)) regF , Compute Add regA regF regA , Push regA 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regE , Push regE 
	, Pop regA , Pop regB , Compute Sub regB regA regB , Push regB 
	, Pop regD , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regD regC , Push regC , -- end accesser: [i-1]
	-- end idCall to A[i-1]
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regG , -- idCall to A[i]
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regD , Compute Sub regA regD regA , Load (IndAddr regA ) regA , Compute Sub regA regD regA , Load (IndAddr regA ) regA , Compute Sub regA regD regA , Load (IndAddr regA ) regA , Load (ImmValue (1)) regD , Compute Add regA regD regA , Push regA 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regD , Compute Sub regA regD regA , Load (IndAddr regA ) regA , Load (ImmValue (1)) regD , Compute Add regA regD regA , Push regA 
	, -- end idCall to i
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regC , Load (IndAddr regC ) regC , Compute Add regC regF regC , Push regC , -- end accesser: [i]
	-- end idCall to A[i]
	Pop regE , Store regG (IndAddr regE ), -- visitIdExpr with type: int
	-- idCall to temp
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regD regA regD , Push regD 
	, -- end idCall to temp
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regC , -- idCall to A[i-1]
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regD , Compute Sub regB regD regB , Load (IndAddr regB ) regB , Compute Sub regB regD regB , Load (IndAddr regB ) regB , Compute Sub regB regD regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regD , Compute Add regB regD regB , Push regB 
	, -- accesser: [i-1]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regD , Compute Sub regB regD regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regD , Compute Add regB regD regB , Push regB 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regD , Push regD 
	, Pop regD , Pop regB , Compute Sub regB regD regB , Push regB 
	, Pop regF , Pop regA , Load (IndAddr regA ) regA , Compute Add regA regF regA , Push regA , -- end accesser: [i-1]
	-- end idCall to A[i-1]
	Pop regG , Store regC (IndAddr regG ), Load (ImmValue (1)) regB , Push regB 
	, Pop regD , -- idCall to swapped
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regG , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Compute Sub regC regG regC , Load (IndAddr regC ) regC , Load (ImmValue (4)) regG , Compute Add regC regG regC , Push regC 
	, -- end idCall to swapped
	Pop regF , Store regD (IndAddr regF ), -- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regB regC regB , Push regB 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regD , Push regD 
	, Pop regA , Pop regF , Compute Add regF regA regF , Push regF 
	, Pop regE , -- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regG regD regG , Push regG 
	, -- end idCall to i
	Pop regB , Store regE (IndAddr regB ), Jump (Rel (-279))
	, -- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regG , Compute Sub regA regG regA , Load (IndAddr regA ) regA , Load (ImmValue (3)) regG , Compute Add regA regG regA , Push regA 
	, -- end idCall to n
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regE , Push regE 
	, Pop regC , Pop regB , Compute Sub regB regC regB , Push regB 
	, Pop regF , -- idCall to n
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regE , Compute Sub regD regE regD , Load (IndAddr regD ) regD , Load (ImmValue (3)) regE , Compute Add regD regE regD , Push regD 
	, -- end idCall to n
	Pop regA , Store regF (IndAddr regA ), -- epilogueScope
	Load (IndAddr regARP ) regB , Compute Add regB reg0 regARP , -- end of epilogueScope
	Jump (Rel (-354))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regE , Compute Add regMEM regE regMEM , -- end prologueScope...
	Load (ImmValue (0)) regF , Push regF 
	, Pop regA , Load (ImmValue (1)) regG , Compute Add regARP regG regG , Store regA (IndAddr regG )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regB regC regB , Push regB 
	, -- end idCall to i
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to len
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regF , Compute Sub regE regF regE , Load (IndAddr regE ) regE , Load (ImmValue (2)) regF , Compute Add regE regF regE , Push regE 
	, -- end idCall to len
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regB , Pop regG , Compute Lt regG regB regG , Push regG 
	, Pop regC , Load (ImmValue (1)) regD , Compute Xor regC regD regC , Branch regC (Rel (49))
	, -- visitIdExpr with type: int
	-- idCall to A[i]
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regD , Compute Sub regC regD regC , Load (IndAddr regC ) regC , Load (ImmValue (1)) regD , Compute Add regC regD regC , Push regC 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regA regG regA , Push regA 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regE , Load (IndAddr regE ) regE , Compute Add regE regF regE , Push regE , -- end accesser: [i]
	-- end idCall to A[i]
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regD , WriteInstr regD numberIO
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regA regG regA , Push regA 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regF , Push regF 
	, Pop regC , Pop regE , Compute Add regE regC regE , Push regE 
	, Pop regD , -- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regB regF regB , Push regB 
	, -- end idCall to i
	Pop regA , Store regD (IndAddr regA ), Jump (Rel (-73))
	, -- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	EndProg ]

main = run [prog]