module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regG , Compute Equal regG reg0 regF , Branch regF (Rel (-3)), Jump (Ind regG ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regB , Compute Add regB reg0 regARP , Load (ImmValue (3)) regA , Compute Add regMEM regA regMEM , -- Function 'isPrime' in scope: [0]
	Jump (Rel (304)), -- prologue...
	Load (ImmValue (3)) regC , Compute Add regMEM regC regMEM , Compute Add reg0 regARP regE , Compute Incr regE reg0 regE , Pop regD , Store regD (IndAddr regE ), -- end prologue...
	Load (ImmValue (0)) regG , Push regG 
	, Pop regF , Load (ImmValue (2)) regB , Compute Add regARP regB regB , Store regF (IndAddr regB )
	, Load (ImmValue (0)) regA , Push regA 
	, Pop regD , Load (ImmValue (3)) regE , Compute Add regARP regE regE , Store regD (IndAddr regE )
	, -- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regF regB regF , Push regF 
	, -- end idCall to n
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regD , Push regD 
	, Pop regC , Pop regE , Compute LtE regE regC regE , Push regE 
	, Pop regC , Load (ImmValue (1)) regG , Compute Xor regC regG regC , Branch regC (Rel (34))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	Load (ImmValue (1)) regF , Push regF 
	, Pop regB , -- idCall to gotit
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regC , Compute Sub regE regC regE , Load (IndAddr regE ) regE , Load (ImmValue (3)) regC , Compute Add regE regC regE , Push regE 
	, -- end idCall to gotit
	Pop regA , Store regB (IndAddr regA ), Load (ImmValue (0)) regG , Push regG 
	, Pop regF , -- idCall to result
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regA , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Load (ImmValue (2)) regA , Compute Add regB regA regB , Push regB 
	, -- end idCall to result
	Pop regE , Store regF (IndAddr regE ), -- epilogueScope
	Load (IndAddr regARP ) regD , Compute Add regD reg0 regARP , -- end of epilogueScope
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regA , Compute Add regMEM regA regMEM , -- end prologueScope...
	Load (ImmValue (2)) regF , Push regF 
	, Pop regE , Load (ImmValue (1)) regC , Compute Add regARP regC regC , Store regE (IndAddr regC )
	, -- visitIdExpr with type: boolean
	-- idCall to gotit
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regG , Compute Sub regD regG regD , Load (IndAddr regD ) regD , Load (ImmValue (3)) regG , Compute Add regD regG regD , Push regD 
	, -- end idCall to gotit
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: boolean
	Pop regA , Load (ImmValue (1)) regF , Compute Xor regA regF regA , Push regA , -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regE regC regE , Push regE 
	, -- end idCall to i
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regB , Compute Add regG regB regG , Push regG 
	, -- end idCall to n
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regE , Pop regF , Compute Lt regF regE regF , Push regF 
	, Pop regD , Pop regC , Compute And regC regD regC , Push regC 
	, Pop regG , Load (ImmValue (1)) regB , Compute Xor regG regB regG , Branch regG (Rel (125))
	, -- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Compute Sub regA regF regA , Load (IndAddr regA ) regA , Load (ImmValue (1)) regF , Compute Add regA regF regA , Push regA 
	, -- end idCall to n
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regC regD regC , Push regC 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to n
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regA , Compute Sub regB regA regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regA , Compute Add regB regA regB , Push regB 
	, -- end idCall to n
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regE regC regE , Push regE 
	, -- end idCall to i
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Pop regB , Pop regG , Load (ImmValue (0)) regF , Compute Lt regG regB regA , Branch regA (Rel (5)), Compute Sub regG regB regG , Load (ImmValue (1)) regA , Compute Add regF regA regF , Jump (Rel (-5)), Push regF 
	, Pop regC , Pop regE , Compute Mul regE regC regE , Push regE 
	, Pop regG , Pop regD , Compute Sub regD regG regD , Push regD 
	, Load (ImmValue (0)) regB , Push regB 
	, Pop regF , Pop regA , Compute Equal regA regF regA , Push regA 
	, Pop regG , Load (ImmValue (1)) regB , Compute Xor regG regB regG , Branch regG (Rel (38))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regE , Compute Add regMEM regE regMEM , -- end prologueScope...
	Load (ImmValue (1)) regC , Push regC 
	, Pop regD , -- idCall to gotit
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Compute Sub regA regF regA , Load (IndAddr regA ) regA , Compute Sub regA regF regA , Load (IndAddr regA ) regA , Load (ImmValue (3)) regF , Compute Add regA regF regA , Push regA 
	, -- end idCall to gotit
	Pop regG , Store regD (IndAddr regG ), Load (ImmValue (0)) regE , Push regE 
	, Pop regC , -- idCall to result
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regG , Compute Sub regD regG regD , Load (IndAddr regD ) regD , Compute Sub regD regG regD , Load (IndAddr regD ) regD , Load (ImmValue (2)) regG , Compute Add regD regG regD , Push regD 
	, -- end idCall to result
	Pop regA , Store regC (IndAddr regA ), -- epilogueScope
	Load (IndAddr regARP ) regB , Compute Add regB reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regE regD regE , Push regE 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regF , Pop regA , Compute Add regA regF regA , Push regA 
	, Pop regB , -- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regG regC regG , Push regG 
	, -- end idCall to i
	Pop regE , Store regB (IndAddr regE ), Jump (Rel (-167))
	, -- epilogueScope
	Load (IndAddr regARP ) regA , Compute Add regA reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: boolean
	-- idCall to gotit
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regB , Load (ImmValue (3)) regB , Compute Add regC regB regC , Push regC 
	, -- end idCall to gotit
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: boolean
	Pop regD , Load (ImmValue (1)) regA , Compute Xor regD regA regD , Push regD , Pop regF , Load (ImmValue (1)) regG , Compute Xor regF regG regF , Branch regF (Rel (22))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regF , Compute Add regMEM regF regMEM , -- end prologueScope...
	Load (ImmValue (1)) regG , Push regG 
	, Pop regC , -- idCall to result
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regA , Compute Sub regD regA regD , Load (IndAddr regD ) regD , Load (ImmValue (2)) regA , Compute Add regD regA regD , Push regD 
	, -- end idCall to result
	Pop regB , Store regC (IndAddr regB ), -- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	--return returnresult;
	-- visitIdExpr with type: boolean
	-- idCall to result
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regD , Load (ImmValue (2)) regD , Compute Add regG regD regG , Push regG 
	, -- end idCall to result
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: boolean
	-- epilogue
	Load (IndAddr regARP ) regC , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regB , Compute Add regC reg0 regARP , Jump (Ind regB )-- end of epilogue
	, -- end return
-- End of function 'isPrime' in scope: [0]
	-- function call to 'isPrime'
	Load (ImmValue (2)) regE , Push regE 
	, -- precall
	Load (ImmValue (328)) regF , Store regF (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regG ] ++ (writeBool regG )
	++ [-- function call to 'isPrime'
	Load (ImmValue (3)) regD , Push regD 
	, -- precall
	Load (ImmValue (364)) regA , Store regA (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regC ] ++ (writeBool regC )
	++ [-- function call to 'isPrime'
	Load (ImmValue (4)) regB , Push regB 
	, -- precall
	Load (ImmValue (400)) regE , Store regE (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regF ] ++ (writeBool regF )
	++ [-- function call to 'isPrime'
	Load (ImmValue (5)) regG , Push regG 
	, -- precall
	Load (ImmValue (436)) regD , Store regD (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regA ] ++ (writeBool regA )
	++ [-- function call to 'isPrime'
	Load (ImmValue (6)) regC , Push regC 
	, -- precall
	Load (ImmValue (472)) regB , Store regB (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regE ] ++ (writeBool regE )
	++ [-- function call to 'isPrime'
	Load (ImmValue (7)) regF , Push regF 
	, -- precall
	Load (ImmValue (508)) regG , Store regG (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regD ] ++ (writeBool regD )
	++ [-- function call to 'isPrime'
	Load (ImmValue (8)) regA , Push regA 
	, -- precall
	Load (ImmValue (544)) regC , Store regC (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regB ] ++ (writeBool regB )
	++ [-- function call to 'isPrime'
	Load (ImmValue (9)) regE , Push regE 
	, -- precall
	Load (ImmValue (580)) regF , Store regF (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regG ] ++ (writeBool regG )
	++ [-- function call to 'isPrime'
	Load (ImmValue (10)) regD , Push regD 
	, -- precall
	Load (ImmValue (616)) regA , Store regA (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regC ] ++ (writeBool regC )
	++ [-- function call to 'isPrime'
	Load (ImmValue (11)) regB , Push regB 
	, -- precall
	Load (ImmValue (652)) regE , Store regE (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regF ] ++ (writeBool regF )
	++ [-- function call to 'isPrime'
	Load (ImmValue (12)) regG , Push regG 
	, -- precall
	Load (ImmValue (688)) regD , Store regD (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regA ] ++ (writeBool regA )
	++ [-- function call to 'isPrime'
	Load (ImmValue (13)) regC , Push regC 
	, -- precall
	Load (ImmValue (724)) regB , Store regB (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regE ] ++ (writeBool regE )
	++ [-- function call to 'isPrime'
	Load (ImmValue (14)) regF , Push regF 
	, -- precall
	Load (ImmValue (760)) regG , Store regG (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regD ] ++ (writeBool regD )
	++ [-- function call to 'isPrime'
	Load (ImmValue (15)) regA , Push regA 
	, -- precall
	Load (ImmValue (796)) regC , Store regC (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regB ] ++ (writeBool regB )
	++ [-- function call to 'isPrime'
	Load (ImmValue (16)) regE , Push regE 
	, -- precall
	Load (ImmValue (832)) regF , Store regF (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regG ] ++ (writeBool regG )
	++ [-- function call to 'isPrime'
	Load (ImmValue (17)) regD , Push regD 
	, -- precall
	Load (ImmValue (868)) regA , Store regA (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regC ] ++ (writeBool regC )
	++ [-- function call to 'isPrime'
	Load (ImmValue (18)) regB , Push regB 
	, -- precall
	Load (ImmValue (904)) regE , Store regE (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regF ] ++ (writeBool regF )
	++ [-- function call to 'isPrime'
	Load (ImmValue (19)) regG , Push regG 
	, -- precall
	Load (ImmValue (940)) regD , Store regD (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regA ] ++ (writeBool regA )
	++ [-- function call to 'isPrime'
	Load (ImmValue (20)) regC , Push regC 
	, -- precall
	Load (ImmValue (976)) regB , Store regB (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'isPrime'
	Pop regE ] ++ (writeBool regE )
	++ [EndProg ]

main = run [prog]