module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regG , Compute Equal regG reg0 regE , Branch regE (Rel (-3)), Jump (Ind regG ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regD , Compute Add regD reg0 regARP , Load (ImmValue (7)) regC , Compute Add regMEM regC regMEM , Push regMEM , Load (ImmValue (3)) regA , Compute Add regMEM regA regMEM , Pop regF , Load (ImmValue (1)) regB , Compute Add regARP regB regB , Store regF (IndAddr regB )
	, Load (ImmValue (0)) regG , Push regG 
	, Pop regE , -- idCall to list.v
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regA regF regA , Push regA 
	, -- accesser: .v
	Pop regB , Load (IndAddr regB ) regB , Load (ImmValue (2)) regG , Compute Add regG regB regB , Push regB , -- end accesser: .v
	-- end idCall to list.v
	Pop regD , Store regE (IndAddr regD ), -- visitIdExpr with type: Node
	-- idCall to list
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regA regF regA , Push regA 
	, -- end idCall to list
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: Node
	Pop regB , Load (ImmValue (2)) regE , Compute Add regARP regE regE , Store regB (IndAddr regE )
	, Push reg0 , Pop regD , Load (ImmValue (3)) regC , Compute Add regARP regC regC , Store regD (IndAddr regC )
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	Load (ImmValue (1)) regB , Push regB 
	, Pop regE , Load (ImmValue (1)) regD , Compute Add regARP regD regD , Store regE (IndAddr regD )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regC regA regC , Push regC 
	, -- end idCall to i
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (10)) regG , Push regG 
	, Pop regE , Pop regB , Compute Lt regB regE regB , Push regB 
	, Pop regA , Load (ImmValue (1)) regF , Compute Xor regA regF regA , Branch regA (Rel (130))
	, Push regMEM , Load (ImmValue (3)) regD , Compute Add regMEM regD regMEM , Pop regC , -- idCall to n2
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Compute Sub regG regB regG , Load (IndAddr regG ) regG , Load (ImmValue (3)) regB , Compute Add regG regB regG , Push regG 
	, -- end idCall to n2
	Pop regA , Store regC (IndAddr regA ), -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regE regD regE , Push regE 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Pop regB , -- idCall to n2.v
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regE , Compute Sub regF regE regF , Load (IndAddr regF ) regF , Load (ImmValue (3)) regE , Compute Add regF regE regF , Push regF 
	, -- accesser: .v
	Pop regD , Load (IndAddr regD ) regD , Load (ImmValue (2)) regG , Compute Add regG regD regD , Push regD , -- end accesser: .v
	-- end idCall to n2.v
	Pop regC , Store regB (IndAddr regC ), -- visitIdExpr with type: Node
	-- idCall to n2
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regE , Compute Sub regF regE regF , Load (IndAddr regF ) regF , Load (ImmValue (3)) regE , Compute Add regF regE regF , Push regF 
	, -- end idCall to n2
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: Node
	Pop regD , -- idCall to n.next
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Compute Sub regA regF regA , Load (IndAddr regA ) regA , Load (ImmValue (2)) regF , Compute Add regA regF regA , Push regA 
	, -- accesser: .next
	Pop regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regG , Compute Add regG regE regE , Push regE , -- end accesser: .next
	-- end idCall to n.next
	Pop regB , Store regD (IndAddr regB ), -- visitIdExpr with type: Node
	-- idCall to n
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Compute Sub regA regF regA , Load (IndAddr regA ) regA , Load (ImmValue (2)) regF , Compute Add regA regF regA , Push regA 
	, -- end idCall to n
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: Node
	Pop regE , -- idCall to n2.prev
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regA , Compute Sub regC regA regC , Load (IndAddr regC ) regC , Load (ImmValue (3)) regA , Compute Add regC regA regC , Push regC 
	, -- accesser: .prev
	Pop regF , Load (IndAddr regF ) regF , Load (ImmValue (0)) regG , Compute Add regG regF regF , Push regF , -- end accesser: .prev
	-- end idCall to n2.prev
	Pop regD , Store regE (IndAddr regD ), -- visitIdExpr with type: Node
	-- idCall to n2
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regA , Compute Sub regC regA regC , Load (IndAddr regC ) regC , Load (ImmValue (3)) regA , Compute Add regC regA regC , Push regC 
	, -- end idCall to n2
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: Node
	Pop regF , -- idCall to n
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Compute Sub regB regC regB , Load (IndAddr regB ) regB , Load (ImmValue (2)) regC , Compute Add regB regC regB , Push regB 
	, -- end idCall to n
	Pop regE , Store regF (IndAddr regE ), -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regA regG regA , Push regA 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regE , Pop regF , Compute Add regF regE regF , Push regF 
	, Pop regD , -- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regB regC regB , Push regB 
	, -- end idCall to i
	Pop regA , Store regD (IndAddr regA ), Jump (Rel (-146))
	, -- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	-- Function 'get' in scope: [0]
	Jump (Rel (125)), -- prologue...
	Load (ImmValue (3)) regC , Compute Add regMEM regC regMEM , Compute Add reg0 regARP regB , Compute Incr regB reg0 regB , Pop regE , Store regE (IndAddr regB ), Compute Incr regB reg0 regB , Pop regE , Store regE (IndAddr regB ), -- end prologue...
	-- visitIdExpr with type: Node
	-- idCall to list
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regD regA regD , Push regD 
	, -- end idCall to list
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: Node
	Pop regF , Load (ImmValue (3)) regE , Compute Add regARP regE regE , Store regF (IndAddr regE )
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regD , Compute Add regMEM regD regMEM , -- end prologueScope...
	Load (ImmValue (0)) regA , Push regA 
	, Pop regG , Load (ImmValue (1)) regF , Compute Add regARP regF regF , Store regG (IndAddr regF )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regE regB regE , Push regE 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to index
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regA , Compute Sub regD regA regD , Load (IndAddr regD ) regD , Load (ImmValue (2)) regA , Compute Add regD regA regD , Push regD 
	, -- end idCall to index
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Pop regE , Pop regF , Compute Lt regF regE regF , Push regF 
	, Pop regB , Load (ImmValue (1)) regC , Compute Xor regB regC regB , Branch regB (Rel (49))
	, -- visitIdExpr with type: Node
	-- idCall to res.next
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Compute Sub regB regC regB , Load (IndAddr regB ) regB , Load (ImmValue (3)) regC , Compute Add regB regC regB , Push regB 
	, -- accesser: .next
	Pop regD , Load (IndAddr regD ) regD , Load (ImmValue (1)) regA , Compute Add regA regD regD , Push regD , -- end accesser: .next
	-- end idCall to res.next
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: Node
	Pop regF , -- idCall to res
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regA , Compute Sub regC regA regC , Load (IndAddr regC ) regC , Load (ImmValue (3)) regA , Compute Add regC regA regC , Push regC 
	, -- end idCall to res
	Pop regE , Store regF (IndAddr regE ), -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regD regG regD , Push regD 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regA , Push regA 
	, Pop regE , Pop regF , Compute Add regF regE regF , Push regF 
	, Pop regB , -- idCall to i
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regC regA regC , Push regC 
	, -- end idCall to i
	Pop regD , Store regB (IndAddr regD ), Jump (Rel (-73))
	, -- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	--return returnres;
	-- visitIdExpr with type: Node
	-- idCall to res
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regC , Load (ImmValue (3)) regC , Compute Add regE regC regE , Push regE 
	, -- end idCall to res
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: Node
	-- epilogue
	Load (IndAddr regARP ) regB , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regD , Compute Add regB reg0 regARP , Jump (Ind regD )-- end of epilogue
	, -- end return
-- End of function 'get' in scope: [0]
	-- function call to 'get'
	Load (ImmValue (5)) regG , Push regG 
	, -- visitIdExpr with type: Node
	-- idCall to list
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regE , Load (ImmValue (1)) regE , Compute Add regF regE regF , Push regF 
	, -- end idCall to list
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: Node
	-- precall
	Load (ImmValue (358)) regA , Store regA (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (215))
	, -- function call ended for 'get'
	Pop regB , Load (ImmValue (4)) regD , Compute Add regARP regD regD , Store regB (IndAddr regD )
	, -- visitIdExpr with type: int
	-- idCall to res.v
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regF , Load (ImmValue (4)) regF , Compute Add regG regF regG , Push regG 
	, -- accesser: .v
	Pop regE , Load (IndAddr regE ) regE , Load (ImmValue (2)) regC , Compute Add regC regE regE , Push regE , -- end accesser: .v
	-- end idCall to res.v
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regB , WriteInstr regB numberIO
	, EndProg ]

main = run [prog]