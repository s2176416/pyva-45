module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regC , Compute Equal regC reg0 regB , Branch regB (Rel (-3)), Jump (Ind regC ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regF , Compute Add regF reg0 regARP , Load (ImmValue (5)) regD , Compute Add regMEM regD regMEM , --array creation of: [5,4,3,2,1]
	Load (ImmValue (1)) regA , Push regA 
	, Load (ImmValue (2)) regG , Push regG 
	, Load (ImmValue (3)) regE , Push regE 
	, Load (ImmValue (4)) regC , Push regC 
	, Load (ImmValue (5)) regB , Push regB 
	, Compute Add regMEM reg0 regD , Compute Add regMEM reg0 regA , Load (ImmValue (5)) regG , Compute Add regMEM regG regMEM , Pop regF , Store regF (IndAddr regD ), Compute Incr regD reg0 regD , Pop regF , Store regF (IndAddr regD ), Compute Incr regD reg0 regD , Pop regF , Store regF (IndAddr regD ), Compute Incr regD reg0 regD , Pop regF , Store regF (IndAddr regD ), Compute Incr regD reg0 regD , Pop regF , Store regF (IndAddr regD ), Compute Incr regD reg0 regD , Push regA 
	, --end of array creation
	Pop regE , Load (ImmValue (1)) regC , Compute Add regARP regC regC , Store regE (IndAddr regC )
	, Load (ImmValue (5)) regB , Push regB 
	, Pop regF , Load (ImmValue (2)) regD , Compute Add regARP regD regD , Store regF (IndAddr regD )
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regE , Compute Add regMEM regE regMEM , -- end prologueScope...
	Load (ImmValue (0)) regC , Push regC 
	, Pop regB , Load (ImmValue (1)) regF , Compute Add regARP regF regF , Store regB (IndAddr regF )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regD regA regD , Push regD 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to len
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regC , Compute Sub regE regC regE , Load (IndAddr regE ) regE , Load (ImmValue (2)) regC , Compute Add regE regC regE , Push regE 
	, -- end idCall to len
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regD , Pop regF , Compute Lt regF regD regF , Push regF 
	, Pop regA , Load (ImmValue (1)) regG , Compute Xor regA regG regA , Branch regA (Rel (276))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (2)) regE , Compute Add regMEM regE regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regB , Compute Sub regC regB regC , Load (IndAddr regC ) regC , Load (ImmValue (1)) regB , Compute Add regC regB regC , Push regC 
	, -- end idCall to i
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regD , Load (ImmValue (1)) regA , Compute Add regARP regA regA , Store regD (IndAddr regA )
	, -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regE , Load (ImmValue (1)) regE , Compute Add regG regE regG , Push regG 
	, -- end idCall to j
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (0)) regB , Push regB 
	, Pop regD , Pop regF , Compute Gt regF regD regF , Push regF 
	, -- visitIdExpr with type: int
	-- idCall to A[j-1]
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regG , Compute Sub regA regG regA , Load (IndAddr regA ) regA , Compute Sub regA regG regA , Load (IndAddr regA ) regA , Load (ImmValue (1)) regG , Compute Add regA regG regA , Push regA 
	, -- accesser: [j-1]
	-- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regB regF regB , Push regB 
	, -- end idCall to j
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regA , Push regA 
	, Pop regB , Pop regG , Compute Sub regG regB regG , Push regG 
	, Pop regC , Pop regE , Load (IndAddr regE ) regE , Compute Add regE regC regE , Push regE , -- end accesser: [j-1]
	-- end idCall to A[j-1]
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to A[j]
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regA , Compute Sub regD regA regD , Load (IndAddr regD ) regD , Compute Sub regD regA regD , Load (IndAddr regD ) regD , Load (ImmValue (1)) regA , Compute Add regD regA regD , Push regD 
	, -- accesser: [j]
	-- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regE , Load (ImmValue (1)) regE , Compute Add regC regE regC , Push regC 
	, -- end idCall to j
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regB , Pop regG , Load (IndAddr regG ) regG , Compute Add regG regB regG , Push regG , -- end accesser: [j]
	-- end idCall to A[j]
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Pop regC , Pop regA , Compute Gt regA regC regA , Push regA 
	, Pop regF , Pop regE , Compute And regE regF regE , Push regE 
	, Pop regA , Load (ImmValue (1)) regG , Compute Xor regA regG regA , Branch regA (Rel (148))
	, -- visitIdExpr with type: int
	-- idCall to A[j]
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regG , Compute Sub regB regG regB , Load (IndAddr regB ) regB , Compute Sub regB regG regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regG , Compute Add regB regG regB , Push regB 
	, -- accesser: [j]
	-- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regE , Load (ImmValue (1)) regE , Compute Add regC regE regC , Push regC 
	, -- end idCall to j
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regA , Pop regD , Load (IndAddr regD ) regD , Compute Add regD regA regD , Push regD , -- end accesser: [j]
	-- end idCall to A[j]
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regG , Load (ImmValue (2)) regC , Compute Add regARP regC regC , Store regG (IndAddr regC )
	, -- visitIdExpr with type: int
	-- idCall to A[j-1]
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regF , Compute Sub regE regF regE , Load (IndAddr regE ) regE , Compute Sub regE regF regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regF , Compute Add regE regF regE , Push regE 
	, -- accesser: [j-1]
	-- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regB regG regB , Push regB 
	, -- end idCall to j
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regE , Push regE 
	, Pop regB , Pop regF , Compute Sub regF regB regF , Push regF 
	, Pop regD , Pop regA , Load (IndAddr regA ) regA , Compute Add regA regD regA , Push regA , -- end accesser: [j-1]
	-- end idCall to A[j-1]
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Pop regC , -- idCall to A[j]
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regD , Compute Sub regB regD regB , Load (IndAddr regB ) regB , Compute Sub regB regD regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regD , Compute Add regB regD regB , Push regB 
	, -- accesser: [j]
	-- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regB regD regB , Push regB 
	, -- end idCall to j
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regG , Pop regA , Load (IndAddr regA ) regA , Compute Add regA regG regA , Push regA , -- end accesser: [j]
	-- end idCall to A[j]
	Pop regE , Store regC (IndAddr regE ), -- visitIdExpr with type: int
	-- idCall to temp
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regB , Load (ImmValue (2)) regB , Compute Add regD regB regD , Push regD 
	, -- end idCall to temp
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Pop regA , -- idCall to A[j-1]
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regD , Compute Sub regF regD regF , Load (IndAddr regF ) regF , Compute Sub regF regD regF , Load (IndAddr regF ) regF , Load (ImmValue (1)) regD , Compute Add regF regD regF , Push regF 
	, -- accesser: [j-1]
	-- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regF regD regF , Push regF 
	, -- end idCall to j
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regD , Push regD 
	, Pop regD , Pop regF , Compute Sub regF regD regF , Push regF 
	, Pop regG , Pop regB , Load (IndAddr regB ) regB , Compute Add regB regG regB , Push regB , -- end accesser: [j-1]
	-- end idCall to A[j-1]
	Pop regC , Store regA (IndAddr regC ), -- visitIdExpr with type: int
	-- idCall to j
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regF regD regF , Push regF 
	, -- end idCall to j
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regB , Push regB 
	, Pop regC , Pop regA , Compute Sub regA regC regA , Push regA 
	, Pop regE , -- idCall to j
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regG regB regG , Push regG 
	, -- end idCall to j
	Pop regF , Store regE (IndAddr regF ), Jump (Rel (-228))
	, -- epilogueScope
	Load (IndAddr regARP ) regA , Compute Add regA reg0 regARP , -- end of epilogueScope
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regC regG regC , Push regC 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regE , Push regE 
	, Pop regD , Pop regF , Compute Add regF regD regF , Push regF 
	, Pop regA , -- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regE , Load (ImmValue (1)) regE , Compute Add regB regE regB , Push regB 
	, -- end idCall to i
	Pop regC , Store regA (IndAddr regC ), Jump (Rel (-300))
	, -- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regE , Compute Add regMEM regE regMEM , -- end prologueScope...
	Load (ImmValue (0)) regA , Push regA 
	, Pop regC , Load (ImmValue (1)) regG , Compute Add regARP regG regG , Store regC (IndAddr regG )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regF regD regF , Push regF 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to len
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regA , Compute Sub regE regA regE , Load (IndAddr regE ) regE , Load (ImmValue (2)) regA , Compute Add regE regA regE , Push regE 
	, -- end idCall to len
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regF , Pop regG , Compute Lt regG regF regG , Push regG 
	, Pop regD , Load (ImmValue (1)) regB , Compute Xor regD regB regD , Branch regD (Rel (49))
	, -- visitIdExpr with type: int
	-- idCall to A[i]
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regB , Compute Sub regD regB regD , Load (IndAddr regD ) regD , Load (ImmValue (1)) regB , Compute Add regD regB regD , Push regD 
	, -- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regC regG regC , Push regC 
	, -- end idCall to i
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Pop regA , Pop regE , Load (IndAddr regE ) regE , Compute Add regE regA regE , Push regE , -- end accesser: [i]
	-- end idCall to A[i]
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Pop regB , WriteInstr regB numberIO
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regC regG regC , Push regC 
	, -- end idCall to i
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regA , Push regA 
	, Pop regD , Pop regE , Compute Add regE regD regE , Push regE 
	, Pop regB , -- idCall to i
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regF regA regF , Push regF 
	, -- end idCall to i
	Pop regC , Store regB (IndAddr regC ), Jump (Rel (-73))
	, -- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	EndProg ]

main = run [prog]