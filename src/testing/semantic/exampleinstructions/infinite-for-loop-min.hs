module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regF , Compute Equal regF reg0 regD , Branch regD (Rel (-3)), Jump (Ind regF ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regB , Compute Add regB reg0 regARP , Load (ImmValue (3)) regA , Compute Add regMEM regA regMEM , -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regE , Compute Add regMEM regE regMEM , -- end prologueScope...
	Load (ImmValue (0)) regF , Push regF 
	, Pop regD , Load (ImmValue (1)) regB , Compute Add regARP regB regB , Store regD (IndAddr regB )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regA regG regA , Push regA 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regE , Push regE 
	, Pop regD , Pop regF , Compute Lt regF regD regF , Push regF 
	, Pop regG , Load (ImmValue (1)) regC , Compute Xor regG regC regG , Branch regG (Rel (46))
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regB regA regB , Push regB 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regF , Pop regE , Compute Sub regE regF regE , Push regE 
	, Pop regD , -- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regG regC regG , Push regG 
	, -- end idCall to i
	Pop regB , Store regD (IndAddr regB ), -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regE regF regE , Push regE 
	, -- end idCall to i
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regB , Pop regD , Compute Add regD regB regD , Push regD 
	, Pop regA , -- idCall to i
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regG regC regG , Push regG 
	, -- end idCall to i
	Pop regE , Store regA (IndAddr regE ), Jump (Rel (-62))
	, -- epilogueScope
	Load (IndAddr regARP ) regD , Compute Add regD reg0 regARP , -- end of epilogueScope
	EndProg ]

main = run [prog]