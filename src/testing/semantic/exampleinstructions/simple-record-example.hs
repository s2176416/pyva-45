module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regF , Compute Equal regF reg0 regG , Branch regG (Rel (-3)), Jump (Ind regF ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regC , Compute Add regC reg0 regARP , Load (ImmValue (4)) regB , Compute Add regMEM regB regMEM , Push regMEM , Load (ImmValue (2)) regD , Compute Add regMEM regD regMEM , Pop regA , Load (ImmValue (1)) regE , Compute Add regARP regE regE , Store regA (IndAddr regE )
	, Load (ImmValue (10)) regF , Push regF 
	, Pop regG , -- idCall to c.x
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regD regA regD , Push regD 
	, -- accesser: .x
	Pop regE , Load (IndAddr regE ) regE , Load (ImmValue (0)) regF , Compute Add regF regE regE , Push regE , -- end accesser: .x
	-- end idCall to c.x
	Pop regC , Store regG (IndAddr regC ), Load (ImmValue (100)) regD , Push regD 
	, Pop regA , -- idCall to c.y
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regG regC regG , Push regG 
	, -- accesser: .y
	Pop regB , Load (IndAddr regB ) regB , Load (ImmValue (1)) regD , Compute Add regD regB regB , Push regB , -- end accesser: .y
	-- end idCall to c.y
	Pop regF , Store regA (IndAddr regF ), -- visitIdExpr with type: int
	-- idCall to c.x
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regG regC regG , Push regG 
	, -- accesser: .x
	Pop regD , Load (IndAddr regD ) regD , Load (ImmValue (0)) regB , Compute Add regB regD regD , Push regD , -- end accesser: .x
	-- end idCall to c.x
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regF , WriteInstr regF numberIO
	, -- visitIdExpr with type: int
	-- idCall to c.y
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regG , Load (ImmValue (1)) regG , Compute Add regE regG regE , Push regE 
	, -- accesser: .y
	Pop regC , Load (IndAddr regC ) regC , Load (ImmValue (1)) regB , Compute Add regB regC regC , Push regC , -- end accesser: .y
	-- end idCall to c.y
	Pop regD , Load (IndAddr regD ) regD , Push regD 
	, -- end of visitIdExpr with type: int
	Pop regA , WriteInstr regA numberIO
	, EndProg ]

main = run [prog]