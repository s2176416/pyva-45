module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regC , Compute Equal regC reg0 regA , Branch regA (Rel (-3)), Jump (Ind regC ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regE , Compute Add regE reg0 regARP , Load (ImmValue (3)) regF , Compute Add regMEM regF regMEM , Load (ImmValue (1)) regD , Push regD 
	, Pop regB ] ++ (writeBool regB )
	++ [EndProg ]

main = run [prog]