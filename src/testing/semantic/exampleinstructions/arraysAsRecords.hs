module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regA , Compute Equal regA reg0 regB , Branch regB (Rel (-3)), Jump (Ind regA ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regC , Compute Add regC reg0 regARP , Load (ImmValue (5)) regD , Compute Add regMEM regD regMEM , -- Function 'printInt' in scope: [0]
	Jump (Rel (23)), -- prologue...
	Load (ImmValue (1)) regG , Compute Add regMEM regG regMEM , Compute Add reg0 regARP regF , Compute Incr regF reg0 regF , Pop regE , Store regE (IndAddr regF ), -- end prologue...
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regA regB regA , Push regA 
	, -- end idCall to i
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Pop regD , WriteInstr regD numberIO
	, -- epilogue
	Load (IndAddr regARP ) regE , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regF , Compute Add regE reg0 regARP , Jump (Ind regF )-- end of epilogue
	, -- End of function 'printInt' in scope: [0]
	Push regMEM , Load (ImmValue (2)) regG , Compute Add regMEM regG regMEM , Pop regA , Load (ImmValue (1)) regB , Compute Add regARP regB regB , Store regA (IndAddr regB )
	, Load (ImmValue (10)) regC , Push regC 
	, Pop regD , -- idCall to intArray.length
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regA , Load (ImmValue (1)) regA , Compute Add regG regA regG , Push regG 
	, -- accesser: .length
	Pop regB , Load (IndAddr regB ) regB , Load (ImmValue (0)) regC , Compute Add regC regB regB , Push regB , -- end accesser: .length
	-- end idCall to intArray.length
	Pop regE , Store regD (IndAddr regE ), --array creation of: [1,2,3,4,5,6,7,8,9,10]
	Load (ImmValue (10)) regG , Push regG 
	, Load (ImmValue (9)) regA , Push regA 
	, Load (ImmValue (8)) regC , Push regC 
	, Load (ImmValue (7)) regB , Push regB 
	, Load (ImmValue (6)) regD , Push regD 
	, Load (ImmValue (5)) regE , Push regE 
	, Load (ImmValue (4)) regF , Push regF 
	, Load (ImmValue (3)) regG , Push regG 
	, Load (ImmValue (2)) regA , Push regA 
	, Load (ImmValue (1)) regC , Push regC 
	, Compute Add regMEM reg0 regD , Compute Add regMEM reg0 regE , Load (ImmValue (10)) regF , Compute Add regMEM regF regMEM , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Pop regB , Store regB (IndAddr regD ), Compute Incr regD reg0 regD , Push regE 
	, --end of array creation
	Pop regG , -- idCall to intArray.arr
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regB regD regB , Push regB 
	, -- accesser: .arr
	Pop regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regF , Compute Add regF regE regE , Push regE , -- end accesser: .arr
	-- end idCall to intArray.arr
	Pop regA , Store regG (IndAddr regA ), -- function call to 'printInt'
	-- visitIdExpr with type: int
	-- idCall to intArray.length
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regB regD regB , Push regB 
	, -- accesser: .length
	Pop regF , Load (IndAddr regF ) regF , Load (ImmValue (0)) regE , Compute Add regE regF regF , Push regF , -- end accesser: .length
	-- end idCall to intArray.length
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	-- precall
	Load (ImmValue (148)) regA , Store regA (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'printInt'
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regD , Compute Add regMEM regD regMEM , -- end prologueScope...
	Load (ImmValue (0)) regE , Push regE 
	, Pop regF , Load (ImmValue (1)) regG , Compute Add regARP regG regG , Store regF (IndAddr regG )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regA regC regA , Push regA 
	, -- end idCall to i
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to intArray.length
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regE , Compute Sub regD regE regD , Load (IndAddr regD ) regD , Load (ImmValue (1)) regE , Compute Add regD regE regD , Push regD 
	, -- accesser: .length
	Pop regF , Load (IndAddr regF ) regF , Load (ImmValue (0)) regG , Compute Add regG regF regF , Push regF , -- end accesser: .length
	-- end idCall to intArray.length
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	Pop regB , Pop regC , Compute Lt regC regB regC , Push regC 
	, Pop regC , Load (ImmValue (1)) regB , Compute Xor regC regB regC , Branch regC (Rel (64))
	, -- function call to 'printInt'
	-- visitIdExpr with type: int
	-- idCall to intArray.arr[i]
	Compute Add regARP reg0 regD , Load (ImmValue (1)) regE , Compute Sub regD regE regD , Load (IndAddr regD ) regD , Load (ImmValue (1)) regE , Compute Add regD regE regD , Push regD 
	, -- accesser: .arr
	Pop regG , Load (IndAddr regG ) regG , Load (ImmValue (1)) regF , Compute Add regF regG regG , Push regG , -- end accesser: .arr
	-- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regB regD regB , Push regB 
	, -- end idCall to i
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Pop regC , Pop regA , Load (IndAddr regA ) regA , Compute Add regA regC regA , Push regA , -- end accesser: [i]
	-- end idCall to intArray.arr[i]
	Pop regF , Load (IndAddr regF ) regF , Push regF 
	, -- end of visitIdExpr with type: int
	-- precall
	Load (ImmValue (232)) regG , Store regG (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Compute Add regARP reg0 regG , Compute Decr regG reg0 regG , Load (IndAddr regG ) regG , Store regG (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'printInt'
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regB regD regB , Push regB 
	, -- end idCall to i
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regF , Pop regA , Compute Add regA regF regA , Push regA 
	, Pop regG , -- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regC , Load (ImmValue (1)) regC , Compute Add regE regC regE , Push regE 
	, -- end idCall to i
	Pop regB , Store regG (IndAddr regB ), Jump (Rel (-93))
	, -- epilogueScope
	Load (IndAddr regARP ) regA , Compute Add regA reg0 regARP , -- end of epilogueScope
	Push regMEM , Load (ImmValue (2)) regF , Compute Add regMEM regF regMEM , Pop regE , Load (ImmValue (2)) regC , Compute Add regARP regC regC , Store regE (IndAddr regC )
	, Load (ImmValue (4)) regG , Push regG 
	, Pop regB , -- idCall to s.length
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regE , Load (ImmValue (2)) regE , Compute Add regF regE regF , Push regF 
	, -- accesser: .length
	Pop regC , Load (IndAddr regC ) regC , Load (ImmValue (0)) regG , Compute Add regG regC regC , Push regC , -- end accesser: .length
	-- end idCall to s.length
	Pop regD , Store regB (IndAddr regD ), -- creation of string 'PyVa'
	Compute Add regMEM reg0 regE , Push regE , Load (ImmValue (4)) regG , Compute Add regMEM regG regMEM , Load (ImmValue (80)) regF , Store regF (IndAddr regE ), Compute Incr regE reg0 regE , Load (ImmValue (121)) regF , Store regF (IndAddr regE ), Compute Incr regE reg0 regE , Load (ImmValue (86)) regF , Store regF (IndAddr regE ), Compute Incr regE reg0 regE , Load (ImmValue (97)) regF , Store regF (IndAddr regE ), Compute Incr regE reg0 regE , -- end of string creation
	Pop regC , -- idCall to s.s
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Load (ImmValue (2)) regF , Compute Add regA regF regA , Push regA 
	, -- accesser: .s
	Pop regE , Load (IndAddr regE ) regE , Load (ImmValue (1)) regG , Compute Add regG regE regE , Push regE , -- end accesser: .s
	-- end idCall to s.s
	Pop regB , Store regC (IndAddr regB ), -- function call to 'printInt'
	-- visitIdExpr with type: int
	-- idCall to s.length
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Load (ImmValue (2)) regF , Compute Add regA regF regA , Push regA 
	, -- accesser: .length
	Pop regG , Load (IndAddr regG ) regG , Load (ImmValue (0)) regE , Compute Add regE regG regG , Push regG , -- end accesser: .length
	-- end idCall to s.length
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	-- precall
	Load (ImmValue (330)) regB , Store regB (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'printInt'
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regF , Compute Add regMEM regF regMEM , -- end prologueScope...
	Load (ImmValue (0)) regE , Push regE 
	, Pop regG , Load (ImmValue (1)) regC , Compute Add regARP regC regC , Store regG (IndAddr regC )
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regB regD regB , Push regB 
	, -- end idCall to i
	Pop regA , Load (IndAddr regA ) regA , Push regA 
	, -- end of visitIdExpr with type: int
	-- visitIdExpr with type: int
	-- idCall to s.length
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regE , Compute Sub regF regE regF , Load (IndAddr regF ) regF , Load (ImmValue (2)) regE , Compute Add regF regE regF , Push regF 
	, -- accesser: .length
	Pop regG , Load (IndAddr regG ) regG , Load (ImmValue (0)) regC , Compute Add regC regG regG , Push regG , -- end accesser: .length
	-- end idCall to s.length
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	Pop regA , Pop regD , Compute Lt regD regA regD , Push regD 
	, Pop regD , Load (ImmValue (1)) regA , Compute Xor regD regA regD , Branch regD (Rel (56))
	, -- visitIdExpr with type: char
	-- idCall to s.s[i]
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regE , Compute Sub regF regE regF , Load (IndAddr regF ) regF , Load (ImmValue (2)) regE , Compute Add regF regE regF , Push regF 
	, -- accesser: .s
	Pop regC , Load (IndAddr regC ) regC , Load (ImmValue (1)) regG , Compute Add regG regC regC , Push regC , -- end accesser: .s
	-- accesser: [i]
	-- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regA regF regA , Push regA 
	, -- end idCall to i
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Pop regD , Pop regB , Load (IndAddr regB ) regB , Compute Add regB regD regB , Push regB , -- end accesser: [i]
	-- end idCall to s.s[i]
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: char
	Pop regC , WriteInstr regC charIO, Load (ImmValue (10)) regC , WriteInstr regC charIO
	, -- visitIdExpr with type: int
	-- idCall to i
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regA regF regA , Push regA 
	, -- end idCall to i
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (1)) regD , Push regD 
	, Pop regG , Pop regB , Compute Add regB regG regB , Push regB 
	, Pop regC , -- idCall to i
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regD , Load (ImmValue (1)) regD , Compute Add regE regD regE , Push regE 
	, -- end idCall to i
	Pop regA , Store regC (IndAddr regA ), Jump (Rel (-85))
	, -- epilogueScope
	Load (IndAddr regARP ) regB , Compute Add regB reg0 regARP , -- end of epilogueScope
	EndProg ]

main = run [prog]