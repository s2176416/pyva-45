module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regA , Compute Equal regA reg0 regE , Branch regE (Rel (-3)), Jump (Ind regA ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regF , Compute Add regF reg0 regARP , Load (ImmValue (3)) regD , Compute Add regMEM regD regMEM , Load (ImmValue (1)) regB , Push regB 
	, Pop regG , WriteInstr regG numberIO
	, EndProg ]

main = run [prog]