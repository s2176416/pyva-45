module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regA , Compute Equal regA reg0 regB , Branch regB (Rel (-3)), Jump (Ind regA ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regC , Compute Add regC reg0 regARP , Load (ImmValue (3)) regD , Compute Add regMEM regD regMEM , -- Function 'febDays' in scope: [0]
	Jump (Rel (125)), -- prologue...
	Load (ImmValue (3)) regG , Compute Add regMEM regG regMEM , Compute Add reg0 regARP regF , Compute Incr regF reg0 regF , Pop regE , Store regE (IndAddr regF ), -- end prologue...
	-- visitIdExpr with type: int
	-- idCall to year
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regA regB regA , Push regA 
	, -- end idCall to year
	Pop regC , Load (IndAddr regC ) regC , Push regC 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (4)) regD , Push regD 
	, -- visitIdExpr with type: int
	-- idCall to year
	Compute Add regARP reg0 regE , Load (ImmValue (1)) regF , Load (ImmValue (1)) regF , Compute Add regE regF regE , Push regE 
	, -- end idCall to year
	Pop regG , Load (IndAddr regG ) regG , Push regG 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (4)) regA , Push regA 
	, Pop regC , Pop regB , Load (ImmValue (0)) regE , Compute Lt regB regC regD , Branch regD (Rel (5)), Compute Sub regB regC regB , Load (ImmValue (1)) regD , Compute Add regE regD regE , Jump (Rel (-5)), Push regE 
	, Pop regG , Pop regF , Compute Mul regF regG regF , Push regF 
	, Pop regB , Pop regA , Compute Sub regA regB regA , Push regA 
	, Pop regC , Load (ImmValue (2)) regD , Compute Add regARP regD regD , Store regC (IndAddr regD )
	, Load (ImmValue (0)) regE , Push regE 
	, Pop regF , Load (ImmValue (3)) regG , Compute Add regARP regG regG , Store regF (IndAddr regG )
	, -- visitIdExpr with type: int
	-- idCall to mod
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regD , Load (ImmValue (2)) regD , Compute Add regC regD regC , Push regC 
	, -- end idCall to mod
	Pop regE , Load (IndAddr regE ) regE , Push regE 
	, -- end of visitIdExpr with type: int
	Load (ImmValue (0)) regF , Push regF 
	, Pop regA , Pop regG , Compute Gt regG regA regG , Push regG 
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	Pop regA , Load (ImmValue (1)) regB , Compute Xor regA regB regA , Branch regA (Rel (18))
	, -- prologueScope...
	Load (ImmValue (0)) regB , Compute Add regMEM regB regMEM , -- end prologueScope...
	Load (ImmValue (28)) regC , Push regC 
	, Pop regD , -- idCall to result
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regA , Compute Sub regG regA regG , Load (IndAddr regG ) regG , Load (ImmValue (3)) regA , Compute Add regG regA regG , Push regG 
	, -- end idCall to result
	Pop regE , Store regD (IndAddr regE ), -- epilogueScope
	Load (IndAddr regARP ) regB , Compute Add regB reg0 regARP , -- end of epilogueScope
	Jump (Rel (17))
	, -- prologueScope...
	Load (ImmValue (0)) regC , Compute Add regMEM regC regMEM , -- end prologueScope...
	Load (ImmValue (29)) regG , Push regG 
	, Pop regA , -- idCall to result
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regB , Compute Sub regF regB regF , Load (IndAddr regF ) regF , Load (ImmValue (3)) regB , Compute Add regF regB regF , Push regF 
	, -- end idCall to result
	Pop regD , Store regA (IndAddr regD ), -- epilogueScope
	Load (IndAddr regARP ) regC , Compute Add regC reg0 regARP , -- end of epilogueScope
	--return returnresult;
	-- visitIdExpr with type: int
	-- idCall to result
	Compute Add regARP reg0 regG , Load (ImmValue (1)) regF , Load (ImmValue (3)) regF , Compute Add regG regF regG , Push regG 
	, -- end idCall to result
	Pop regB , Load (IndAddr regB ) regB , Push regB 
	, -- end of visitIdExpr with type: int
	-- epilogue
	Load (IndAddr regARP ) regA , Compute Decr regARP reg0 regARP , Compute Decr regARP reg0 regARP , Load (IndAddr regARP ) regD , Compute Add regA reg0 regARP , Jump (Ind regD )-- end of epilogue
	, -- end return
-- End of function 'febDays' in scope: [0]
	-- function call to 'febDays'
	Load (ImmValue (2000)) regE , Push regE 
	, -- precall
	Load (ImmValue (149)) regC , Store regC (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'febDays'
	Pop regG , WriteInstr regG numberIO
	, -- function call to 'febDays'
	Load (ImmValue (2001)) regF , Push regF 
	, -- precall
	Load (ImmValue (162)) regB , Store regB (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'febDays'
	Pop regA , WriteInstr regA numberIO
	, -- function call to 'febDays'
	Load (ImmValue (2020)) regD , Push regD 
	, -- precall
	Load (ImmValue (175)) regE , Store regE (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add reg0 regMEM regARP , Compute Incr regMEM reg0 regMEM 
	, -- end of precall
	Jump (Abs (14))
	, -- function call ended for 'febDays'
	Pop regC , WriteInstr regC numberIO
	, EndProg ]

main = run [prog]