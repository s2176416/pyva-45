package testing.semantic;

import codegeneration.CodeGenerator;
import exceptions.Exception;
import pyva.PyVaLexer;
import pyva.PyVaParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import testing.syntax.SyntaxTest;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("Duplicates")
public class SemanticTest {

    @Test
    public void simplePrintTest() {
        ArrayList<String> output1 = new ArrayList<>();
        output1.add("Sprockell 0 says 1");
        testString("print 1;", output1, "simple-int-example");

        ArrayList<String> output2 = new ArrayList<>();
        output2.add("true");
        testString("print true;", output2, "simple-boolean-example");

        ArrayList<String> output3 = new ArrayList<>();
        output3.add("a");
        testString("print 'a';", output3, "simple-char-example");

        ArrayList<String> output4 = new ArrayList<>();
        output4.add("Sprockell 0 says 500");
        testString("print 1000 / 2;", output4, "simple-division-example");

        ArrayList<String> output5 = new ArrayList<>();
        output5.add("Sprockell 0 says 2");
        testString("int[][] iaa = [[1], [1, 2]]; print iaa[1][1];", output5, "simple-array-example");

        ArrayList<String> output6 = new ArrayList<>();
        output6.add("Sprockell 0 says 10");
        output6.add("Sprockell 0 says 100");
        testString("def record Coor {int x; int y;} Coor c = new Coor; c.x = 10; c.y = 100; print c.x; print c.y;", output6, "simple-record-example");
    }

    @Test
    public void simpleAlgorithms() {
        ArrayList<String> output1 = new ArrayList<>();
        output1.add("Sprockell 0 says 29"); // Year 2000
        output1.add("Sprockell 0 says 28"); // Year 2001
        output1.add("Sprockell 0 says 29"); // Year 2020
        testFile("feb-days", output1); // Open file in same package to see program

        ArrayList<String> output2 = new ArrayList<>();
        output2.add("true"); // Tests primality of 2
        output2.add("true"); // Tests primality of  3
        output2.add("false"); // Tests primality of 4
        output2.add("true"); // Tests primality of 5
        output2.add("false"); // Tests primality of 6
        output2.add("true"); // Tests primality of 7
        output2.add("false"); // Tests primality of 8
        output2.add("false"); // Tests primality of 9
        output2.add("false"); // Tests primality of 10
        output2.add("true"); // Tests primality of 11
        output2.add("false"); // Tests primality of 12
        output2.add("true"); // Tests primality of 13
        output2.add("false"); // Tests primality of 14
        output2.add("false"); // Tests primality of 15
        output2.add("false"); // Tests primality of 16
        output2.add("true"); // Tests primality of 17
        output2.add("false"); // Tests primality of 18
        output2.add("true"); // Tests primality of 19
        output2.add("false"); // Tests primality of 20
        testFile("prime", output2);

        ArrayList<String> output3 = new ArrayList<>();
        output3.add("Sprockell 0 says 1"); // Inspect bubble-sort.txt and insertion-sort.txt.
        output3.add("Sprockell 0 says 2"); // They print the initial array [5, 4, 3, 2, 1] after it is sorted.
        output3.add("Sprockell 0 says 3");
        output3.add("Sprockell 0 says 4");
        output3.add("Sprockell 0 says 5");
        testFile("bubble-sort", output3);
        testFile("insertion-sort", output3);

        ArrayList<String> output4 = new ArrayList<>();
        // Tests dynamic programming with a two dimensional array which counts the remainder
        // of currency that cannot be made given an amount and set of coins.
        // Here, the coins [10, 7, 4, 2] can make up 16. Thus, the remainder and printed result is 0.
        output4.add("Sprockell 0 says 0");
        testFile("dynamic-prog-rem-coins", output4);
    }

    @Test
    public void recordTest() {
        ArrayList<String> output = new ArrayList<>();
        output.add("Sprockell 0 says 10");
        output.add("Sprockell 0 says 1");
        output.add("Sprockell 0 says 2");
        output.add("Sprockell 0 says 3");
        output.add("Sprockell 0 says 4");
        output.add("Sprockell 0 says 5");
        output.add("Sprockell 0 says 6");
        output.add("Sprockell 0 says 7");
        output.add("Sprockell 0 says 8");
        output.add("Sprockell 0 says 9");
        output.add("Sprockell 0 says 10");
        output.add("Sprockell 0 says 4");
        output.add("P");
        output.add("y");
        output.add("V");
        output.add("a");
        testFile("arraysAsRecords", output);

        output.clear();
        output.add("Sprockell 0 says 5");
        testFile("linkedList", output);
    }

    // If this test is done, don't forget to manually terminate the ghcs created by this through task manager or the sort.
    // "runhaskell" opens ghc, does the "main" function, and terminates the ghc. But since these are infinite, they never terminate
    // since they never actually finish. The way this works now is if each process has elapsed 2 seconds and isn't finished yet, then the
    // test succeeds. But the actual programs are still running in ghc and the ghcs created by this test are not yet terminated
    @Test
    public void infiniteLoopTest() {
        testInfiniteString("for (int i = 0; i < 1; i = i) {}", "infinite-for-loop"); // Infinite for loop
        testInfiniteString("for (int i = 0; i < 1; i = i + 1) {i = i - 1;}", "infinite-for-loop-min"); // Similar infinite for loop as above
        testInfiniteString("print 1 / 0;", "infinite-divide-by-zero"); // Since soft-division is implemented with a loop, this will never terminate
        testInfiniteString("thread pp {for (int i = 0; i < 1; i = i) {}} join pp;", "infinite-while-thread-join"); // Since the main thread waits for child thread to finish, and child thread infinitely loops, never finishes.
    }

    public static void testInfiniteString(String input, String progName) {
        try {
            generate(input, progName);

            String workDir = System.getProperty("user.dir");
            String path;
            ProcessBuilder builder;
            if (System.getProperty("os.name").contains("Windows")) {
                path = workDir + "\\src\\codegeneration";
                builder = new ProcessBuilder(
                        "cmd.exe",
                        "/c",
                        "cd " + path + " && runhaskell ",
                        "Program.hs");
            } else {
                path = workDir + "/src/codegeneration";
                builder = new ProcessBuilder(
                        "bash",
                        "-c",
                        "cd " + path + " && runhaskell ",
                        "Program.hs");
            }
            builder.redirectErrorStream(true);
            Process p = builder.start();
            p.waitFor(2, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            // infinite loop. succeeds
        } catch (Exception | IOException e) {
            System.err.println("Input '" + input + "' could not be compiled!");
            e.printStackTrace();
            assert false;
        }
    }

    public static void testFile(String fileName, ArrayList<String> expectedOutput) {
        try {
            ArrayList<String> result = runFile(fileName);
            assertEquals(expectedOutput, result);
        } catch (IOException | Exception e) {
            System.err.println("File " + fileName + " could not be compiled!");
            e.printStackTrace();
            assert false;
        }
    }

    public static void testString(String input, ArrayList<String> expectedOutput, String progName) {
        try {
            ArrayList<String> result = runString(input, progName);
            assertEquals(result, expectedOutput);
        } catch (IOException | Exception e) {
            System.err.println("Input '" + input + "' could not be compiled!");
            e.printStackTrace();
            assert false;
        }
    }

    public static ArrayList<String> runFile(String fileName) throws IOException, Exception {
        String contents = null;
        try {
            contents = new String(Files.readAllBytes(Paths.get("src/testing/semantic/exampleprogs/" + fileName + ".txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return runString(contents, fileName);
    }

    public static ArrayList<String> runString(String input, String progName) throws IOException, Exception {
        generate(input, progName);

        String workDir = System.getProperty("user.dir");
        String path;
        ProcessBuilder builder;
        if (System.getProperty("os.name").contains("Windows")) {
            path = workDir + "\\src\\codegeneration";
            builder = new ProcessBuilder(
                    "cmd.exe",
                    "/c",
                    "cd " + path + " && runhaskell ",
                    "Program.hs");
        } else {
            path = workDir + "/src/codegeneration";
            builder = new ProcessBuilder(
                    "bash",
                    "-c",
                    "cd " + path + " && runhaskell ",
                    "Program.hs");
        }
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        ArrayList<String> result = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            r.readLine();
        }

        while (true) {
            line = r.readLine();
            if (line == null) break;
            result.add(line);
        }

        return result;
    }

    public static void generate(String input, String fileName) throws Exception, IOException {
        SyntaxTest.syntaxCheck(input);

        CharStream stream = CharStreams.fromString(input);
        Lexer lexer = new PyVaLexer(stream);
        TokenStream tokens = new CommonTokenStream(lexer);
        PyVaParser parser = new PyVaParser(tokens);
        ParseTree tree = parser.goal();

        CodeGenerator generator = new CodeGenerator();
        String generated = generator.generate(tree);

        FileWriter myWriter = new FileWriter("src/codegeneration/Program.hs");
        myWriter.write(generated);
        myWriter.close();

        myWriter = new FileWriter("src/testing/semantic/exampleinstructions/" + fileName + ".hs");
        myWriter.write(generated);
        myWriter.close();
    }
}
