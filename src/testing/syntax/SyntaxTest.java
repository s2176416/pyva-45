package testing.syntax;

import static org.junit.Assert.*;

import exceptions.Exception;
import exceptions.passes.SyntaxErrorListener;
import pyva.PyVaLexer;
import pyva.PyVaParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Objects;

public class SyntaxTest {

    @Test
    public void semiColonTest() {
        try {
        succeeds("int i = 0;");
        fails("int i = 0?");
        fails("int i = 0");
        assertEquals( "int", Objects.requireNonNull(syntaxCheck("int i = 0;")).getChild(0).getChild(0).getChild(0).getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void parenTest() {
        try {
            succeeds("if (i < 3) {print i;}");
            fails("if [i < 3] {}"); // Requires parentheses for if statement condition. Ditto for the one below.
            fails("if i < 3 {}");
            assertEquals("print", Objects.requireNonNull(syntaxCheck("if (i < 3) {print i;}")).getChild(0).getChild(4).getChild(1).getChild(0).getChild(0).getText());

            succeeds("while (i < 0) {print i;}");
            fails("while i = 0 {}"); // Requires parentheses for while loop condition
            assertEquals("print", Objects.requireNonNull(syntaxCheck("while (i < 0) {print i;}")).getChild(0).getChild(4).getChild(1).getChild(0).getChild(0).getText());

            succeeds("for (int i = 0; i < 10; i = i + 1) {print i;}");
            fails("for int i = 0; i < 10; i = i + 1 {}"); // Requires parentheses for for loops.
            assertEquals("print", Objects.requireNonNull(syntaxCheck("for (int i = 0; i < 10; i = i + 1) {print i;}")).getChild(0).getChild(9).getChild(0).getChild(0).getText());

            succeeds("def int one() { return 1;}");
            fails("def int one {return 1;}"); // Must use parentheses even though no parameters are required
            assertEquals("return", Objects.requireNonNull(syntaxCheck("def int one() { return 1;}")).getChild(0).getChild(7).getChild(0).getChild(0).getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void funcDefTest() {
        try {
            succeeds("def int inc(int i) {return i + 1;}");
            succeeds("int i = 0; def void inci() {def int inc(int a) {return a + 1;} i = inc(i);}");

            fails("def inc(int i) {return i + 1;}"); // Must have type in function definition
            fails("def void inc(int i) {return i = i + 1;}"); // Void functions can't have return statements
            fails("def int one {return 1;}"); // Must have parentheses in function definitions even when it requires no parameters

            assertEquals("void", Objects.requireNonNull(syntaxCheck("int i = 0; def void inci() {def int inc(int a) {return a + 1;} i = inc(i);}"))
            .getChild(1).getChild(1).getText());
            assertEquals("return", Objects.requireNonNull(syntaxCheck("int i = 0; def void inci() {def int inc(int a) {return a + 1;} i = inc(i);}"))
            .getChild(1).getChild(7).getChild(0).getChild(7).getChild(0).getChild(0).getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void blockScopeTest() {
        // { } imply a new scope
        succeeds("{print 0;}");
        succeeds("if (true) {print true;}");
        succeeds("for (int i = 0; i < 10; i = i + 1) {print i;}");
        succeeds("int i = 0; while (i < 10) {print i; i = i + 1;}");
        succeeds("def int one() {return 1;}");
        succeeds("int i = 0; thread inc {sync {i = i + 1;}}");

        fails("(print 0)"); // Cannot make another scope with any other symbols like below.
    }

    @Test
    public void vardeclareTest() {
        try {
            succeeds("int i = 0;");
            succeeds("int i = a = b = 0;");
            succeeds("int i = 0; int a = 1; i = a = 100;");
            succeeds("def int one() {return 1;} int i = one();");

            fails("int i;"); // Must give initial value

            assertEquals(8, Objects.requireNonNull(syntaxCheck("int i = a = b = 0;")).getChild(0).getChild(0).getChildCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void recordTest() {
        succeeds("def record Coor {int x; int y;}");
        fails("def Coor {int x; int y;}"); // The word "record" is required
        fails("def record Coor {int x = 0; int y = 0;}"); // Should only specify type of field, not value.
        fails("def record Coor (int x; int y;)"); // Should use curly braces

        succeeds("def record Coor {int x; int y;} Coor coor = new Coor; coor.x = 0; coor.y = 0;");
        fails("def record Coor {int x; int y;} Coor coor = new Coor();"); // No parentheses needed
    }

    @Test
    public void arrayTest() {
        succeeds("int[] intArr = [1, 2, 3];");
        succeeds("int[] intArr = new int[3];");
        succeeds("int[][] twoDim = [[1], [1, 2]];");
        succeeds("int[][] twoDim = new int[2][3];");

        fails("int[] intArr = {1, 2, 3};"); // Must use square brackets for the three tests below
        fails("int[] intArr = (1, 2, 3);");
        fails("int[] intArr = 1, 2, 3;");
        fails("int[] intArr = new int[];"); // Must give size if initializing in this way
    }

    public void succeeds(String input) {
        try {
            syntaxCheck(input);
        } catch (Exception e) {
            System.err.println("Input '" + input + "' should have been accepted but wasn't.");
            assert false;
        }
    }

    public void fails(String input) {
        try {
            syntaxCheck(input);
            fail("Input '" + input + "' should have been rejected but wasn't.");
        } catch (Exception e) {
            // Passes
        }
    }

    public static ParseTree syntaxCheck(String text) throws Exception {
        try {
            CharStream stream = CharStreams.fromString(text);
            Lexer lexer = null;
            Constructor<? extends Lexer> lexerConstr = (PyVaLexer.class)
                    .getConstructor(CharStream.class);

            lexer = lexerConstr.newInstance(stream);
            lexer.removeErrorListeners();
            SyntaxErrorListener errorListener = new SyntaxErrorListener();
            lexer.addErrorListener(errorListener);

            TokenStream tokens = new CommonTokenStream(lexer);
            PyVaParser parser = new PyVaParser(tokens);

            parser.removeErrorListeners();
            parser.addErrorListener(errorListener);

            ParseTree tree = parser.goal();
            if (errorListener.getErrors().size() > 0) {
                throw new Exception(Exception.getMessage(errorListener.getErrors()));
            }
            return tree;
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
