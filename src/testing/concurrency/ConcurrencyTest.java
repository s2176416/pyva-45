package testing.concurrency;

import codegeneration.CodeGenerator;
import exceptions.Exception;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pyva.PyVaLexer;
import pyva.PyVaParser;
import testing.syntax.SyntaxTest;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class ConcurrencyTest {
    @Test
    public void bankingTest() {
        // Comments provided in the program file
        ArrayList<String> output = new ArrayList<>();
        output.add("Sprockell 0 says 35000");
        testFile("banking", output);
    }

    @Test
    public void petersonTest() {
        // Peterson algorithm provided in file is very similar to the wiki algorithm.
        ArrayList<String> output = new ArrayList<>();
        output.add("Sprockell 0 says 2");
        testFile("peterson", output);
    }

    public static void testFile(String fileName, ArrayList<String> expectedOutput) {
        try {
            ArrayList<String> result = runFile(fileName);
            assertEquals(expectedOutput, result);
        } catch (IOException | Exception e) {
            System.err.println("File " + fileName + " could not be compiled!");
            e.printStackTrace();
            assert false;
        }
    }

    public static ArrayList<String> runFile(String fileName) throws IOException, Exception {
        String contents = null;
        try {
            contents = new String(Files.readAllBytes(Paths.get("src/testing/concurrency/programs/" + fileName + ".txt")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return runString(contents, fileName);
    }

    public static ArrayList<String> runString(String input, String progName) throws IOException, Exception {
        generate(input, progName);

        String workDir = System.getProperty("user.dir");
        String path;
        ProcessBuilder builder;
        if (System.getProperty("os.name").contains("Windows")) {
            path = workDir + "\\src\\codegeneration";
            builder = new ProcessBuilder(
                    "cmd.exe",
                    "/c",
                    "cd " + path + " && runhaskell ",
                    "Program.hs");
        } else {
            path = workDir + "/src/codegeneration";
            builder = new ProcessBuilder(
                    "bash",
                    "-c",
                    "cd " + path + " && runhaskell ",
                    "Program.hs");
        }
        builder.redirectErrorStream(true);
        Process p = builder.start();
        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        ArrayList<String> result = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            r.readLine();
        }

        while (true) {
            line = r.readLine();
            if (line == null) break;
            result.add(line);
        }

        return result;
    }

    public static void generate(String input, String fileName) throws Exception, IOException {
        SyntaxTest.syntaxCheck(input);

        CharStream stream = CharStreams.fromString(input);
        Lexer lexer = new PyVaLexer(stream);
        TokenStream tokens = new CommonTokenStream(lexer);
        PyVaParser parser = new PyVaParser(tokens);
        ParseTree tree = parser.goal();

        CodeGenerator generator = new CodeGenerator();
        String generated = generator.generate(tree);

        FileWriter myWriter = new FileWriter("src/codegeneration/Program.hs");
        myWriter.write(generated);
        myWriter.close();

        myWriter = new FileWriter("src/testing/concurrency/instructions/" + fileName + ".hs");
        myWriter.write(generated);
        myWriter.close();
    }
}
