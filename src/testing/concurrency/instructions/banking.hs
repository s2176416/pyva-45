module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regF , Compute Equal regF reg0 regG , Branch regG (Rel (-3)), Jump (Ind regF ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regA , Compute Add regA reg0 regARP , Load (ImmValue (4)) regB , Compute Add regMEM regB regMEM , Load (ImmValue (0)) regC , Push regC 
	, Pop regD , Load (ImmValue (1)) regE , Compute Add regARP regE regE , Store regD (IndAddr regE )
	, WriteInstr regD (DirAddr 5)
	, Load (ImmValue (28)) regF , WriteInstr regF (DirAddr 1), Jump (Rel (77)), -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regC , Compute Add regMEM regC regMEM , -- end prologueScope...
	Load (ImmValue (0)) regD , Push regD 
	, Pop regE , WriteInstr regE (DirAddr 4)
	, -- visitIdExpr with type: int
	-- idCall to i
	ReadInstr (DirAddr 4), Receive regA , Push regA , -- end idCall to i
	-- end of visitIdExpr with type: int
	Load (ImmValue (10)) regC , Push regC 
	, Pop regE , Pop regD , Compute Lt regD regE regD , Push regD 
	, Pop regA , Load (ImmValue (1)) regB , Compute Xor regA regB regA , Branch regA (Rel (39))
	, TestAndSet (DirAddr 0), Receive regF , Compute Equal reg0 regF regF , Branch regF (Rel (-3))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to balance
	ReadInstr (DirAddr 5), Receive regB , Push regB , -- end idCall to balance
	-- end of visitIdExpr with type: int
	Load (ImmValue (1000)) regD , Push regD 
	, Pop regF , Pop regE , Compute Add regE regF regE , Push regE 
	, Pop regG , WriteInstr regG (DirAddr 5), -- epilogueScope
	Load (IndAddr regARP ) regC , Compute Add regC reg0 regARP , -- end of epilogueScope
	Load (IndAddr regARP ) regARP 
	, WriteInstr reg0 (DirAddr 0)
	, -- visitIdExpr with type: int
	-- idCall to i
	ReadInstr (DirAddr 4), Receive regE , Push regE , -- end idCall to i
	-- end of visitIdExpr with type: int
	Load (ImmValue (1)) regG , Push regG 
	, Pop regB , Pop regA , Compute Add regA regB regA , Push regA 
	, Pop regC , WriteInstr regC (DirAddr 4), Jump (Rel (-50))
	, -- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	-- epilogueScope
	Load (IndAddr regARP ) regG , Compute Add regG reg0 regARP , -- end of epilogueScope
	Load (IndAddr regARP ) regARP 
	, WriteInstr reg0 (IndAddr regSprID ), EndProg 
	, Load (ImmValue (107)) regA , WriteInstr regA (DirAddr 2), Jump (Rel (77)), -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regB , Compute Add regMEM regB regMEM , -- end prologueScope...
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regE , Compute Add regMEM regE regMEM , -- end prologueScope...
	Load (ImmValue (0)) regF , Push regF 
	, Pop regG , WriteInstr regG (DirAddr 6)
	, -- visitIdExpr with type: int
	-- idCall to i
	ReadInstr (DirAddr 6), Receive regC , Push regC , -- end idCall to i
	-- end of visitIdExpr with type: int
	Load (ImmValue (10)) regE , Push regE 
	, Pop regG , Pop regF , Compute Lt regF regG regF , Push regF 
	, Pop regC , Load (ImmValue (1)) regD , Compute Xor regC regD regC , Branch regC (Rel (39))
	, TestAndSet (DirAddr 0), Receive regA , Compute Equal reg0 regA regA , Branch regA (Rel (-3))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regB , Compute Add regMEM regB regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to balance
	ReadInstr (DirAddr 5), Receive regD , Push regD , -- end idCall to balance
	-- end of visitIdExpr with type: int
	Load (ImmValue (500)) regF , Push regF 
	, Pop regA , Pop regG , Compute Add regG regA regG , Push regG 
	, Pop regB , WriteInstr regB (DirAddr 5), -- epilogueScope
	Load (IndAddr regARP ) regE , Compute Add regE reg0 regARP , -- end of epilogueScope
	Load (IndAddr regARP ) regARP 
	, WriteInstr reg0 (DirAddr 0)
	, -- visitIdExpr with type: int
	-- idCall to i
	ReadInstr (DirAddr 6), Receive regG , Push regG , -- end idCall to i
	-- end of visitIdExpr with type: int
	Load (ImmValue (1)) regB , Push regB 
	, Pop regD , Pop regC , Compute Add regC regD regC , Push regC 
	, Pop regE , WriteInstr regE (DirAddr 6), Jump (Rel (-50))
	, -- epilogueScope
	Load (IndAddr regARP ) regA , Compute Add regA reg0 regARP , -- end of epilogueScope
	-- epilogueScope
	Load (IndAddr regARP ) regB , Compute Add regB reg0 regARP , -- end of epilogueScope
	Load (IndAddr regARP ) regARP 
	, WriteInstr reg0 (IndAddr regSprID ), EndProg 
	, Load (ImmValue (186)) regC , WriteInstr regC (DirAddr 3), Jump (Rel (77)), -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regD , Compute Add regMEM regD regMEM , -- end prologueScope...
	-- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (1)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	Load (ImmValue (0)) regA , Push regA 
	, Pop regB , WriteInstr regB (DirAddr 7)
	, -- visitIdExpr with type: int
	-- idCall to i
	ReadInstr (DirAddr 7), Receive regE , Push regE , -- end idCall to i
	-- end of visitIdExpr with type: int
	Load (ImmValue (10)) regG , Push regG 
	, Pop regB , Pop regA , Compute Lt regA regB regA , Push regA 
	, Pop regE , Load (ImmValue (1)) regF , Compute Xor regE regF regE , Branch regE (Rel (39))
	, TestAndSet (DirAddr 0), Receive regC , Compute Equal reg0 regC regC , Branch regC (Rel (-3))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regD , Compute Add regMEM regD regMEM , -- end prologueScope...
	-- visitIdExpr with type: int
	-- idCall to balance
	ReadInstr (DirAddr 5), Receive regF , Push regF , -- end idCall to balance
	-- end of visitIdExpr with type: int
	Load (ImmValue (2000)) regA , Push regA 
	, Pop regC , Pop regB , Compute Add regB regC regB , Push regB 
	, Pop regD , WriteInstr regD (DirAddr 5), -- epilogueScope
	Load (IndAddr regARP ) regG , Compute Add regG reg0 regARP , -- end of epilogueScope
	Load (IndAddr regARP ) regARP 
	, WriteInstr reg0 (DirAddr 0)
	, -- visitIdExpr with type: int
	-- idCall to i
	ReadInstr (DirAddr 7), Receive regB , Push regB , -- end idCall to i
	-- end of visitIdExpr with type: int
	Load (ImmValue (1)) regD , Push regD 
	, Pop regF , Pop regE , Compute Add regE regF regE , Push regE 
	, Pop regG , WriteInstr regG (DirAddr 7), Jump (Rel (-50))
	, -- epilogueScope
	Load (IndAddr regARP ) regC , Compute Add regC reg0 regARP , -- end of epilogueScope
	-- epilogueScope
	Load (IndAddr regARP ) regD , Compute Add regD reg0 regARP , -- end of epilogueScope
	Load (IndAddr regARP ) regARP 
	, WriteInstr reg0 (IndAddr regSprID ), EndProg 
	, ReadInstr (DirAddr 1), Receive regE , Compute NEq regE reg0 regE , Branch regE (Rel (-3))
	, ReadInstr (DirAddr 2), Receive regF , Compute NEq regF reg0 regF , Branch regF (Rel (-3))
	, ReadInstr (DirAddr 3), Receive regG , Compute NEq regG reg0 regG , Branch regG (Rel (-3))
	, -- visitIdExpr with type: int
	-- idCall to balance
	Compute Add regARP reg0 regA , Load (ImmValue (1)) regB , Load (ImmValue (1)) regB , Compute Add regA regB regA , ReadInstr (DirAddr 5), Receive regB , Store regB (IndAddr regA ), Load (IndAddr regA ) regA , Push regA 
	, -- end idCall to balance
	-- end of visitIdExpr with type: int
	Pop regD , WriteInstr regD numberIO
	, EndProg ]

main = run [prog, prog, prog, prog]