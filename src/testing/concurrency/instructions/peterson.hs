module Program where

import Sprockell
import ProgramFunctions

prog :: [Instruction]
prog = [
	 Branch regSprID (Rel (2)), Jump (Rel (6)), ReadInstr (IndAddr regSprID ), Receive regA , Compute Equal regA reg0 regB , Branch regB (Rel (-3)), Jump (Ind regA ), Load (ImmValue (0)) regARP 
	, Load (ImmValue (0)) regMEM 
	, Load (ImmValue (1)) regC , Compute Add regC reg0 regARP , Load (ImmValue (7)) regD , Compute Add regMEM regD regMEM , Load (ImmValue (0)) regE , Push regE 
	, Pop regF , Load (ImmValue (1)) regG , Compute Add regARP regG regG , Store regF (IndAddr regG )
	, WriteInstr regF (DirAddr 6)
	, Load (ImmValue (0)) regA , Push regA 
	, Pop regB , Load (ImmValue (2)) regC , Compute Add regARP regC regC , Store regB (IndAddr regC )
	, WriteInstr regB (DirAddr 4)
	, Load (ImmValue (0)) regD , Push regD 
	, Pop regE , Load (ImmValue (3)) regF , Compute Add regARP regF regF , Store regE (IndAddr regF )
	, WriteInstr regE (DirAddr 5)
	, Load (ImmValue (0)) regG , Push regG 
	, Pop regA , Load (ImmValue (4)) regB , Compute Add regARP regB regB , Store regA (IndAddr regB )
	, WriteInstr regA (DirAddr 7)
	, Load (ImmValue (49)) regC , WriteInstr regC (DirAddr 1), Jump (Rel (66)), -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regD , Compute Add regMEM regD regMEM , -- end prologueScope...
	Load (ImmValue (1)) regE , Push regE 
	, Pop regF , WriteInstr regF (DirAddr 4), Load (ImmValue (0)) regB , Push regB 
	, Pop regC , WriteInstr regC (DirAddr 5), -- visitIdExpr with type: boolean
	-- idCall to flag0
	ReadInstr (DirAddr 6), Receive regB , Push regB , -- end idCall to flag0
	-- end of visitIdExpr with type: boolean
	-- visitIdExpr with type: int
	-- idCall to turn
	ReadInstr (DirAddr 5), Receive regE , Push regE , -- end idCall to turn
	-- end of visitIdExpr with type: int
	Load (ImmValue (0)) regG , Push regG 
	, Pop regB , Pop regA , Compute Equal regA regB regA , Push regA 
	, Pop regD , Pop regC , Compute And regC regD regC , Push regC 
	, Pop regF , Load (ImmValue (1)) regG , Compute Xor regF regG regF , Branch regF (Rel (11))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regE , Compute Add regMEM regE regMEM , -- end prologueScope...
	-- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	Jump (Rel (-29))
	, -- visitIdExpr with type: int
	-- idCall to value
	ReadInstr (DirAddr 7), Receive regA , Push regA , -- end idCall to value
	-- end of visitIdExpr with type: int
	Load (ImmValue (1)) regC , Push regC 
	, Pop regE , Pop regD , Compute Add regD regE regD , Push regD 
	, Pop regF , WriteInstr regF (DirAddr 7), Load (ImmValue (0)) regB , Push regB 
	, Pop regC , WriteInstr regC (DirAddr 4), -- epilogueScope
	Load (IndAddr regARP ) regF , Compute Add regF reg0 regARP , -- end of epilogueScope
	Load (IndAddr regARP ) regARP 
	, WriteInstr reg0 (IndAddr regSprID ), EndProg 
	, Load (ImmValue (1)) regG , Push regG 
	, Pop regA , Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Compute Add regB regC regB , Store regA (IndAddr regB )
	, WriteInstr regA (DirAddr 6), Load (ImmValue (1)) regD , Push regD 
	, Pop regE , Compute Add regARP reg0 regF , Load (ImmValue (3)) regG , Compute Add regF regG regF , Store regE (IndAddr regF )
	, WriteInstr regE (DirAddr 5), -- visitIdExpr with type: boolean
	-- idCall to flag1
	Compute Add regARP reg0 regC , Load (ImmValue (1)) regD , Load (ImmValue (2)) regD , Compute Add regC regD regC , ReadInstr (DirAddr 4), Receive regD , Store regD (IndAddr regC ), Load (IndAddr regC ) regC , Push regC 
	, -- end idCall to flag1
	-- end of visitIdExpr with type: boolean
	-- visitIdExpr with type: int
	-- idCall to turn
	Compute Add regARP reg0 regF , Load (ImmValue (1)) regG , Load (ImmValue (3)) regG , Compute Add regF regG regF , ReadInstr (DirAddr 5), Receive regG , Store regG (IndAddr regF ), Load (IndAddr regF ) regF , Push regF 
	, -- end idCall to turn
	-- end of visitIdExpr with type: int
	Load (ImmValue (1)) regB , Push regB 
	, Pop regD , Pop regC , Compute Equal regC regD regC , Push regC 
	, Pop regF , Pop regE , Compute And regE regF regE , Push regE 
	, Pop regA , Load (ImmValue (1)) regB , Compute Xor regA regB regA , Branch regA (Rel (11))
	, -- preScope
	Store regARP (IndAddr regMEM ), Compute Incr regMEM reg0 regMEM , Store regARP (IndAddr regMEM ), Compute Add regMEM reg0 regARP , Compute Incr regMEM reg0 regMEM , -- end of preScope
	-- prologueScope...
	Load (ImmValue (0)) regG , Compute Add regMEM regG regMEM , -- end prologueScope...
	-- epilogueScope
	Load (IndAddr regARP ) regA , Compute Add regA reg0 regARP , -- end of epilogueScope
	Jump (Rel (-41))
	, -- visitIdExpr with type: int
	-- idCall to value
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Load (ImmValue (4)) regC , Compute Add regB regC regB , ReadInstr (DirAddr 7), Receive regC , Store regC (IndAddr regB ), Load (IndAddr regB ) regB , Push regB 
	, -- end idCall to value
	-- end of visitIdExpr with type: int
	Load (ImmValue (1)) regE , Push regE 
	, Pop regG , Pop regF , Compute Add regF regG regF , Push regF 
	, Pop regA , Compute Add regARP reg0 regB , Load (ImmValue (4)) regC , Compute Add regB regC regB , Store regA (IndAddr regB )
	, WriteInstr regA (DirAddr 7), Load (ImmValue (0)) regD , Push regD 
	, Pop regE , Compute Add regARP reg0 regF , Load (ImmValue (1)) regG , Compute Add regF regG regF , Store regE (IndAddr regF )
	, WriteInstr regE (DirAddr 6), ReadInstr (DirAddr 1), Receive regA , Compute NEq regA reg0 regA , Branch regA (Rel (-3))
	, -- visitIdExpr with type: int
	-- idCall to value
	Compute Add regARP reg0 regB , Load (ImmValue (1)) regC , Load (ImmValue (4)) regC , Compute Add regB regC regB , ReadInstr (DirAddr 7), Receive regC , Store regC (IndAddr regB ), Load (IndAddr regB ) regB , Push regB 
	, -- end idCall to value
	-- end of visitIdExpr with type: int
	Pop regE , WriteInstr regE numberIO
	, EndProg ]

main = run [prog, prog]