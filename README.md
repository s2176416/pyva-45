Before using the compiler, you need to know that we also changed and altered the Sprockell
installation. We changed the name of the register that we constantly use that points to
the current ARP to regARP, changed the register count since we're only left with 5
free registers after reserving one for the regARP, and increased the heap size since
we couldn't find a way to make functions, arrays and records work at once without doing that,
especially if multi-dimensional arrays, records that has other records as fields and/or
functions that use other functions.

For that we have provided our very own BasicFunctions.hs in the same directory as this README
file. Please change your current BasicFunctions.hs in your Sprockell with the one we've provided
and reinstall your Sprockell.

To use the compiler:
1. Write your program in pyva.PyVa.txt
2. Open the class pyva.PyVa.java
3. Run the main method of pyva.PyVa.java

On default, the main method should look like:
runFile(PYVA_NOTE);
which takes whatever program is in pyva.PyVa.txt, compiles it, and runs it
by accessing the terminal/cmd and running the haskell file in which the instruction
code of the program is witten to.

But if for whatever reason it doesn't work (maybe because of some system specific
errors), then:
1. Write your program in pyva.PyVa.txt
2. Open the class pyva.PyVa.java
3. Comment out runFile(PYVA_NOTE); in the main method of pyva.PyVa.java
4. Uncomment the line directly below it (generateFile(PYVA_NOTE);)
5. Run the main method of pyva.PyVa.java
6. Open ghci in the directory of the file codegeneration.Program.hs
7. run the main method of Program.hs

There also exists other example programs that you can try out whose file paths are provided
in pyve.PyVa.java. simply change the the "PYVA_NOTE" part of the methods calls "runFile"
and/or "generateFile" (whichever one applies to you) with the file path of he program
that you want to try out.

Have fun with out compiler! ;-)